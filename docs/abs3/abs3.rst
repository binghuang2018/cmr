.. _abs3:

Database of ABS3 materials
==========================

.. container:: article

    Korina Kuhar, Andrea Crovetto, Mohnish Pandey,
    Kristian Sommer Thygesen, Brian Joseph Seger,
    Peter C K Vesborg, Ole Hansen, Ib Chorkendorff,
    Karsten Wedel Jacobsen

    `Sulfide Perovskites for Solar Energy Conversion Applications:
    Computational Screening and Synthesis of the Selected Compound
    LaYS3`__

    Energy Environ. Sci., 2017,10, 2579-2593

    __ https://dx.doi.org/10.1039/C7EE02702H


.. contents::


The data
--------

* Download database: :download:`abs3.db`
* `Browse data <https://cmrdb.fysik.dtu.dk/abs3>`_


Key-value pairs
---------------


.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Example
-------

Band gaps of TeHfS3 calculated in the different phases:

.. image:: gaps.svg
.. literalinclude:: gaps.py
