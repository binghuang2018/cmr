# creates: gaps.svg
import matplotlib.pyplot as plt
from ase.db import connect

db = connect('abs3.db')
indirect = []
direct = []
names = []
for row in db.select('TeHfS3', sort='GLLB_ind'):
    indirect.append(row.GLLB_ind)
    direct.append(row.GLLB_dir)
    names.append(row.ABS3_name + '-' + row.prototype)

plt.plot(indirect, 'o', label='GLLBSC (indirect)')
plt.plot(direct, 'o', label='GLLBSC (direct)')
plt.xticks(range(len(names)), names, rotation=90)
plt.legend()
plt.ylabel('Gap [eV]')
plt.savefig('gaps.svg', bbox_inches='tight')
