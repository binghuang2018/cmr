.. _agau309:

=========================================================
Ground state orderings in icosahedral Ag-Au nanoparticles
=========================================================

Structures at every concentration of a 309-atom Mackay icosahedron,
obtained using exact solution of a cluster expansion model.

.. container:: article

    Rich Ground-State Chemical Ordering in Nanoparticles:
    Exact Solution of a Model for Ag-Au Clusters

    Peter Mahler Larsen, Karsten Wedel Jacobsen, and Jakob Schiøtz

    `Phys. Rev. Lett. 120, 256101`__

    __ https://doi.org/10.1103/PhysRevLett.120.256101


* Download data: :download:`agau309.db`
* `Browse data <https://cmrdb.fysik.dtu.dk/agau309>`_

Example
-------

Energies of formation (from EMT) vs the number of Au atoms in the nanoparticle:

.. image:: formation_energies.svg

.. literalinclude:: formation_energies.py
