.. _mp_gllbsc:

New Light Harvesting Materials
==============================

Electronic bandgap calculations are presented for 2400 experimentally known
materials from the Materials Project database and the bandgaps, obtained with
different types of functionals within density functional theory and (partial)
self-consistent GW approximation, are compared for 20 randomly chosen
compounds forming an unconventional set of ternary and quaternary materials.

.. container:: article

    Ivano E. Castelli, Falco Hüser, Mohnish Pandey, Hong Li,
    Kristian S. Thygesen, Brian Seger, Anubhav Jain, Kristin Persson,
    Gerbrand Ceder, and Karsten W. Jacobsen

    `New Light Harvesting Materials Using Accurate and Efficient Bandgap
    Calculations`__

    Advanced Energy Materials, Juli 22, 2014

    __ http://dx.doi.org/10.1002/aenm.201400915

* :download:`Download raw data <mp_gllbsc.db>`
* `Browse data <https://cmrdb.fysik.dtu.dk/mp_gllbsc>`_


Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Band gaps
---------

Here, we calculated the errors in the band gaps at `\Gamma` for a set of 20
ternary and quaternary materials releative to self-consitent GW:

.. literalinclude:: table.py
    :start-after: future

.. csv-table::
    :file: gaps.csv
    :header-rows: 1

.. image:: gaps.svg

Here is how to make the plot:

.. literalinclude:: figure.py
