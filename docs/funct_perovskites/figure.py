# creates: gaps.svg
import numpy as np
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.cm as cm
import ase.db

con = ase.db.connect('funct_perovskites.db')

comb_A = 'BaSnO3'
comb_B = 'BaTaO2N'

gaps = []

for nA in range(1, 7):
    for nB in range(1, 7):
        name = nA * 'A' + nB * 'B'
        row = con.get(comb_A=comb_A, comb_B=comb_B, sequence=name)
        gaps.append(row.gllbsc_gamma_gap)

colors = []
for gap in gaps:
    c = cm.jet(int(gap / max(gaps) * 255))
    colors.append([c[0], c[1], c[2]])

x, y = np.meshgrid(range(1, 7), range(1, 7))
x = x.flat
y = y.flat
z = np.zeros_like(x)
dx = dy = 0.5 * np.ones_like(x)
dz = gaps
fig = plt.figure()
ax = Axes3D(fig, auto_add_to_figure=False)
fig.add_axes(ax)
plt.xlabel('nB, B = ' + comb_B)
plt.ylabel('nA, A = ' + comb_A)
ax.set_zlabel('Bandgap [eV]')
ax.bar3d(x, y, z, dx, dy, dz, color=colors)
plt.savefig('gaps.svg')
