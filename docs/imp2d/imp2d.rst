.. _imp2d:

===================================
Impurities in 2D Materials Database
===================================

If you are using data from this database in your research, please cite the
following paper:

.. container:: article

    `Absorption versus Adsorption: High-Throughput Computation of Impurities in 2D Materials`__

    Joel Davidsson, Fabian Bertoldo, Kristian S. Thygesen, Rickard Armiento

    arXiv:2207.05353 (cond-mat)

    __ https://arxiv.org/abs/2207.05353

Please also cite the related DOI for the actual data:

.. container:: article

    `Interstitial and Adsorbate Structure Database`__

    Joel Davidsson, Fabian Bertoldo, Kristian S. Thygesen, Rickard Armiento

    DOI 10.11583/DTU.19692238

    __ https://doi.org/10.11583/DTU.19692238


* `Browse data <https://cmrdb.fysik.dtu.dk/imp2d>`_
* Download database: :download:`imp2d.db`


.. contents::

Brief description
=================
This database contains structural, thermodynamic, and electronic data of more
than 17,500 single impurity defects over a set of 53 experimentally synthesized
2D monolayers. The defect structures (interstitials, adsorption sites) were set up with the ASE
DefectBuilder_ module. The computational workflow was implemented with the
httk_ High-Throughput Toolkit. 12,813 of the overall 17,364 systems have reached
full ionic convergence as described in the publication above.


Overview of methods and parameters used
=======================================

If a parameter is not specified at a given step, its value equals that
of the last step where it was specified:

.. list-table::
  :widths: 10 25
  :header-rows: 1

  * - Workflow step(s)
    - Parameters

  * - Initial relaxation
    - Electronic structure code: VASP_;
      xc functional = PBE;
      `k`-point grid: Gamma-point only;
      spin-polarized;
      PW energy cutoff = 600 eV;
      kinetic energy cutoff = 900 eV;
      electronic tolerance (energy) = 10`^{-4}`;
      ionic tolerance (energy) = 0.005 eV;
      FFT grid = 3/2

  * - Final relaxation
    - Electronic structure code: VASP_;
      xc functional = PBE;
      `k`-point grid: Gamma-point only;
      spin-polarized;
      PW energy cutoff = 600 eV;
      kinetic energy cutoff = 900 eV;
      electronic tolerance (energy) = 10`^{-6}`;
      ionic tolerance (energy) = 0.0001 eV;
      FFT grid = 2

Funding acknowledgements
========================

The project has received funding from the Danish National Research
Foundation’s  Center for Nanostructured Graphene (CNG) and the European
Research Council (ERC) under the European Union’s Horizon 2020 research and
innovation program Grant No. 773122 (LIMA) and Grant agreement No. 951786
(NOMAD CoE). We acknowlege funding from the Swedish eScience Centre (SeRC)
and support from the Swedish REsearch Council (VR) Grant No. 2020-05402.
The computations were enabled by resources provided by the Swedish National
Infrastructure for Computing (SNIC) at NSC and PDC partially funded by the
Swedish Research Council through Grant Agreement No. 2018-05973.


Versions
========


.. list-table::
    :widths: 3 1 10

    * - Version
      - rows
      - comment

    * - 2022-07-12
      - 17364
      - Initial release


Key-value pairs
===============

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Examples
========

Formation energies for period 4 transition metal impurities in MoSe2
--------------------------------------------------------------------

The following Python script shows how to plot the formation energies
of period 4 transition metal impurities (for different interstitial
and adsorption sites).

.. literalinclude:: formation_energies_MoSe2.py

This produces the figure

.. image:: formation_energies_MoSe2.png


Tools for creating the database
============================================================

Software used
--------------

VASP_, httk_, Python_, and ASE_.


Defect setup
------------
The defect setup was inspired by the work of J. Davidsson for ADAQ_ and generalized in
the ASE DefectBuilder_. Find more information, explanations, and some code examples in
the documentation_ of the DefectBuilder.


Workflow
--------

The workflow was executed using httk_.

.. _Python: https://www.python.org/
.. _ASE: https://wiki.fysik.dtu.dk/ase/
.. _httk: https://httk.org/
.. _VASP: https://www.vasp.at/
.. _ADAQ: https://httk.org/adaq/
.. _DefectBuilder: https://gitlab.com/ase/ase/-/blob/defect-setup-utils/ase/build/defects.py
.. _documentation: https://gitlab.com/ase/ase/-/blob/defect-setup-utils/doc/ase/build/defects.rst
