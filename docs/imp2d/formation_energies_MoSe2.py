# creates: formation_energies_MoSe2.png
from ase.data import atomic_numbers
from ase.db import connect
import matplotlib.pyplot as plt


def get_plot_data(db, hosts, dopants):
    """Return data needed for subsequent plotting."""
    eforms = []
    numbers = []
    defecttypes = []
    # loop over all given host materials
    for host in hosts:
        # loop over all given dopants
        for dopant in dopants:
            # only include data that is fully converged and relaxed
            for row in db.select(host=host, dopant=dopant, converged=True):
                # convert dopant string to atomic number and append to list
                numbers.append(atomic_numbers[dopant])
                # extract formation energy and defecttype
                # (interstitial or adsorbate)
                eforms.append(row.eform)
                defecttypes.append(row.defecttype)

    return eforms, numbers, defecttypes


def plot(filename):
    # connect to the 'Impurities in 2D Materials database'
    db = connect('imp2d.db')

    # extract plotting data for group 4 transition metal doped MoSe2
    hosts = ['MoSe2']
    dopants = ['Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn']
    eforms, numbers, defecttypes = get_plot_data(db, hosts, dopants)

    # the actual plotting is happening here:
    # matplotlib setup
    fig, ax = plt.subplots(figsize=(6, 3.8))
    # plot the formation energies for the period 4 TM-impurities in MoS2
    for i in range(len(eforms)):
        if defecttypes[i] == 'interstitial':
            color = 'C0'
        elif defecttypes[i] == 'adsorbate':
            color = 'C1'
        # plot formation energy versus dopant element
        ax.scatter(numbers[i], eforms[i], marker='.', color=color)
    # set title
    ax.set_title(r'MoSe$_2$ impurities')
    # set up the legend
    ax.scatter([], [], marker='.', color='C0',
               label='interstitial')
    ax.scatter([], [], marker='.', color='C1',
               label='adsorbate')
    ax.legend()
    # fix xticks
    ax.set_xticks(list(set(numbers)))
    ax.set_xticklabels(dopants)
    # set x- and y-label
    ax.set_xlabel(r'Dopant element')
    ax.set_ylabel(r'Formation energy [eV]')
    # set plot limits
    ax.set_ylim(0, 5)
    # save figure
    plt.tight_layout()
    plt.savefig(filename)
    # plt.show()


plot('formation_energies_MoSe2.png')
