import datetime
import sys

sys.path[:0] = ['.']
extensions = ['run_python_scripts',
              'sphinx.ext.autodoc',
              'sphinx.ext.viewcode',
              'sphinx.ext.intersphinx',
              'sphinx.ext.mathjax']
source_suffix = '.rst'
master_doc = 'index'
project = u'COMPUTATIONAL MATERIALS REPOSITORY'
copyright = f'2014-{datetime.date.today().year}, CAMd'
exclude_patterns = ['build', 'README.rst']
pygments_style = 'sphinx'
default_role = 'math'
html_theme = 'pyramid'
html_theme_options = {'body_max_width': 1000}
html_style = 'cmr.css'
html_title = 'COMPUTATIONAL MATERIALS REPOSITORY'
# html_logo = 'static/cmr.png'
html_favicon = 'static/cmr.ico'
html_static_path = ['static']
html_last_updated_fmt = '%a, %d %b %Y %H:%M:%S'
intersphinx_mapping = {'ase': ('https://wiki.fysik.dtu.dk/ase', None),
                       'asr': ('https://asr.readthedocs.io/en/latest', None)}
