.. _low_symmetry_perovskites:

Low symmetry perovskites
========================

We have used density functional theory (DFT) calculations to investigate 300
oxides and oxynitrides in the Ruddlesden–Popper phase of the layered
perovskite structure.

.. container:: article

    Castelli, I. E., Thygesen, K. S., and Jacobsen, K. W.

    `Bandgap Engineering of Double Perovskites for One- and Two-photon
    Water Splitting.`__

    MRS Proc. 1523, mrsf121523qq0706.

    __ http://dx.doi.org/10.1557/opl.2013.450

.. container:: article

    Castelli, I. E., García-Lastra, J. M., Hüser, F., Thygesen, K. S.,
    and Jacobsen, K. W.

    `Stability and bandgaps of layered perovskites for one- and two-photon
    water splitting.`__

    New Journal Of Physics 15, 105026.

    __ http://dx.doi.org/10.1088/1367-2630/15/10/105026

* :download:`Download raw data <low_symmetry_perovskites.db>`
* `Browse data
  <https://cmrdb.fysik.dtu.dk/low_symmetry_perovskites>`_


Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1
