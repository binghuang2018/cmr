# creates: enthalpy.png
import matplotlib.pyplot as plt
from ase.db import connect

db = connect('a2bcx4.db')

plt.figure(figsize=(10, 5))

names = []
space_groups = ['P31', 'Pmn21', 'I-4', 'I4-2m', 'Ama2', 'P1n1']

for space_group, mark, color in zip(space_groups, 'osvp*h', 'rbgcmy'):
    y = []
    dy = []
    x = []
    counter = 0
    for row in db.select(space_group=space_group):
        if 'S' in row.symbols and 'Cu' in row.symbols:
            y.append(row.E_relative_per_atom)
            dy.append(row.E_uncertainty_per_atom)
            names.append(row.name)
            x.append(counter)
            counter = counter + 1
    plt.errorbar(x, y, yerr=dy, color=color, fmt=mark, label=space_group)
    n = len(x)
    assert names[:n] == names[-n:]

plt.xticks(x, names[:n], rotation=70)
for xx in x:
    plt.axvline(xx + 0.5, color='lightgrey')
plt.axhline(0, color='grey')
plt.xlim(-0.5, x[-1] + 0.5)
plt.legend()
plt.ylabel(r'$E$ [meV/atom]')
plt.savefig('enthalpy.png', bbox_inches='tight')
