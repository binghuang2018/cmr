.. _oqmd12:

==========================================
One and two component references from OQMD
==========================================

GPAW calculations for 1 and 2 component systems from convex hull of
the OQMD_ database:

.. _OQMD: http://oqmd.org/


Please reference the following work if you use any of this data:

.. container:: article

    `Materials Design and Discovery with High-Throughput
    Density Functional Theory:
    The Open Quantum Materials Database (OQMD)`__

    Saal, J. E., Kirklin, S., Aykol, M., Meredig, B., and Wolverton, C.

    JOM 65, 1501-1509 (2013)

    __ http://dx.doi.org/10.1007/s11837-013-0755-4

.. container:: article

    `The Open Quantum Materials Database (OQMD):
    assessing the accuracy of DFT formation energies`__

    Kirklin, S., Saal, J.E., Meredig, B., Thompson, A., Doak, J.W.,
    Aykol, M., Rühl, S. and Wolverton, C.

    npj Computational Materials 1, 15010 (2015)

    __ http://dx.doi.org/10.1038/npjcompumats.2015.10

* Download reference state data: :download:`oqmd12.db`
* `Browse reference state data <https://cmrdb.fysik.dtu.dk/oqmd12>`_

.. contents::


Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


How the data was created
------------------------

The raw data comes from the SI here:

    https://journals.aps.org/prb/supplemental/10.1103/PhysRevB.96.024104

1) The 1 and 2 component systems on the convex hull are extracted with this
   script: :git:`~cmr/oqmd12/extract_from_si.py`.

2) Find the systems on the hull: :git:`~cmr/oqmd12/bin.py`.

3) Choose systems to calculate with GPAW: :git:`~cmr/oqmd12/prepare.py`.

4) GPAW calculations are done with this script: :git:`~cmr/oqmd12/run.py`.

5) Script for creating the database: :git:`~cmr/oqmd12/collect.py`.
