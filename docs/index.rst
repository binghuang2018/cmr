CMR
---

Each CMR project in the table below consists of an ASE-database and a project
page describing the data and showing examples of how to work with the data
using Python_ and ASE_.  Browse the databases by clicking the **[browse]**
links below or download the databases and explore them using ASE tools:

* :ref:`ase db <ase:ase-db>` command-line tool
* :mod:`ase.db` Python module
* :ref:`web-interface <ase:ase-db-web>`

.. `Browse all databases <https://cmrdb.fysik.dtu.dk>`_

.. _ASE: https://wiki.fysik.dtu.dk/ase/
.. _Python: https://www.python.org/

All CMR databases are licensed under a
`Creative Commons Attribution-ShareAlike 4.0
International License <https://creativecommons.org/licenses/by-sa/4.0/>`_
|bysa|

.. |bysa| image:: https://licensebuttons.net/l/by-sa/4.0/88x31.png
    :target: https://creativecommons.org/licenses/by-sa/4.0/
    :alt: Creative Commons License

---------

.. list-table::
   :class: frontpage

   * - .. image:: static/imp2d.png
          :align: center
          :height: 105px
          :target: imp2d/imp2d.html

       :ref:`imp2d`
       `[browse] <https://cmrdb.fysik.dtu.dk/imp2d>`__

     - .. image:: static/qpod.png
          :align: center
          :height: 105px
          :target: qpod/qpod.html

       :ref:`qpod`
       `[browse] <https://cmrdb.fysik.dtu.dk/qpod>`__

     - .. image:: static/c1db.png
          :align: center
          :height: 105px
          :target: c1db/c1db.html

       :ref:`c1db`
       `[browse] <https://cmrdb.fysik.dtu.dk/c1db>`__

   * - .. image:: static/c2db.png
          :align: center
          :height: 105px
          :target: c2db/c2db.html

       :ref:`c2db`
       `[browse] <https://cmrdb.fysik.dtu.dk/c2db>`__

     - .. image:: static/abse3.png
          :align: center
          :height: 124px
          :target: abse3/abse3.html

       :ref:`abse3`
       `[browse] <https://cmrdb.fysik.dtu.dk/abse3>`__

     - .. image:: static/oqmd123.png
          :align: center
          :height: 124px
          :target: oqmd123/oqmd123.html

       :ref:`oqmd123`
       `[browse] <https://cmrdb.fysik.dtu.dk/oqmd123>`__

   * - .. image:: static/bondmin.png
          :align: center
          :height: 124px
          :target: bondmin/bondmin.html

       :ref:`bondmin`

     - .. image:: static/lowdim.png
          :align: center
          :height: 124px
          :target: lowdim/lowdim.html

       :ref:`lowdim`
       `[browse] <https://cmrdb.fysik.dtu.dk/lowdim>`__

     - .. image:: static/a2bcx4.png
          :align: center
          :height: 124px
          :target: a2bcx4/a2bcx4.html

       :ref:`a2bcx4`
       `[browse] <https://cmrdb.fysik.dtu.dk/a2bcx4>`__

   * - .. image:: static/agau309.png
          :align: center
          :height: 124px
          :target: agau309/agau309.html

       :ref:`agau309`
       `[browse] <https://cmrdb.fysik.dtu.dk/agau309>`__

     - .. image:: static/pv_pec_oqmd.png
          :align: center
          :height: 124px
          :target: pv_pec_oqmd/pv_pec_oqmd.html

       :ref:`pv_pec_oqmd`
       `[browse] <https://cmrdb.fysik.dtu.dk/pv_pec_oqmd>`__

     - .. image:: static/abs3.png
          :align: center
          :height: 124px
          :target: abs3/abs3.html

       :ref:`abs3`
       `[browse] <https://cmrdb.fysik.dtu.dk/abs3>`__

   * - .. image:: static/adsorption.png
          :align: center
          :height: 124px
          :target: adsorption/adsorption.html

       :ref:`adsorption`
       `[browse] <https://cmrdb.fysik.dtu.dk/adsorption>`__

     - .. image:: static/solar.png
          :align: center
          :height: 124px
          :target: solar/solar.html

       :ref:`solar`
       `[browse] <https://cmrdb.fysik.dtu.dk/solar>`__

     - .. image:: static/abx2.png
          :align: center
          :height: 124px
          :target: abx2/abx2.html

       :ref:`abx2`
       `[browse] <https://cmrdb.fysik.dtu.dk/abx2>`__

   * - .. image:: static/c2dm.png
          :align: center
          :height: 124px
          :target: c2dm/c2dm.html

       :ref:`c2dm`

     - .. image:: static/vdwh.png
          :align: center
          :width: 230px
          :target: vdwh/vdwh.html

       :ref:`vdwh`

     - .. image:: static/organometal.png
          :align: center
          :height: 124px
          :target: organometal/organometal.html

       :ref:`organometal`
       `[browse] <https://cmrdb.fysik.dtu.dk/organometal>`__

   * - .. image:: static/dssc.png
          :align: center
          :height: 124px
          :target: dssc/dssc.html

       :ref:`dssc`
       `[browse] <https://cmrdb.fysik.dtu.dk/dssc>`__

     - .. image:: static/mp_gllbsc.png
          :align: center
          :height: 124px
          :target: mp_gllbsc/mp_gllbsc.html

       :ref:`mp_gllbsc`
       `[browse] <https://cmrdb.fysik.dtu.dk/mp_gllbsc>`__

     - .. image:: static/cubic_perovskites.png
          :align: center
          :height: 124px
          :target: cubic_perovskites/cubic_perovskites.html

       :ref:`cubic_perovskites`
       `[browse] <https://cmrdb.fysik.dtu.dk/cubic_perovskites>`__

   * - .. image:: static/low_symmetry_perovskites.png
          :align: center
          :height: 124px
          :target: low_symmetry_perovskites/low_symmetry_perovskites.html

       :ref:`low_symmetry_perovskites`
       `[browse] <https://cmrdb.fysik.dtu.dk/low_symmetry_perovskites>`__

     - .. image:: static/absorption_perovskites.png
          :align: center
          :height: 124px
          :target: absorption_perovskites/absorption_perovskites.html

       :ref:`absorption_perovskites`
       `[browse] <https://cmrdb.fysik.dtu.dk/absorption_perovskites>`__

     - .. image:: static/funct_perovskites.png
          :align: center
          :height: 124px
          :target: funct_perovskites/funct_perovskites.html

       :ref:`funct_perovskites`
       `[browse] <https://cmrdb.fysik.dtu.dk/funct_perovskites>`__

   * - .. image:: static/beef.png
          :align: center
          :height: 124px
          :target: beef/beef.html

       :ref:`beef`

     - .. image:: static/catapp.png
          :align: center
          :height: 124px
          :target: catapp/catapp.html

       :ref:`catapp1`
       `[browse] <https://cmrdb.fysik.dtu.dk/catapp>`__

     - .. image:: static/htgw.png
          :align: center
          :height: 124px
          :target: htgw/htgw.html

       :ref:`htgw`

   * - .. image:: static/oqmd12.png
          :align: center
          :height: 124px
          :target: oqmd12/oqmd12.html

       :ref:`oqmd12`
       `[browse] <https://cmrdb.fysik.dtu.dk/oqmd12>`__

     -
     -


All projects
------------

.. toctree::
    :maxdepth: 1

    imp2d/imp2d
    qpod/qpod
    c1db/c1db
    c2db/c2db
    abse3/abse3
    bondmin/bondmin
    lowdim/lowdim
    a2bcx4/a2bcx4
    agau309/agau309
    pv_pec_oqmd/pv_pec_oqmd
    oqmd123/oqmd123
    abs3/abs3
    adsorption/adsorption
    solar/solar
    abx2/abx2
    c2dm/c2dm
    vdwh/vdwh
    organometal/organometal
    dssc/dssc
    mp_gllbsc/mp_gllbsc
    cubic_perovskites/cubic_perovskites
    low_symmetry_perovskites/low_symmetry_perovskites
    absorption_perovskites/absorption_perovskites
    funct_perovskites/funct_perovskites
    beef/beef
    catapp/catapp
    htgw/htgw
    oqmd12/oqmd12


Older databases
---------------

.. toctree::
    :maxdepth: 1

    dcdft/dcdft
    tmfp06d/tmfp06d
    fcc111/fcc111
    compression/compression
    gbrv/gbrv
    g2/g2



About this web-page
-------------------

.. toctree::

    about
    server
