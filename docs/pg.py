"""
create role cmr;
alter role cmr with login;
alter role cmr createdb;
alter database c2db owner to cmr;
"""
from pathlib import Path
import subprocess
from ase.db import connect


password = 'ase'


def shell(cmd: str) -> None:
    print(cmd)
    subprocess.run(cmd, shell=True, check=True)


def psql(stmt: str) -> None:
    shell(f'psql -c "{stmt};" postgres')


def update(name: str, path) -> None:
    dbname = path.name[:-3]
    if 0:
        print(f'DROP DATABASE {dbname};')
        print(f'CREATE DATABASE {dbname};')
        return

    print(path, end=' ')
    db = connect(path)
    n = len(db)
    print(n, end=' ')

    # psql(f'DROP DATABASE {name}')
    # psql(f'CREATE DATABASE {name}')

    if dbname == 'c2db':
        def select(db):
            fi = [(row.formula, row.id) for row in db.select()]
            for f, id in sorted(fi):
                yield db.get(id=id)
    else:
        def select(db):
            yield from db.select()

    url = f'postgresql://ase:{password}@localhost:5432'
    with connect(f'{url}/{dbname}') as tmp:
        i = 0.0
        for row in select(db):
            # kvp = row.get('key_value_pairs', {})
            tmp.write(row, data=row.get('data'))  # , **kvp)
            i += 1
            if i >= n / 50:
                print(end='.', flush=True)
                i -= n / 50
        if dbname == 'c2db':
            tmp.metadata = db.metadata
    print('done')


if __name__ == '__main__':
    from cmr import downloads
    for name, files in downloads:
        if name in {'dcdft', 'beef'}:
            continue
        if name == 'adsorption':
            continue
        if name != 'c2db':
            continue
        for f in files:
            if not f.endswith('.db'):
                continue
            p = Path(name) / f
            update(name, p)
