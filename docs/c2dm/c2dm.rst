.. _c2dm:

Monolayer transition metal dichalcogenides and -oxides
======================================================

This database contains calculated structural and electronic properties of a
range of 2D materials. The database contains the results presented in the
following paper:

.. container:: article

    Rasmussen, F., Thygesen, K. S.

    `Computational 2D Materials Database: Electronic Structure of
    Transition-Metal Dichalcogenides and Oxides`__

    J. Phys. Chem. C, 2015, 119 (23), pp 13169–13183, April 30, 2015

    __ http://pubs.acs.org/doi/abs/10.1021/acs.jpcc.5b02950

* Download raw data: :download:`c2dm.db`


Electronic structure of 2D materials
------------------------------------

The structures were first relaxed using the PBE xc-functional and a 18x18x1
k-point sampling until all forces on the atoms where below 0.01 eV/Å. The rows
with xc='PBE' contains data from these calculations.

For materials that were found to be semiconducting in the PBE calculations we
furthermore performed calculations using the LDA and GLLB-SC xc functionals
and the lattice constants and atom positions found from the PBE calculation.
For these calculations we used a 30x30x1 k-point sampling. For the GLLB-SC
calculations we calculated the derivative discontinuity and have added this
contribution to the electronic band gaps. Data for these calculations are
found in rows with xc='GLLBSC' and xc='LDA', respectively.

Furthermore, we calculated the G0W0 quasiparticle energies using the
wavefunctions and eigenvalues from the LDA calculations and a plane-wave
cut-off energy of at least 150 eV. The quasiparticle energies where further
extrapolated to infinite cut-off energy via the methods described in the
paper. The LDA rows thus further have key-value pairs with the results from
the G0W0 calculations.


Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Example
-------

The following python script shows how to plot the positions of the VBM and CBM.

.. literalinclude:: plot_band_alignment.py

This produces the figure

.. image:: band-alignment.png
