.. _lowdim:

Definition of a scoring parameter to identify low-dimensional materials components
==================================================================================

.. container:: article

    Peter Mahler Larsen, Mohnish Pandey, Mikkel Strange, and Karsten W. Jacobsen

    `Definition of a scoring parameter to identify low-dimensional materials
    components`__

    Phys. Rev. Materials 3 034003, 2019

    __ https://doi.org/10.1103/PhysRevMaterials.3.034003

The data can be downloaded or browsed online:

* Download data: :download:`lowdim.db`
* `Browse data <https://cmrdb.fysik.dtu.dk/lowdim/>`_


.. contents::

Brief description
-----------------

The analyses of two databases are available here: the Inorganic Crystal
Structure Database (`ICSD <https://icsd.products.fiz-karlsruhe.de/en/>`_)
and the Crystallography Open Database (`COD
<https://www.crystallography.net/cod/>`_). The COD database contains the full
structures, dimensionality scores, and publication information, for every
structure with at most 200 atoms. For copyright reasons, the atomic positions
of the ICSD entries cannot be shown here; only the formulae, ICSD code, and
dimensionality scores are shown.


Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Example
-------

.. image:: lowdim.png
.. literalinclude:: lowdim.py
