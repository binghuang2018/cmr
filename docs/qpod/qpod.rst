.. _qpod:

=====================================================
Quantum Point Defects in 2D Materials (QPOD) Database
=====================================================

.. image:: ../static/qpod.png
   :width: 50 %

If you are using data from this database in your research, please cite the
following papers:

.. container:: article

    Bertoldo, F., Ali, S., Manti, S., Thygesen, K. S.

    `Quantum point defects in 2D materials - the QPOD database`__

    *npj Comput Mater* **8**, 56 (2022)

    __ https://doi.org/10.1038/s41524-022-00730-w


* `Browse data <https://cmrdb.fysik.dtu.dk/qpod>`_
* Download database: :download:`qpod.db`


.. contents::

Brief description
=================

The database contains structural, thermodynamic, electronic, magneto-optical,
of 500 distinct intrinsic point defects in 2D Materials. The properties are
calculated by density functional theory (DFT) as implemented in the GPAW_
electronic structure code. The workflow was constructed using the Atomic
Simulation Recipes (ASR_) and executed with the MyQueue_ task manager.
For each defect, different charge states are available resulting in more than
2000 rows for this database.

.. image:: workflow_qpod.png


Overview of methods and parameters used
=======================================

If a parameter is not specified at a given step, its value equals that
of the last step where it was specified:

.. list-table::
  :widths: 10 25
  :header-rows: 1

  * - Workflow step(s)
    - Parameters

  * - Structure and energetics(*)
    - `k`-point density = 6.0/Å\ `^{-1}`;
      Fermi smearing = 0.02 eV; PW cutoff = 800 eV;
      xc functional = PBE; maximum force = 0.01 eV/Å;

  * - PBE electronic properties (*)
    - `k`-point density = `12.0/\mathrm{Å}^{-1}`

(*) *For the cases with convergence issues, we increased the smearing to 0.05, 0.08, 0.1 eV.*


Funding acknowledgements
========================

The QPOD project has received funding from the Danish National Research
Foundation’s  Center for Nanostructured Graphene (CNG) and the European
Research Council (ERC) under the European Union’s Horizon 2020 research and
innovation program Grant No. 773122 (LIMA) and Grant agreement No. 951786
(NOMAD CoE).


Versions
========


.. list-table::
    :widths: 3 1 10

    * - Version
      - rows
      - comment

    * - 2022-03-31
      - 2061
      - Initial release

    * - 2022-06-09
      - 2061
      - Added spin coherence times for 69 triplet centers in 45 different host materials
        (see related manuscript `here <https://arxiv.org/abs/2205.07604>`_)

    * - 2022-07-11
      - 2061
      - Web panel update for improved readability


Examples
========

Ionization potentials (IPs) and electron affinities (EAs)
---------------------------------------------------------

The following Python script shows how to plot IPs and EAs with and
without relaxation effects included for systems within a given list
of hosts (similar to Fig. 12 from the QPOD paper).

.. literalinclude:: fig12.py

This produces the figure

.. image:: fig12.png


Tools for creating the "QPOD" database
============================================================

Requirements
------------

Python_ 3.7+ and up-to-date versions of ASE_, ASR_ and GPAW_.


Workflow
--------

The workflow was executed using MyQueue_. The corresponding workflow script will
follow shortly.

.. _MyQueue: https://myqueue.readthedocs.io/


.. _Python: https://www.python.org/
.. _ASE: https://wiki.fysik.dtu.dk/ase/
.. _GPAW: https://wiki.fysik.dtu.dk/gpaw/
.. _ASR: https://asr.readthedocs.io/en/latest/
