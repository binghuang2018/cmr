import csv
import functools
import importlib
from pathlib import Path
from urllib.request import urlretrieve

from ase.db import connect
from ase.utils.sphinx import create_png_files, git_role_tmpl, mol_role

from cmr import downloads, project2folder

url = 'https://cmr.fysik.dtu.dk/'
repo = 'https://gitlab.com/camd/cmr/blob/master/'


def download():
    """Download the big data files and front-page images for each project."""
    for project, filenames in downloads:
        for filename in filenames:
            path = Path(project) / filename
            if not path.is_file():
                print('Downloading', path)
                if filename.endswith('.png'):
                    urlretrieve(f'{url}/_images/{filename}', path)
                else:
                    urlretrieve(f'{url}/_downloads/{filename}', path)
        path = Path('static') / f'{project}.png'
        if not path.is_file():
            print('Downloading', path)
            urlretrieve(f'{url}/_static/{project}.png', path)


def create_key_description_tables():
    for project, folder in project2folder.items():
        if project == folder:
            x = ''
        else:
            x = f'_{project}'
        custom = Path(f'../cmr/{folder}/custom{x}.py')
        if not custom.is_file():
            continue
        out = Path(folder) / f'keytable{x}.csv'
        if out.is_file() and out.stat().st_mtime >= custom.stat().st_mtime:
            continue
        data = {}
        rst = (Path(folder) / (folder + '.rst')).read_text()
        if 'keytable.csv' not in rst:
            continue
        print(custom, '->', out)
        exec(custom.read_text(), data)
        kd = data.get('key_descriptions')
        if kd is None:
            mod = importlib.import_module(f'cmr.{folder}.custom{x}')
            create_function = getattr(mod, 'create_key_descriptions')
            db = connect(f'{folder}/{project}.db')
            kd = create_function(db)

        if project == 'c2db':
            kd = {key: value for key, value in kd.items()
                  if not key.startswith('has_asr_')}

        with out.open('w') as o:
            writer = csv.writer(o)
            writer.writerow(['key', 'description', 'unit'])
            for key, (short, long, unit) in sorted(kd.items()):
                writer.writerow([key,
                                 long or short,
                                 unit.replace('|', r'\|')])


def create_test_database():
    cmr = Path('cmr.db')
    if cmr.is_file():
        return
    db = connect(cmr)
    for project, filenames in downloads:
        for filename in filenames:
            if filename.endswith('.db'):
                path = Path(project) / filename
                db2 = connect(path)
                for row in db2.select(limit=1):
                    db.write(row, project_name=filename[:-3], data=row.data,
                             **row.get('key_value_pairs', {}))
                if project == 'c2db':
                    db.metadata = db2.metadata


def setup(app):
    """Sphinx entry point."""
    app.add_role('mol', mol_role)
    app.add_role('git', functools.partial(git_role_tmpl, repo))
    download()
    create_key_description_tables()
    create_png_files()
    create_test_database()


if __name__ == '__main__':
    download()
