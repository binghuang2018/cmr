.. _c1db:

=================================================================
A systematic computational database for one-dimensional materials
=================================================================

If you are using data from this database in your research, please cite the
following papers:

.. container:: article

    Computational exfoliation of atomically thin 1D materials with
    application to Majorana bound states

    Hadeel Moustafa, Peter Mahler Larsen, Morten N. Gjerding, Jens
    Jørgen Mortensen, Kristian S. Thygesen, Karsten W. Jacobsen

    `preprint`__

    __ https://arxiv.org/abs/2204.00472

* Download data: :download:`c1db.db`
* `Browse data <https://cmrdb.fysik.dtu.dk/c1db>`_ (WIP)


.. contents::

Brief description
=================

The database includes one-dimensional materials with structural,
thermodynamic, electronic, magnetic, and optical properties. The materials are
studied in bulk as well as as isolated one-dimensional components. Density
functional theory (DFT), as implemented in the GPAW electronic structure code,
is used to calculate the properties. The Atomic Simulation Recipes (ASR_) were
used to create the workflow, which was then performed using the MyQueue_ task
management. Below you'll find the process script as well as a table with the
numerical parameters used to calculate the various attributes.


Funding acknowledgements
========================

The C1DB project has received funding from the VILLUM Center for Science of
Sustainable Fuels and Chemicals and the European Research Council (ERC) under
the European Union’s Horizon 2020 research and innovation program Grant No.
773122 (LIMA) and Grant agreement No. 951786 (NOMAD CoE)


Overview of methods and parameters used
=======================================

If a parameter is not specified at a given step, its value equals that
of the last step where it was specified:

.. list-table::
  :widths: 10 25
  :header-rows: 1

  * - Workflow step(s)
    - Parameters

  * - Structure and energetics
    - vacuum = 16 Å; `k`-point density = 6.0/Å\ `^{-1}`;
      Fermi smearing = 0.05 eV; PW cutoff = 800 eV;
      xc functional = PBE, PBE-D3; maximum force = 0.01 eV/Å;
      maximum stress = 0.002 eV/Å\ `^3`

  * - PBE electronic properties
    - `k`-point density = `12.0/\mathrm{Å}^{-1}`

  * - HSE band structure
    - HSE06\ @PBE; `k`-point density = 8.0/Å\ `^{-1}`


Key-value pairs
===============

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Examples
========

WIP ...


.. _MyQueue: https://myqueue.readthedocs.io/
.. _ASR: https://asr.readthedocs.io/
