# creates: MoS2_on_hBN_Eb.svg
import os
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from qeh import Heterostructure

if not Path('chi-data').is_dir():
    os.system('tar -xf chi-data.tar.gz')
if not Path('H-MoS2-chi.npz').is_file():
    os.system('cp chi-data/H-MoS2-chi.npz .')
if not Path('BN-chi.npz').is_file():
    os.system('cp chi-data/BN-chi.npz .')

# Calculate binding energy of the lowest excitonic state in MoS2
d_BN = 3.22  # hBN-hBN distance
d_MoS2_BN = 4.69  # MoS2-hBN distance
d_MoS2 = 6.15

n_hBN = 10

Eb_list = []

for n in range(0, n_hBN + 1, 1):
    if n == 0:
        d = []
    else:
        d = [d_MoS2_BN] + [d_BN for i in range(n - 1)]

    hl_array = np.zeros(2 * (n + 1))  # the factor 2 accounts for
    el_array = np.zeros(2 * (n + 1))  # the dipole component
    hl_array[0] = 1  # the hole is placed on MoS2
    el_array[0] = 1  # the electron is  placed on MoS2

    HS = Heterostructure(structure=['1H-MoS2', '%dBN' % n],  # set up structure
                         d=d,  # layer distance array
                         wmax=0,
                         d0=d_MoS2)

    ee, ev = HS.get_exciton_binding_energies(eff_mass=0.27,  # exciton eff_mass
                                             e_distr=el_array,
                                             h_distr=hl_array)

    # ee is the list of eigenvalues, sorted from the lowest to the highest
    Eb_list.append(-ee[0].real)

n = np.arange(0, n_hBN + 1, 1)
plt.plot(n, Eb_list, '-s')
plt.title('Exciton Binding Energy', fontsize=20)
plt.xlabel(r'no. hBN', fontsize=20)
plt.ylabel(r'$E_{\rm b}$ (eV)', fontsize=20)
plt.savefig('MoS2_on_hBN_Eb.svg')
