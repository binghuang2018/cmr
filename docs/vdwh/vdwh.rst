.. _vdwh:

Van der Waals heterostructures
==========================================

This database contains the dielectric building blocks of 51 transition metal
dichalcogenides and oxides, hexagonal boron nitride, and graphene at ten
different doping levels. These results are used to calculate the dielectric
function of van der Waals heterostructures that is build as combinations of
these materials.  In the following the reference for the original article:

.. container:: article

    Andersen, K., Latini, S., Thygesen, K. S.

    `Dielectric Genome of van der Waals Heterostructures.`__

    Nano Letters (2015)

    __ http://dx.doi.org/10.1021/acs.nanolett.5b01251

The data can be downloaded and used together with qeh_ package
as shown below. The dielectric building blocks are obtained from
the files:

* Building blocks for the transition metal dichalcogenides
  (do not use the doped graphene building block in this file as
  it is poorly converged for low frequencies): :download:`chi-data.tar.gz`
* Optimised building blocks for doped graphene at various doping levels
  based on an analytical approximation for the bandstructure at small
  energies (should be very accurate): :download:`graphene-data.tar.gz`
* (Building blocks used in the qeh_ package on PyPI. Do not use these
  unless you know what you are doing: :download:`chi-data-v2.tar.gz`)

The electronic band gap and band edge positions of the transition metal
dichalcogenides and oxides are available from the 2D materials database
:ref:`c2db`, where both metallic and semiconducting materials have been
studied.

The dielelectric building blocks have presently only been calculated for the
51 semiconducing materials found to be stable in the study above.


Dielectric building blocks
---------------------------

As explained above, the dielectric building blocks of the materials are
obtained from the files :download:`chi-data.tar.gz` and
:download:`graphene-data.tar.gz`. These files contain a numpy zip
file for each material called: *<name>-chi.npz*, where the name consists of
the phase (*'H'* for 2H and *'T'* for 1T) followed by the chemical formula,
such that the file for 2H-MoS2 is called: *H-MoS2-chi.npz*. The files contain
the data described in the table below:

=====================  =======================================================
quantity               description
=====================  =======================================================
q                      Grid for parallel momentum transfers

frequencies            frequency grid

chi_monopole           Monopole density response function, array of dimension
                       (q x frequencies)

chi_dipole             Dipole density response function, array of dimension
                       (q x frequencies)

z                      Real space grid perpendicular to the layer

drho_monopole          Monopole induced density, array of dimension  (q x z)

drho_dipole            Dipole induced density, array of dimension (q x z)
=====================  =======================================================


The quantum electrostatic heterostructure (QEH) model
-----------------------------------------------------

The dielectric function of van der waals heterostructures and associated
properties can be calculated with the Python qeh_ package available on PyPI.

.. _qeh: https://qeh.readthedocs.io/en/latest/

As an example the macroscopic dielectric function of multilayer MoS2 can be
obtained. First we extract the data for MoS2 from the database::

    $ tar xf chi-data.tar.gz
    $ cp chi-data/H-MoS2-chi.npz .

Then the Heterostructure module is used to calculate the dielectric function
for one to 20 layers of MoS2.

.. literalinclude:: multi_MoS2.py
    :start-after: Calculate

Which should return this result:

.. image:: epsMoS2.svg

The structure is set up with the structure parameter, that should be a list of
speciems within the structure. In this case ``structure=['20MoS2']`` gives 20
layers of MoS2. As an example a more complicated heterostructure of graphene,
hBN and MoS2 can be set up with: ``structure=['3H-MoS2', '2BN','graphene',
'2BN', '3H-MoS2']``, which will give one layer of graphene sandwiched between
two layers of hBN and three layers of MoS2 on each side. The d parameter
should be a list of the distance bewteen all neigboring layers, with a length
equal to N-1, where N is the number of layers in the structure.


As a further example, the QEH model can be combined with the Mott-Wannier (MW)
equation for excitons in 2D semiconductors. In particular the case of MoS2
supported on a hBN substrate of varying thickness is considered. First the
MoS2 and hBN building blocks should be copied to the working directory,
namely::

    $ tar xf chi-data.tar.gz
    $ cp chi-data/H-MoS2-chi.npz .
    $ cp chi-data/BN-chi.npz .

Then the binding energy of the lowest excitonic state in MoS2 is calculated in
the following script as a function of hBN layers is obtained.

.. literalinclude:: MoS2_on_hBN.py
    :start-after: Calculate

The results are shown in the following figure:

.. image:: MoS2_on_hBN_Eb.svg

The presence of hBN increases screens the electron-hole interaction and
therefore reduces the exciton binding energy. More information on the effect
of environmental screening on excitons in van der Waals heterostructures can
be found in the following paper:

.. container:: article

    Latini, S., Olsen, T. and Thygesen, K. S.

    `Excitons in van der Waals Heterostructures: The important role of
    dielectric screening`__

    Phys. Rev. B (2015)

    __ https://doi.org/10.1103/PhysRevB.92.245123
