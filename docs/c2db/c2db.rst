.. _c2db:

==========================================
Computational 2D Materials Database (C2DB)
==========================================

If you are using data from this database in your research, please cite the
following papers:

.. container:: article

    `The Computational 2D Materials Database: High-Throughput Modeling
    and Discovery of Atomically Thin Crystals`__

    Sten Haastrup, Mikkel Strange, Mohnish Pandey, Thorsten Deilmann,
    Per S. Schmidt, Nicki F. Hinsche, Morten N. Gjerding, Daniele Torelli,
    Peter M. Larsen, Anders C. Riis-Jensen, Jakob Gath, Karsten W. Jacobsen,
    Jens Jørgen Mortensen, Thomas Olsen, Kristian S. Thygesen

    2D Materials 5, 042002 (2018)

    __ http://iopscience.iop.org/article/10.1088/2053-1583/aacfc1

.. container:: article

    `Recent Progress of the Computational 2D Materials Database (C2DB)`__

    M. N. Gjerding, A. Taghizadeh, A. Rasmussen, S. Ali, F. Bertoldo, T.
    Deilmann, U. P. Holguin, N. R. Knøsgaard, M. Kruse, A. H. Larsen,
    S. Manti, T. G. Pedersen, T. Skovhus, M. K. Svendsen, J. J. Mortensen,
    T. Olsen, K. S. Thygesen

    2D Materials 8, 044002 (2021)

    __ https://doi.org/10.1088/2053-1583/ac1059

* `Browse data <https://cmrdb.fysik.dtu.dk/c2db>`_
* The database is provided upon request.


.. contents::

Brief description
=================

The database contains structural, thermodynamic, elastic, electronic,
magnetic, and optical properties of around 4000 two-dimensional (2D)
materials distributed over more than 40 different crystal structures. The
properties are calculated by density functional theory (DFT) and many-body
perturbation theory (`G_0W_0` and the Bethe- Salpeter Equation for around 300
materials) as implemented in the GPAW_ electronic structure code. The workflow
was constructed using the Atomic Simulation Recipes (ASR_) and executed with
the MyQueue_ task manager. The workflow script and a table with the numerical
settings employed for the calculation of the different properties are
provided below.

.. image:: workflow.png


Overview of methods and parameters used
=======================================

If a parameter is not specified at a given step, its value equals that
of the last step where it was specified:

.. list-table::
  :widths: 10 25
  :header-rows: 1

  * - Workflow step(s)
    - Parameters

  * - Structure and energetics(*) (1-4)
    - vacuum = 15 Å; `k`-point density = 6.0/Å\ `^{-1}`;
      Fermi smearing = 0.05 eV; PW cutoff = 800 eV;
      xc functional = PBE; maximum force = 0.01 eV/Å;
      maximum stress = 0.002 eV/Å\ `^3`; phonon displacement = 0.01Å

  * - Elastic constants (5)
    - `k`-point density = `12.0/\mathrm{Å}^{-1}`; strain = `\pm`\ 1%

  * - Magnetic anisotropy (6)
    - `k`-point density = `20.0/\mathrm{Å}^{-1}`;
      spin-orbit coupling = True

  * - PBE electronic properties (7-10 and 12)
    - `k`-point density = `12.0/\mathrm{Å}^{-1}`
      (`36.0/\mathrm{Å}^{-1}` for DOS)

  * - Effective masses (11)
    - `k`-point density = `45.0/\mathrm{Å}^{-1}`; finite difference

  * - Deformation potential (13)
    - `k`-point density = 12.0/Å\ `^{-1}`; strain = `\pm`\ 1%

  * - Plasma frequency (14)
    - `k`-point density = 20.0/Å\ `^{-1}`; tetrahedral interpolation

  * - HSE band structure (8-12)
    - HSE06\ @PBE; `k`-point density = 12.0/Å\ `^{-1}`

  * - `G_0W_0` band structure (8, 9)
    - `G_0W_0`\ @PBE; `k`-point density = `5.0/\mathrm{Å}^{-1}`;
      PW cutoff = `\infty` (extrapolated from 170, 185 and 200 eV);
      full frequency integration;
      analytical treatment of `W({q})` for small `q`;
      truncated Coulomb interaction

  * - RPA polarisability (15)
    - RPA\ @PBE; `k`-point density = `20.0/\mathrm{Å}^{-1}`;
      PW cutoff = 50 eV; truncated Coulomb interaction;
      tetrahedral interpolation

  * - BSE absorbance (16)
    - BSE\ @PBE with `G_0W_0` scissors operator;
      `k`-point density = `20.0/\mathrm{Å}^{-1}`;
      PW cutoff = 50 eV; truncated Coulomb interaction;
      at least 4 occupied and 4 empty bands

(*) *For the cases with convergence issues, we set a \(k\)-point density of
9.0 and a smearing of 0.02 eV*.


Funding acknowledgements
========================

The C2DB project has received funding from the Danish National Research
Foundation’s  Center for Nanostructured Graphene (CNG) and the European
Research Council (ERC) under the European Union’s Horizon 2020 research and
innovation program Grant No. 773122 (LIMA) and Grant agreement No. 951786
(NOMAD CoE).


Versions
========


.. list-table::
    :widths: 3 1 10

    * - Version
      - rows
      - comment

    * - 2018-06-01
      - 1888
      - Initial release

    * - 2018-08-01
      - 2391
      - Two new prototypes added

    * - 2018-09-25
      - 3084
      - New prototypes

    * - 2018-12-10
      - 3331
      - Some BSE spectra recalculated due to small bug affecting
        absorption strength of materials with large spin-orbit couplings

    * - 2021-04-22
      - 4049
      - Major update including several hundreds new monolayers
        from experimentally known crystals (from the ICSD/COD
        databases), new crystal prototype characterization scheme,
        improved properties (stiffness tensor, effective masses, optical
        absorbance), and new properties (exfoliation energies, Bader
        charges, spontaneous polarisations, Born charges, infrared
        polarisabilities, piezoelectric tensors,  band topology
        invariants, exchange couplings, Raman- and second harmonic
        generation spectra). See Gjerding et al. for a full description.

    * - 2021-06-24
      - 4056
      - Updated MARE for effective masses.

    * - 2022-09-08
      - 15686
      - Added 11.630 materials from lattice decoration and a
        deep generative model
        (see `arXiv:2206.12159 <https://arxiv.org/abs/2206.12159>`__).
        Corrected symmetry threshold used to define band structure
        path to be consistent with the threshold used to define the
        space group.


Key-value pairs
===============

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Examples
========

Band alignment
--------------

The following Python script shows how to plot the VBM and CBM.

.. literalinclude:: plot_band_alignment.py

This produces the figure

.. image:: band-alignment.png


Polarizability
--------------

In this example we reproduce figure 18 from
`Gjerding et al. <https://arxiv.org/abs/2102.03029>`_.
See :class:`asr.c2db.polarizability.Result` and
:class:`asr.c2db.infraredpolarizability.Result` for more information.

.. literalinclude:: fig18.py

The figure shows the total polarizability, including both electrons and
phonons, of monolayer hBN in the infrared frequency regime.

.. image:: fig18.png


Extracting VBM and CBM positions in the BZ
------------------------------------------

In this example, we want to extract the **k**-points of HSE valence band
maximum and conduction band minimum.  We do that by creating an instance of
:class:`asr.c2db.hse.Result`.  Once we have a :class:`~asr.c2db.hse.Result`
object we can get the data from the attributes
:attr:`~asr.c2db.hse.Result.kvbm` and :attr:`~asr.c2db.hse.Result.kcbm`:

.. literalinclude:: bzpos.py


Tools for creating the "Computational 2D materials" database
============================================================

Requirements
------------

Python_ 3.6+ and up-to-date versions of ASE_, ASR_ and GPAW_.


Workflow
--------

The workflow for each material is described in this
:download:`workflow.py <../../cmr/c2db/workflow.py>` file

As an example, let's do MoS\ `_2` in the ``CdI2`` prototype (T-phase).
Create a main folder and a subfolder::

    $ mkdir TMD
    $ cd TMD
    $ mkdir MoS2-CdI2

Create a ``structure.json`` file containing something close to the groundstate
structure::

    $ python3
    >>> from ase.build import mx2
    >>> a = mx2('MoS2', '1T', a=3.20, thickness=3.17, vacuum=7.5)
    >>> a.write('MoS2-CdI2/structure.json')

Start the workflow with::

    $ mq workflow ~/cmr/docs/c2db/workflow.py MoS2-CdI2/

.. note::

    The `mq` tool is described here_

.. _MyQueue:
.. _here: https://myqueue.readthedocs.io/

Once the first part of the workflow has finished,
one or more folders will appear (``nm/``, ``fm/``, ``afm/``) if the material
was thermodynimically stable.  You can then run the remaining part of the
workflow::

    $ mq workflow ~/cmr/docs/c2db/workflow.py MoS2-CdI2/*m/

When everything is ready you can collect the results in a database file and
inspect the results in a web-browser::

    $ python3 -m asr.database.fromtree MoS2-CdI2/*m/
    $ python3 -m asr.app

.. image:: c2db-db.png

.. _Python: https://www.python.org/
.. _ASE: https://wiki.fysik.dtu.dk/ase/
.. _GPAW: https://wiki.fysik.dtu.dk/gpaw/
.. _ASR: https://asr.readthedocs.io/en/latest/
