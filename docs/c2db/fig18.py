# creates: fig18.png
import matplotlib.pyplot as plt
from asr.infraredpolarizability import \
    Result as InfraredPolarizabilityResult
from asr.polarizability import Result as PolarizabilityResult
from ase.db import connect
from scipy.interpolate import interp1d

db = connect('c2db.db')
row = db.get(formula='BN')

dct = row.data['results-asr.infraredpolarizability.json']
irpol = InfraredPolarizabilityResult(**dct['kwargs'])
freq = irpol.omega_w
iralpha = irpol.alpha_wvv[:, 0, 0]  # xx-component

dct = row.data['results-asr.polarizability.json']
elpol = PolarizabilityResult(**dct['kwargs'])
x = elpol.frequencies
y = elpol.alphax_w
# Interpolate to IR frequency grid:
elalpha = (interp1d(x, y.real)(freq) +
           interp1d(x, y.imag)(freq) * 1j)

alpha = iralpha + elalpha

ax = plt.subplot()
ax.plot(freq * 1000, alpha.real, label='real')
ax.plot(freq * 1000, alpha.imag, label='imag')
ax.set_xlabel('Energy [meV]')
ax.set_ylabel(r'Polarizability [$\mathrm{\AA}$]')
ax.set_xlim(0, 500)
ax.legend()
plt.tight_layout()
plt.savefig('fig18.png')
