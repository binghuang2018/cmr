from asr.database import connect

db = connect('c2db.db')

# Select a row that has HSE06 data:
for row in db.select(has_asr_hse=True):
    break

record = row.cache.get(name='asr.c2db.hse:main')
result = record.result

print(result.kvbm)
print(result.kcbm)
