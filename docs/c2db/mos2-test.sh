#!/usr/bin/bash
set -e  # stop if there are errors
cd ~/CMR-C2DB-MOS2-TEST
mkdir TEST
cd TEST
echo "
module purge
unset PYTHONPATH
module load Python
module load GPAW-setups/0.9.20000
module load libxc/4.3.4-GCC-10.2.0
module load libvdwxc/0.4.0-foss-2020b
" > modules.sh
. ./modules.sh
python3 -m venv venv
cd venv
. bin/activate
which pip
which python
pip install -U pip -qq
mv bin/activate old
mv ../modules.sh bin/activate
cat old >> bin/activate
rm old
pip install git+https://gitlab.com/ase/ase.git
pip install git+https://gitlab.com/asr-dev/asr.git@old-master
pip install myqueue ase-ext
git clone git@gitlab.com:camd/cmr
git clone git@gitlab.com:gpaw/gpaw
cd gpaw
cp doc/platforms/Linux/Niflheim/siteconfig-foss.py siteconfig.py
pip install -e .
cd ../..
mq init
mkdir MoS2-1T MoS2-2H
python -c "from ase.build import mx2;
a = mx2('MoS2', '1T', a=3.20, thickness=3.17, vacuum=5.0);
a.write('MoS2-1T/unrelaxed.json');
a = mx2('MoS2', '2H', a=3.20, thickness=3.17, vacuum=5.0);
a.write('MoS2-2H/unrelaxed.json')"
mq workflow venv/cmr/cmr/c2db/flow.py MoS2-*/
