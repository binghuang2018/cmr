import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from ase.db import connect

sns.set(style="white")
sns.set_context('talk')

# Atoms names:
elements_x = ['Ni', 'Pt', 'Pd', 'Au', 'Ag', 'Cu', 'Mg', 'Hg', 'Cd',
              'Zn', 'Be', 'Tl', 'In', 'Al',
              'Ga', 'Pb', 'Sn', 'Ge', 'Si', 'B', 'Bi', 'Sb', 'As', 'P', 'Te']

elements_y = ['Be', 'Tl', 'In', 'Al',
              'Ga', 'Pb', 'Sn', 'Ge', 'Si', 'B', 'Bi', 'Sb', 'As', 'P', 'Te']


# Define the prototypes and assign them a color code
prototypes = ['NH4CdCl3', 'PbPS3', 'MnPSe3', 'BaNiO3', 'Al2S3', 'FePS3']
colors = {'NH4CdCl3': 1,
          'PbPS3': 2,
          'MnPSe3': 3,
          'BaNiO3': 4,
          'Al2S3': 5,
          'FePS3': 6}


# Get data for the plot
db = connect('abse3.db')

data = np.zeros((len(elements_x), len(elements_y)))
for i, A in enumerate(elements_x):
    for j, B in enumerate(elements_y):
        name = '{}{}Se3'.format(A, B)
        de_min = 100000
        if db.count('name=' + name) == 0:
            continue
        for row in db.select('name=' + name):
            if row.pbe_hof < de_min:
                de_min = row.pbe_hof
                prototype = row.prototype

        data[i, j] = colors[prototype]

data = data.T
# Set up the matplotlib figure
f, ax = plt.subplots()

# Generate a custom diverging colormap
cmap = ['white', '#3F3075',
        'tab:orange', 'tab:olive',
        'tab:blue', 'tab:purple', 'tab:pink']

# Draw the heatmap with the mask and correct aspect ratio
p = sns.heatmap(data, cmap=cmap, square=True, linewidths=.5, cbar=False)

# Place the plot ticks correctly
x = [i + 0.5 for i in range(len(elements_x))]
y = [i + 0.5 for i in range(len(elements_y))]

plt.xticks(x, elements_x, rotation='vertical')
plt.yticks(y, elements_y, rotation='horizontal')

# Add labels
plt.xlabel('B atom')
plt.ylabel('A atom')

# Handle the legend
handles = []
for prototype in prototypes:
    i = colors[prototype]
    handles.append(mpatches.Patch(color=cmap[i], label=prototype))


plt.tight_layout()

# Save the figure
plt.savefig('logo.svg')
