# creates: abse3_heatmap.svg
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from ase.db import connect

sns.set(style='white')


# Atoms names:
elements = ['Cs', 'Rb', 'K', 'Na', 'Li', 'Ba', 'Sr', 'Ca',
            'Y', 'Sc', 'La', 'Zr', 'Hf', 'Ti',
            'Nb', 'Ta', 'V', 'Mo', 'W', 'Re',  # 'Mn', 'Fe',
            'Os', 'Ru',  # 'Co',
            'Ir', 'Rh',
            'Ni', 'Pt', 'Pd', 'Au', 'Ag', 'Cu', 'Mg', 'Hg', 'Cd', 'Zn',
            'Be', 'Tl', 'In', 'Al',
            'Ga', 'Pb', 'Sn', 'Ge', 'Si', 'B', 'Bi', 'Sb', 'As', 'P', 'Te']

# Define the prototypes and assign them a color code
prototypes = ['NH4CdCl3', 'PbPS3', 'MnPSe3', 'BaNiO3', 'Al2S3', 'FePS3']
colors = {'NH4CdCl3': 1,
          'PbPS3': 2,
          'MnPSe3': 3,
          'BaNiO3': 4,
          'Al2S3': 5,
          'FePS3': 6}

# Get data for the plot
db = connect('abse3.db')

data = np.zeros((len(elements), len(elements)))
for i, A in enumerate(elements):
    for j, B in enumerate(elements):
        name = f'{A}{B}Se3'
        de_min = 100000
        if db.count('name=' + name) == 0:
            continue
        for row in db.select('name=' + name):
            if row.pbe_hof < de_min:
                de_min = row.pbe_hof
                prototype = row.prototype

        data[i, j] = colors[prototype]


# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(10, 7.5))

# Generate a custom diverging colormap
cmap = ['white', '#3F3075',
        'tab:orange', 'tab:olive',
        'tab:blue', 'tab:purple', 'tab:pink']

# Draw the heatmap with the mask and correct aspect ratio
p = sns.heatmap(data, cmap=cmap, square=True, linewidths=.5, cbar=False)

# Place the plot ticks correctly
x = [i + 0.5 for i in range(49)]
plt.xticks(x, elements, rotation='vertical')
plt.yticks(x, elements, rotation='horizontal')

# Add labels
plt.xlabel('B atom')
plt.ylabel('A atom')

# Handle the legend
handles = []
for prototype in prototypes:
    i = colors[prototype]
    handles.append(mpatches.Patch(color=cmap[i], label=prototype))

plt.legend(handles=handles, bbox_to_anchor=(1.05, 1), loc=2)
plt.tight_layout()

# Save the figure
plt.savefig('abse3_heatmap.svg')
