.. _pv_pec_oqmd:

Screening for PV and PEC materials using the OQMD database
==========================================================

.. container:: article

    Korina Kuhar, Mohnish Pandey,
    Kristian Sommer Thygesen, Karsten Wedel Jacobsen

    `High-Throughput Computational Assessment of Previously Synthesized
    Semiconductors for Photovoltaic and Photoelectrochemical Devices.`__

    ACS Energy Lett., 2018, 3 (2), pp 436–446

    __ https://dx.doi.org/10.1021/acsenergylett.7b01312


.. contents::


The data
--------

* Download database: :download:`pv_pec_oqmd.db`
* `Browse data <https://cmrdb.fysik.dtu.dk/pv_pec_oqmd>`_


Key-value pairs
---------------


.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Example
-------

Select rows::

    $ ase db pv_pec_oqmd.db "0.1<=PBE_gap<=2" -n
    1629 rows
    $ ase db pv_pec_oqmd.db "0.1<=PBE_gap<=2,magnetic=0" -n
    929 rows
    $ ase db pv_pec_oqmd.db "0.1<=PBE_gap<=2,magnetic=0,0.5<GLLB_ind<2.5" -n
    519 rows
    $ ase db pv_pec_oqmd.db "0.1<=PBE_gap<=2,magnetic=0,0.5<GLLB_ind<2.5,m_e<1,m_h<1" -n
    222 rows
    $ ase db pv_pec_oqmd.db "0.1<=PBE_gap<=2,magnetic=0,0.5<GLLB_ind<2.5,m_e<1,m_h<1,defect_tolerant=1" -n
    74 rows


Same thing in Python:

.. literalinclude:: table1.py

.. csv-table::
    :file: table1.csv
    :header-rows: 1
