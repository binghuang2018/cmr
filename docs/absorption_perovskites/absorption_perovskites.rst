.. _absorption_perovskites:

Absorption spectra of perovskites
=================================

We calculate the optical properties of a set of oxides, oxynitrides, and
organometal halide cubic and layered perovskites with a bandgap in the visible
part of the solar spectrum.

.. container:: article

   Ivano E. Castelli, Kristian S. Thygesen, and Karsten W. Jacobsen

    `Calculated optical absorption of different perovskite phases.`__

    Journal of Materials Chemistry A, printed online.

    __ http://dx.doi.org/10.1039/c5ta01586c

* :download:`Download raw data <absorption_perovskites.db>`
* `Browse data
  <https://cmrdb.fysik.dtu.dk/absorption_perovskites>`_


Key-value pairs
---------------

.. csv-table::
    :file: keytable.csv
    :header-rows: 1
    :widths: 3 10 1


Absorption spectrum of AgNbO3
-----------------------------

.. literalinclude:: spectrum.py

.. image:: spectrum.svg


Calculated efficiencies of the cubic phase perovskites
------------------------------------------------------

This script requires the perfect absorption limit:

* :download:`Download perfect absorption limit <perfect_abs.txt>`

.. literalinclude:: efficiency.py

.. image:: efficiency.svg
