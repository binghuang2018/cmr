"""Build CMR's web-page."""

import os
import shutil
import subprocess
import sys
from pathlib import Path


home = Path.home() / 'web-pages'

cmds = f"""\
python3 -m venv venv
. venv/bin/activate
pip install -U pip -qq
pip install wheel sphinx-rtd-theme seaborn qeh
git clone -q https://gitlab.com/camd/cmr.git
git clone -q https://gitlab.com/ase/ase.git
git clone -q https://gitlab.com/asr-dev/asr.git
pip install ./ase/ --use-feature=in-tree-build
pip install ./asr/ --use-feature=in-tree-build
pip install -e cmr
DB=venv/lib/python3.8/site-packages/ase/db
mkdir $DB/static/jsmol
touch $DB/static/jsmol/JSmol.min.js
cd cmr/docs
python -m cmr symlink
sphinx-build -b html -d build/doctrees  -n -W . build/html
mv build/html cmr-web-page
python -m cmr fix-links
python -m cmr create-c2db-test-db
tar -czf cmr-web-page.tar.gz cmr-web-page
cp cmr-web-page.tar.gz {home}"""


def build():
    root = Path('/tmp/cmr-docs')
    if root.is_dir():
        sys.exit('Locked')
    root.mkdir()
    os.chdir(root)

    cmds2 = ' && '.join(cmds.splitlines())
    p = subprocess.run(cmds2, shell=True)
    if p.returncode == 0:
        status = 'ok'
    else:
        print('FAILED!', file=sys.stdout)
        status = 'error'
    f = root.with_name(f'cmr-docs-{status}')
    if f.is_dir():
        shutil.rmtree(f)
    root.rename(f)
    return status


def symlink() -> None:
    from cmr import downloads
    dl = home / 'cmr/downloads'
    for project, filenames in downloads:
        for filename in filenames:
            to = dl / filename
            fro = Path(project) / filename
            if not fro.is_file():
                fro.symlink_to(to)
        to = dl / (project + '.png')
        fro = Path('static') / to.name
        if not fro.is_file():
            fro.symlink_to(to)


def fix_links() -> None:
    from cmr import downloads
    dl = Path('cmr-web-page/_downloads')
    extra = [('', ['cmr.db'])]
    for project, filenames in downloads + extra:
        for filename in filenames:
            for p in dl.glob(f'*/{filename}'):
                md5_digest = p.parts[-2]
                (dl / filename).symlink_to(f'{md5_digest}/{filename}')


def create_c2db_test_db() -> None:
    from ase.db import connect
    path1 = home / 'cmr/downloads/c2db.db'
    big = connect(path1)
    path2 = Path('cmr-web-page/_downloads/c2db.db')
    small = connect(path2)
    with small:
        # For bzplot.py example:
        for row in big.select(has_asr_hse=True):
            break
        else:
            1 / 0
        small.write(row, data=row.data)

        # For plot_band_alignment.py example:
        for row in big.select('gap>0,class=TMDC-H', include_data=False):
            small.write(row)

        # For fig18.py example:
        row = big.get(formula='BN')
        small.write(row, data=row.data)

    small.metadata = big.metadata


def main():
    command = sys.argv[1]
    if command == 'build':
        build()
    elif command == 'symlink':
        symlink()
    elif command == 'fix-links':
        fix_links()
    elif command == 'create-c2db-test-db':
        create_c2db_test_db()
    else:
        print(command + '?')


if __name__ == '__main__':
    main()
