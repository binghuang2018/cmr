# flake8: noqa
title = 'Absorption spectra of perovskites'

key_descriptions = {
    'name': ('Name', 'Name of the structure', ''),
    'phase': ('Phase', 'Perovskite phase (cubic halides (cubic tetragonal orthorhombic) RP DJ)', ''),
    'gllbsc_dir_gap': ('GLLB dir. gap', 'Direct bandgap calculated with GLLB-SC', 'eV'),
    'gllbsc_ind_gap': ('GLLB ind. gap', 'Indirect bandgap calculated with GLLB-SC', 'eV'),
    'gllbsc_disc': ('GLLB der. disc.', 'Derivative discontinuity calculated with GLLB-SC', 'eV'),
    'eff': ('1nm efficiency', 'Calculated efficiency (thickness 1 nm)', 'eV'),
    'eff_max': ('Max. efficiency', 'Calculated efficiency (infinite thickness)', 'eV'),
    'data.energy': ('Freq. grid', 'Grid used to calculate the Re and Im part of epsilon', 'eV'),
    'data.re_eps': ('Re eps', 'Re(epsilon) in xyz direction', ''),
    'data.im_eps': ('Im eps', 'Im(epsilon) in xyz direction', ''),
    'project': ('', '', '')}

default_columns = ['id', 'age', 'formula', 'energy', 'pbc', 'volume', 'charge', 'magmom', 'gllbsc_dir_gap', 'gllbsc_ind_gap', 'eff']
