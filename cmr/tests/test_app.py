import pytest

from cmr import names
from cmr.app import projects


@pytest.mark.xfail
def test_index(client):
    page = client.get('/').data.decode()
    assert 'scoring parameter' in page


@pytest.mark.parametrize('name', names)
def test_project(client, name):
    print(name)
    if name == 'c1db':
        return
    page = client.get(f'/{name}/').data.decode()
    for line in page.splitlines():
        if 'update_table' in line:
            sid = int(line.split('table')[1][1:].split(',')[0])
            break
    else:
        assert False
    page = client.get(f'/update/{sid}/query/0/?query=').data.decode()
    project = projects[name]
    db = project['database']
    uid_key = project['uid_key']
    print(uid_key, flush=True)
    try:
        uid = next(db.select(limit=1)).get(uid_key)
    except StopIteration:
        print('Missing database for', name)
        return
    print(uid, sid)
    page = client.get(f'/{name}/row/{uid}').data.decode()
