#!/bin/bash
set -e
cd /tmp
# mkdir cmr-check
cd cmr-check
# python3 -m venv venv
. venv/bin/activate
pip install -U pip wheel
pip install psycopg2-binary
pip install git+https://gitlab.com/ase/ase.git@block-db-insert
pip install git+https://gitlab.com/asr-dev/asr.git@old-master
pip install git+https://gitlab.com/camd/cmr.git
# (cd cmr; git checkout check-everything-script)
# pip install -e ~/ase
# pip install -e ~/asr
# pip install -e ~/cmr
export ASETESTENV=1
export CMR_TMPDIR=/tmp/cmr-check/static
mkdir static
python -m cmr.check
python ~/cmr/cmr/check.py
cd ..
mv cmr-check cmr-ok
