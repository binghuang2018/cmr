from typing import Dict, Tuple

title = 'AgAu309'

key_descriptions: Dict[str, Tuple[str, str, str]] = {}

default_columns = ['calculator', 'formula', 'energy', 'mass']
