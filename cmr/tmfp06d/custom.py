title = ('Benchmark: the performance of semilocal and hybrid density '
         'functionals in 3d transition-metal chemistry')

default_columns = [
    'id',
    'age',
    'formula',
    'energy',
    'fmax',
    'pbc',
    'volume',
    'charge',
    'mass',
    'magmom',
    'name',
    'relativistic',
    'xc',
]

key_descriptions = {
    'basis': ('', '', ''),
    'calculator_version': ('', '', ''),
    'frequency': ('', '', ''),
    'project': ('', '', ''),
    'relaxed': ('', '', ''),
    'relativistic': ('', '', ''),
    'xc': ('', '', ''),
    'name': ('', '', ''),
    'time': ('', '', '')}
