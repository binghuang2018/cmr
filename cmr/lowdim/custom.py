from collections import namedtuple
from itertools import combinations
from pathlib import Path
from typing import Iterable, List, Tuple, Dict

import matplotlib.pyplot as plt
import numpy as np
from ase.db.row import AtomsRow

from cmr.web import (create_table as make_table,
                     ATOMS, UNITCELL, miscellaneous_section)


title = ('Definition of a scoring parameter to' +
         ' identify low-dimensional materials components')
uid_key = 'dbid'

Desc = namedtuple('Desc', ['short', 'long', 'unit'])


def get_combinations(dims: List[int]) -> Iterable[Tuple[int, ...]]:
    return (x for y in (combinations(dims, i)
                        for i in range(1, len(dims) + 1)) for x in y)


def sab_key_descriptions() -> Dict[str, Desc]:
    combs = list(get_combinations([0, 1, 2, 3]))
    keys = []
    for x in ['s', 'a', 'b']:
        for comb in combs:
            combstr = ''.join([str(d) for d in comb])
            keys.append('{}_{}'.format(x, combstr))

    key_descriptions = {}
    # add description of s, a, and b keys
    labels = {'s': ('', 'score'),
              'a': ('Start of', 'k-interval'),
              'b': ('End of', 'k-interval')}
    for k in keys:
        x, numbers = k.split('_')
        beg, end = labels.get(x, (None, None))
        if beg is None and end is None:
            continue
        else:
            assert beg is not None
            assert end is not None
            beg = beg + ' '
            end = ' ' + end
        desc = beg + '+'.join([n + 'D' for n in numbers]) + end
        key_descriptions[k] = Desc(short=desc, long='', unit='')

    return key_descriptions


def argmax(fun, keys: 'Iterable'):
    vals = ((k, fun(k)) for k in keys)
    return max(vals, key=lambda x: x[1])[0]


key_descriptions = sab_key_descriptions()
# add description of number of dimensional components
for d in range(4):
    desc = 'Number of {}D components'.format(d)
    key = 'numc_{}'.format(d)
    key_descriptions[key] = Desc(short=desc, long='', unit='')
# add description of external db ids
key_descriptions['source'] = Desc('Source', 'Database source', '')
key_descriptions['dbid'] = Desc('ID #', 'Database ID Number', '')
key_descriptions['doi'] = Desc('DOI', '', '')
key_descriptions['publication'] = Desc('Publication', '', '')
key_descriptions['spacegroup_number'] = Desc('Space group #',
                                             'Space group Number', '')

key_descriptions['dimtype'] = Desc('Dimensionality',
                                   'Dimensionality with highest score', '')
key_descriptions['h'] = Desc('Component count',
                             '# components with each dimensionality type', '')
key_descriptions['warning'] = Desc('Warning',
                                   'Potential problems with structure \
(comma separated)', '')

keysfortable0 = ['source', 'spacegroup_number', 'publication']
keysfortable2 = [k for k in key_descriptions.keys() if k.startswith('numc')]
keysforfigure = [k for k in key_descriptions.keys() if k.startswith('s_')]


def handle_query(args):
    parts = []
    if args['query']:
        parts.append(args['query'])
    if args['from_s']:
        parts.append(args['s'] + '>=' + args['from_s'])
    if args['to_s']:
        parts.append(args['s'] + '<=' + args['to_s'])
    if args['source']:
        parts.append('source=' + args['source'])
    return ','.join(parts)


default_columns = ['formula', 'source', 'dbid', 's_0', 's_1', 's_2', 's_3']


def layout(row,  # AtomsRow
           key_descriptions: Dict[str, Tuple[str, str, str]],
           prefix: str):  # ? -> List[Tuple[str, List[List[Dict[str, Any]]]]]:
    """Page layout."""
    FOSection = namedtuple('FOSection', ['title', 'columns'])
    table0 = make_table(row, ['Item', ''], keysfortable0, key_descriptions)
    table2 = make_table(row, ['Item', ''], keysfortable2, key_descriptions)
    scorefig = {'type': 'figure', 'filename': 'score.png'}
    doi = row.get('doi')
    if doi:
        href = '<a href="https://doi.org/{doi}">{doi}</a>'.format(doi=doi)
        table0['rows'].append(['doi', href])
    if row.source == 'COD':
        href = ('<a href="http://www.crystallography.net/cod/' +
                '{id}.html">{id}</a>'.format(id=row.dbid))
        table0['rows'].insert(0, ['COD Number', href])
    elif row.source == 'ICSD':
        table0['rows'].insert(0, ['ICSD Number', row.dbid])
    else:
        table0['rows'].insert(0, [key_descriptions['dbid'][1], row.dbid])
    # Basic properties
    col0 = [table0]
    col1 = [ATOMS, UNITCELL] if row.source == 'COD' else []
    page = [FOSection(title='Basic properties', columns=[col0, col1])]
    # Dimensionality properties
    col0 = [table2] if len(table2['rows']) else []
    col1 = [scorefig]
    page.append(FOSection(title='Dimensionality analysis',
                          columns=[col0, col1]))
    path = Path(prefix + 'score.png')
    if not path.is_file():
        score(row, path)

    excludekeys = keysfortable0 + keysfortable2
    page.append(miscellaneous_section(row, key_descriptions,
                                      exclude=excludekeys))
    return page


def score(row: AtomsRow, path: Path) -> None:
    vs = []
    for k in keysforfigure:
        v = row.get(k, 0)
        vs.append(v)
    x = np.arange(len(keysforfigure))
    fig, ax = plt.subplots()
    ax.bar(x, vs)
    ax.set_xticks(x)
    prettykeys = ['${}_{{{}}}$'.format(*k.split('_')) for k in keysforfigure]
    ax.set_xticklabels(prettykeys, rotation=90)
    ax.set_ylabel('Score')
    plt.tight_layout()
    plt.savefig(str(path))
    plt.close()
