import matplotlib.pyplot as plt
from ase.phasediagram import PhaseDiagram
from ase.data import atomic_numbers

# Don't ever do this:
atomic_numbers['A'] = 1
atomic_numbers['B'] = 1

refs = [('A', 0.0),
        ('B', 0.0),
        ('AB', -0.21 * 2),
        ('AB2', -0.35 * 3)]

pd = PhaseDiagram(refs)
data = pd.plot2d2()
x, e, names, hull, simplices, xlabel, ylabel = data
x = 1 - x

plt.xkcd()
fig = plt.figure(figsize=(4, 3))
ax = plt.gca()

for i, j in simplices:
    ax.plot(x[[i, j]], e[[i, j]], '-', color='lightblue')

ax.plot(x, e, 'sg')

for a, b, name in zip(x, e, names):
    ax.text(a - 0.04, b, name, ha='right', va='center')

ax.set_xlabel('A$_x$B$_{1-x}$')
ax.set_ylabel(r'$\Delta H$ [eV/atom]')
ax.axis(xmin=-0.15, xmax=1.15, ymax=0.03, ymin=-0.4)
plt.tight_layout()

# plt.show()
plt.savefig('oqmd12.png')
