from math import log
from ase.db import connect
from ase.build import niggli_reduce
from ase.calculators.calculator import kpts2ndarray
# from gpaw import GPAW, PW
from gpaw.symmetry import Symmetry
from gpaw.kpt_descriptor import KPointDescriptor
from gpaw.setup import create_setup
n = 0
symbols = {'H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na',
           'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti',
           'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As',
           'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Ru', 'Rh',
           'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba',
           'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb',
           'Bi', 'Rn'}
mag_elements = {'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn',
                'Y', 'Zr', 'Nb', 'Mo', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In',
                'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl'}
db = connect('oqmd-all.db')
out = connect('oqmd3.db')
d = 6
E = 800
T = 0.0
N = {s: create_setup(s).Nv for s in symbols}
for row in db.select('stability<=0'):
    c = row.count_atoms()
    if len(c) == 3:
        if all(s in symbols for s in c):
            n += 1
            a = row.toatoms()
            # a.calc = GPAW(mode=PW(200),
            #               kpts=
            #               txt=None)
            # a.calc.initialize(a)
            # ni = len(a.calc.get_ibz_k_points())
            # nk = len(a.calc.get_bz_k_points())
            # ne = a.calc.setups.nvalence
            bzkpts_kc = kpts2ndarray({'density': d}, a)
            kd = KPointDescriptor(bzkpts_kc, 1)
            id_a = a.get_atomic_numbers()
            symmetry = Symmetry(id_a, a.cell, a.pbc,
                                symmorphic=not False,
                                time_reversal=not False)
            symmetry.analyze(a.get_scaled_positions())
            kd.set_symmetry(a, symmetry)
            ni = len(kd.ibzk_kc)
            nk = len(kd.bzk_kc)
            ne = sum(n * N[s] for s, n in c.items())
            V = a.get_volume()
            nG = V * E**1.5 / 4000 * 9
            t = ni / nk * ne * d**3 * E**1.5 * log(nG) * 1e-3 / 1600 * 2
            mem = 0.6 * ne * ni * nG * 16 * 3e-9
            print(row.formula, t, mem)
            T += t
            niggli_reduce(a)
            out.write(a, mem=mem, txeon8=t, name=row.formula)
