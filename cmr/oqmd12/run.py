import sys
import traceback
from typing import Dict, Union

import numpy as np
from ase.db import connect
from ase.utils import opencew
from gpaw import GPAW, PW, Davidson, FermiDirac
from gpaw.mpi import world

if len(sys.argv) == 2:
    pbeu = 'u' in sys.argv[1]
    mag = 'm' in sys.argv[1]
else:
    pbeu = False
    mag = False

kptdensity = 6.0
width = 0.05
ecut = 800
xc = 'PBE'

# U values are from AFLOW.  Based on some papers mentioned here:
#
#     https://pubs.acs.org/doi/10.1021/co200012w
u_atoms = {'Ti': 4.4,
           'V': 2.7,
           'Cr': 3.5,
           'Mn': 4.0,
           'Fe': 4.6,
           'Co': 5.0,
           'Ni': 5.1,
           'Cu': 4.0,
           'Zn': 7.5,
           'Nb': 2.1,
           'Mo': 2.4,
           'Tc': 2.7,
           'Ru': 3.0,
           'Rh': 3.3,
           'Pd': 3.6,
           'Cd': 2.1,
           'Ta': 2.0,
           'W': 2.2,
           'Re': 2.4,
           'Os': 2.6,
           'Ir': 2.8,
           'Pt': 3.0}

mag_elements = {'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn',
                'Y', 'Zr', 'Nb', 'Mo', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In',
                'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl'}

db = connect('oqmd.db')
out = connect('gpaw.db')

for row in db.select():
    name = row.formula
    setups: Union[Dict[str, str], str]
    if pbeu:
        setups = {symbol: ':d,{:.1f}'.format(u)
                  for symbol, u in u_atoms.items()
                  if symbol in row.symbols}
        if len(setups) == 0:
            continue
    else:
        setups = 'paw'

    if mag:
        for symbol in row.symbols:
            if symbol in mag_elements:
                break
        else:
            continue

    atoms = row.toatoms()

    if world.size == 0:
        if row.mem / world.size * (2 if mag else 1) > 1.5:
            continue
    else:
        if row.mem / 8 * (2 if mag else 1) < 1.5:
            pass  # continue

    if opencew(name + '.txt') is None:
        continue

    if mag:
        atoms.set_initial_magnetic_moments(np.ones(len(atoms)))

    atoms.calc = GPAW(
        mode=PW(ecut),
        eigensolver=Davidson(3),
        xc=xc,
        setups=setups,
        basis='dzp',
        kpts={'density': kptdensity},
        occupations=FermiDirac(width=width),
        # symmetry={'do_not_symmetrize_the_density': True},
        txt=name + '.txt')
    try:
        atoms.get_potential_energy()
    except Exception:
        atoms.calc = None
        if world.rank == 0:
            with open(name + '.err', 'w') as f:
                traceback.print_exc(file=f)
        continue

    atoms.get_forces()
    atoms.get_stress()
    atoms.calc.write(name + '.gpw')
    out.write(atoms, name=row.formula)
