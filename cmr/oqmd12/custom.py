import numpy as np

from cmr.web import create_table, ATOMS, UNITCELL, miscellaneous_section

title = 'One and two component references from OQMD'

key_descriptions = {
    'de': ('Heat of formation', '', 'eV/atom'),
    'ns': ('Species', 'Number of species', ''),
    'emag': ('Mag. energy', 'Magnetization energy', 'eV/atom'),
    'u': ('PBE+U?', '', ''),
    'uid': ('Unique ID', '', ''),
    'name': ('Name', '', ''),
    'niggli': ('Niggli-reduced?', '', ''),
    'oqmd_id': ('OQMD-ID', '', ''),
    'oqmd_bandgap': ('', '', ''),
    'oqmd_delta_e': ('', '', ''),
    'oqmd_energy_pa': ('', '', ''),
    'oqmd_prototype': ('', '', ''),
    'oqmd_stability': ('', '', '')}

uid_key = 'uid'

default_columns = ['formula', 'energy', 'magmom', 'de', 'ns', 'volume',
                   'fmax', 'smax', 'u']


def handle_query(args):
    parts = []
    if args['query']:
        parts.append(args['query'])
    if args['ns']:
        parts.append('ns=' + args['ns'])
    if args['xc']:
        parts.append('u=' + 'False' if args['xc'] == 'PBE' else 'True')
    return ','.join(parts)


basic_keys = default_columns + ['oqmd_' + key for key in
                                ['bandgap', 'energy_pa',
                                 'stability', 'delta_e']]


def layout(row, key_descriptions, prefix):
    from pathlib import Path
    basic = create_table(row, ['Property', ''], basic_keys, key_descriptions)
    basic['rows'].append(
        ('OQMD-link',
         '<a href="http://oqmd.org/materials/entry/{oqmd}">{oqmd}</a>'
         .format(oqmd=row.oqmd_id)))

    page = [('Basic properties',
             [[ATOMS, UNITCELL],
              [basic]])]

    if 'hull' in row.data:
        hull = row.data.hull
        x = hull.x
        y = hull.y
        formulas = hull.formulas
        table = []
        for a, e, formula in sorted(zip(x, y, formulas)):
            if formula != row.formula:
                uid = formula
                if row.u:
                    uid += '-U'
                formula = ('<a href="/oqmd12/row/{uid}">{formula}</a>'
                           .format(uid=uid, formula=formula))
            table.append([formula, '{:.3f} eV/atom'.format(e)])

        page.append(
            ('Convex hull',
             [[{'type': 'figure', 'filename': 'convex-hull.png'}],
              [{'type': 'table', 'title': 'Formation energies',
                'rows': table}]]))

        path = Path(prefix + 'convex-hull.png')
        if not path.is_file():
            convex_hull(hull, row, path)

    page.append(miscellaneous_section(row, key_descriptions, basic_keys))

    return page


def convex_hull(hull, row, path):
    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.gca()
    x = np.array(hull.x)
    y = np.array(hull.y)
    for i, j in hull.lines:
        ax.plot(x[[i, j]], y[[i, j]], '-', color='lightblue')
    ax.plot(x, y, 'og')
    j = list(hull.formulas).index(row.formula)
    ax.plot([x[j]], [y[j]], 'sr')
    for a, b, name in zip(x, y, hull.formulas):
        if a > 0.5:
            ax.text(a + 0.015, b, name, ha='left', va='top')
        else:
            ax.text(a - 0.015, b, name, ha='right', va='top')
    A, B = hull.symbols
    ax.set_xlabel('{}$_{{1-x}}${}$_x$'.format(A, B))
    ax.set_ylabel(r'$\Delta H$ [eV/atom]')

    plt.tight_layout()
    plt.savefig(path)
    plt.close()
