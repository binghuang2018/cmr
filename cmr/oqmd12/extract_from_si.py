from pathlib import Path
from ase import Atoms
from ase.db import connect

keys = ['energy_pa', 'volume', 'bandgap', 'delta_e', 'stability']
folder = Path('voro-ml-si/datasets/oqmd-all/')


def extract(db):
    for line in (folder / 'properties.txt').read_text().splitlines()[1:]:
        fname, dir, *numbers = line.split()
        lines = [line.split()
                 for line in (folder / fname).read_text().splitlines()]
        if len(lines[0]) > 2:
            continue
        atoms = Atoms(''.join(symbol + n
                              for symbol, n in zip(lines[5], lines[6])),
                      cell=[[float(x) for x in line]
                            for line in lines[2:5]],
                      scaled_positions=[[float(x) for x in line]
                                        for line in lines[8:]],
                      pbc=True)
        oqmd_id = int(fname.split('-')[1])
        kv = {key: float(x)
              for key, x in zip(keys, numbers)
              if x != 'None'}
        db.write(atoms, oqmd_id=oqmd_id, **kv)


db = connect('oqmd-si.db')
with db:
    extract(db)
