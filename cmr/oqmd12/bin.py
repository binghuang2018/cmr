import pickle
from collections import defaultdict
from ase.db import connect
from ase.phasediagram import PhaseDiagram
if 1:
    db = connect('oqmd-si.db')
    s1 = defaultdict(list)
    s2 = defaultdict(list)
    with connect('oqmd-12.db') as db2:
        for row in db.select():  # 'stability<=0'):
            c = row.count_atoms()
            if len(c) > 2:
                continue
            id = db2.write(row, **row.key_value_pairs)
            if len(c) == 1:
                s1[row.symbols[0]].append((row.delta_e, id))
            elif len(c) == 2:
                key = tuple(sorted(c))
                s2[key].append((row.formula,
                                row.delta_e * row.natoms, id))
                # print(key, end=',')
            else:
                1 / 0
    print(len(s1))
    print(len(s2))
    pickle.dump((s1, s2), open('oqmd-12.pckl', 'wb'))
if 1:
    id1 = set()
    id2 = set()
    s1, s2 = pickle.load(open('oqmd-12.pckl', 'rb'))
    for a, b in s2:
        refs = [(f, e) for f, e, id in s2[(a, b)]]
        n2 = len(refs)
        for x in [a, b]:
            refs.extend([(x, e) for e, id in s1[x]])
        ch = PhaseDiagram(refs, verbose=False)
        h1 = ch.hull[n2:]
        h2 = ch.hull[:n2]
        for h, (e, id) in zip(h1, s1[a] + s1[b]):
            if h:
                id1.add(id)
        for h, (f, e, id) in zip(h2, s2[(a, b)]):
            if h:
                id2.add(id)
    print(len(id1))
    print(len(id2))

if 1:
    db = connect('oqmd-12.db')
    db1 = connect('oqmd-1.db')
    db2 = connect('oqmd-2.db')
    for row in db.select():
        if row.id in id1:
            db1.write(row, **row.key_value_pairs)
        if row.id in id2:
            db2.write(row, **row.key_value_pairs)
