from ase.db import connect

symbols = {'H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na',
           'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti',
           'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As',
           'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Ru', 'Rh',
           'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba',
           'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb',
           'Bi', 'Rn'}

db1 = connect('oqmd-1.db')
db2 = connect('oqmd-2.db')
db = connect('gpaw-in.db')

for row in db2.select():
    for symbol in row.symbols:
        if symbol not in symbols:
            break
    else:
        db.write(row.toatoms())

a = set()
for row in db1.select():
    db.write(row.toatoms())
    symbol = row.symbols[0]
    assert symbol not in a
    a.add(symbol)
