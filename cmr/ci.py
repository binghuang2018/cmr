from pathlib import Path
from urllib.request import urlretrieve
from cmr import project2folder, skip
from cmr.app import app, initialize_project, projects, tmpdir
from ase.db import connect

postgres_url = 'postgresql://ase:ase@postgres:5432/testase'


def check_web_pages() -> None:
    """Check that serving web-pages from postgres works."""
    # Download db-file with one row from each project:
    cmr = Path() / 'cmr.db'
    url = 'https://cmr.fysik.dtu.dk'
    urlretrieve(f'{url}/_downloads/cmr.db', cmr)

    db = connect(cmr)
    pg = connect(postgres_url)

    for row in db.select():
        # Write ro to postgres db:
        name = row.project_name
        folder = project2folder[name]
        if folder in skip:
            continue
        kvp = row.key_value_pairs
        del kvp['project_name']
        id = pg.write(row.toatoms(), data=row.data, key_value_pairs=kvp)
        if name in {'c1db', 'c2db', 'qpod'}:
            pg.metadata = db.metadata

        check_web_page(name)

        pg.delete([id])


def check_web_page(project_name: str) -> None:
    """Serve web-page and hit overview and row pages."""
    # Initialize project:
    folder = project2folder[project_name]
    initialize_project(project_name, folder, postgres_url)

    # Create client:
    app.testing = True
    client = app.test_client()

    # Test overview page:
    client.get(f'/{project_name}/').data.decode()

    # Test row:
    project = projects[project_name]
    db = project['database']
    uid_key = project['uid_key']
    uids = [row.get(uid_key) for row in db.select(include_data=False)]
    print(project_name, folder, uid_key, uids)
    for uid in uids:
        client.get(f'/{project_name}/row/{uid}').data.decode()
    for file in (tmpdir / project_name).glob('*'):
        print(file.name)

    del projects[project_name]


if __name__ == '__main__':
    check_web_pages()
