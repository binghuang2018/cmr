from cmr.web import convert_old_layout as convert, ATOMS, UNITCELL

title = 'Adsorption energy database'

key_descriptions = {
    'surf_mat': ('Surface Material', '', ''),
    'adsorbate': ('Adsorbate', '', ''),
    'mol': ('Reference molecule 1', '', ''),
    'mol2': ('Reference molecule 2', '', ''),
    'LDA_adsorp': ('LDA', 'Adsorption energy with LDA', 'eV'),
    'LDA_ads': ('LDA ads@slab', 'LDA adsorbate on slab', 'eV'),
    'LDA_slab': ('LDA slab', 'LDA slab', 'eV'),
    'LDA_mol': ('LDA 1st molecule', 'LDA 1st molecule', 'eV'),
    'LDA_mol2': ('LDA 2nd molecule', 'LDA 2nd molecule', 'eV'),
    'PBE_adsorp': ('PBE', 'Adsorption energy with PBE', 'eV'),
    'PBE_ads': ('PBE ads@slab', 'PBE adsorbate on slab', 'eV'),
    'PBE_slab': ('PBE slab', 'PBE slab', 'eV'),
    'PBE_mol': ('PBE 1st molecule', 'PBE 1st molecule', 'eV'),
    'PBE_mol2': ('PBE 2nd molecule', 'PBE 2nd molecule', 'eV'),
    'RPBE_adsorp': ('RPBE', 'Adsorption energy with RPBE', 'eV'),
    'RPBE_ads': ('RPBE ads@slab', 'RPBE adsorbate on slab', 'eV'),
    'RPBE_slab': ('RPBE slab', 'RPBE slab', 'eV'),
    'RPBE_mol': ('RPBE 1st molecule', 'RPBE 1st molecule', 'eV'),
    'RPBE_mol2': ('RPBE 2nd molecule', 'RPBE 2nd molecule', 'eV'),
    'BEEFvdW_adsorp': ('BEEF-vdW', 'Adsorption energy with BEEF-vdW', 'eV'),
    'BEEFvdW_ads': ('BEEF-vdW ads@slab', 'BEEF-vdW adsorbate on slab', 'eV'),
    'BEEFvdW_slab': ('BEEF-vdW slab', 'BEEF-vdW slab', 'eV'),
    'BEEFvdW_mol': ('BEEF-vdW 1st molecule', 'BEEF-vdW 1st molecule', 'eV'),
    'BEEFvdW_mol2': ('BEEF-vdW 2nd molecule', 'BEEF-vdW 2nd molecule', 'eV'),
    'vdWDF2_adsorp': ('vdW-DF2', 'Adsorption energy with vdW-DF2', 'eV'),
    'vdWDF2_ads': ('vdW-DF2 ads@slab', 'vdW-DF2 adsorbate on slab', 'eV'),
    'vdWDF2_slab': ('vdW-DF2 slab', 'vdW-DF2 slab', 'eV'),
    'vdWDF2_mol': ('vdW-DF2 1st molecule', 'vdW-DF2 1st molecule', 'eV'),
    'vdWDF2_mol2': ('vdW-DF2 2nd molecule', 'vdW-DF2 2nd molecule', 'eV'),
    'mBEEF_adsorp': ('mBEEF', 'Adsorption energy with mBEEF', 'eV'),
    'mBEEF_ads': ('mBEEF ads@slab', 'mBEEF adsorbate on slab', 'eV'),
    'mBEEF_slab': ('mBEEFW slab', 'mBEEF slab', 'eV'),
    'mBEEF_mol': ('mBEEF 1st molecule', 'mBEEF 1st molecule', 'eV'),
    'mBEEF_mol2': ('mBEEF 2nd molecule', 'mBEEF 2nd molecule', 'eV'),
    'mBEEFvdW_adsorp': ('mBEEF-vdW', 'Adsorption energy with mBEEF-vdW', 'eV'),
    'mBEEFvdW_ads': ('mBEEF-vdW ads@slab', 'mBEEF-vdW adsorbate on slab',
                     'eV'),
    'mBEEFvdW_slab': ('mBEEF-vdW slab', 'mBEEF-vdW slab', 'eV'),
    'mBEEFvdW_mol': ('mBEEF-vdW 1st molecule', 'mBEEF-vdW 1st molecule', 'eV'),
    'mBEEFvdW_mol2': ('mBEEF-vdW 2nd molecule', 'mBEEF-vdW 2nd molecule',
                      'eV'),
    'EXX_ads': ('EXX ads@slab', 'EXX adsorbate on slab', 'eV'),
    'EXX_slab': ('EXX slab', 'EXX slab', 'eV'),
    'EXX_mol': ('EXX 1st molecule', 'EXX 1st molecule', 'eV'),
    'EXX_mol2': ('EXX 2nd molecule', 'EXX 2nd molecule', 'eV'),
    'EXX_adsorp': ('EXX', 'Adsorption energy with EXX', 'eV'),
    'RPA_ads_extrap': ('RPA ads@slab extrapolated',
                       'RPA adsorbate on slab extrapolated', 'eV'),
    'RPA_slab_extrap': ('RPA slab extrapolated', 'RPA slab extrapolated',
                        'eV'),
    'RPA_mol_extrap': ('RPA 1st molecule extrapolated',
                       'RPA 1st molecule extrapolated', 'eV'),
    'RPA_mol2_extrap': ('RPA 2nd molecule extrapolated',
                        'RPA 2nd molecule extrapolated', 'eV'),
    'RPA_adsorp': ('RPA extrapolated',
                   'RPA correlation adsorption energy extrapolated', 'eV'),
    'RPA_EXX_adsorp': ('EXX+RPA', 'Adsorption energy with EXX+RPA', 'eV'),
    'RPA_ads_ecut300_k6': ('RPA ads@slab, Ecut=300, k=6x6x1',
                           'RPA adsorbate on slab, Ecut=300, k=6x6x1', 'eV'),
    'RPA_ads_ecut400_k6': ('RPA ads@slab, Ecut=400, k=6x6x1',
                           'RPA adsorbate on slab, Ecut=400, k=6x6x1', 'eV'),
    'RPA_ads_ecut500_k6': ('RPA ads@slab, Ecut=500, k=6x6x1',
                           'RPA adsorbate on slab, Ecut=500, k=6x6x1', 'eV'),
    'RPA_ads_ecut300_k12': ('RPA ads@slab, Ecut=300, k=12x12x1',
                            'RPA adsorbate on slab, Ecut=300, k=12x12x1',
                            'eV'),
    'RPA_slab_ecut300_k6': ('RPA slab, Ecut=300, k=6x6x1',
                            'RPA slab, Ecut=300, k=6x6x1', 'eV'),
    'RPA_slab_ecut400_k6': ('RPA slab, Ecut=400, k=6x6x1',
                            'RPA slab, Ecut=400, k=6x6x1', 'eV'),
    'RPA_slab_ecut500_k6': ('RPA slab, Ecut=500, k=6x6x1',
                            'RPA slab, Ecut=500, k=6x6x1', 'eV'),
    'RPA_slab_ecut300_k12': ('RPA slab, Ecut=300, k=12x12x1',
                             'RPA slab, Ecut=300, k=12x12x1', 'eV'),
    'RPA_mol_ecut425': ('RPA 1st molecule, Ecut=425',
                        'RPA 1st molecule, Ecut=425', 'eV'),
    'RPA_mol_ecut450': ('RPA 1st molecule, Ecut=450',
                        'RPA 1st molecule, Ecut=450', 'eV'),
    'RPA_mol_ecut475': ('RPA 1st molecule, Ecut=475',
                        'RPA 1st molecule, Ecut=475', 'eV'),
    'RPA_mol_ecut500': ('RPA 1st molecule, Ecut=500',
                        'RPA 1st molecule, Ecut=500', 'eV'),
    'RPA_mol_ecut530': ('RPA 1st molecule, Ecut=530',
                        'RPA 1st molecule, Ecut=530', 'eV'),
    'RPA_mol_ecut560': ('RPA 1st molecule, Ecut=560',
                        'RPA 1st molecule, Ecut=560', 'eV'),
    'RPA_mol_ecut600': ('RPA 1st molecule, Ecut=600',
                        'RPA 1st molecule, Ecut=600', 'eV'),
    'RPA_mol2_ecut425': ('RPA 2nd molecule, Ecut=425',
                         'RPA 2nd molecule, Ecut=425', 'eV'),
    'RPA_mol2_ecut450': ('RPA 2nd molecule, Ecut=450',
                         'RPA 2nd molecule, Ecut=450', 'eV'),
    'RPA_mol2_ecut475': ('RPA 2nd molecule, Ecut=475',
                         'RPA 2nd molecule, Ecut=475', 'eV'),
    'RPA_mol2_ecut500': ('RPA 2nd molecule, Ecut=500',
                         'RPA 2nd molecule, Ecut=500', 'eV'),
    'RPA_mol2_ecut530': ('RPA 2nd molecule, Ecut=530',
                         'RPA 2nd molecule, Ecut=530', 'eV'),
    'RPA_mol2_ecut560': ('RPA 2nd molecule, Ecut=560',
                         'RPA 2nd molecule, Ecut=560', 'eV'),
    'RPA_mol2_ecut600': ('RPA 2nd molecule, Ecut=600',
                         'RPA 2nd molecule, Ecut=600', 'eV'),
}

default_columns = ['adsorbate', 'surf_mat', 'LDA_adsorp', 'PBE_adsorp',
                   'RPBE_adsorp',
                   'BEEFvdW_adsorp', 'vdWDF2_adsorp', 'mBEEF_adsorp',
                   'mBEEFvdW_adsorp', 'RPA_EXX_adsorp']


def handle_query(args):
    parts = []
    if args['query']:
        parts.append(args['query'])
    if args['surf_mat']:
        parts.append('surf_mat=' + args['surf_mat'])
    if args['adsorbate']:
        parts.append('adsorbate=' + args['adsorbate'])
    return ','.join(parts)


reac = ('Reactants', ['adsorbate', 'surf_mat', 'mol', 'mol2'])
basic = ('Adsorption energies',
         ['LDA_adsorp', 'PBE_adsorp', 'RPBE_adsorp', 'vdWDF2_adsorp',
          'BEEFvdW_adsorp', 'mBEEF_adsorp', 'mBEEFvdW_adsorp',
          'RPA_EXX_adsorp'])
ldadata = ('LDA Data',
           ['LDA_ads', 'LDA_slab', 'LDA_mol', 'LDA_mol2', 'LDA_adsorp'])
pbedata = ('PBE Data',
           ['PBE_ads', 'PBE_slab', 'PBE_mol', 'PBE_mol2', 'PBE_adsorp'])
rpbedata = ('RPBE Data',
            ['RPBE_ads', 'RPBE_slab', 'RPBE_mol', 'RPBE_mol2', 'RPBE_adsorp'])
vdwdf2data = ('vdW-DF2 Data',
              ['vdWDF2_ads', 'vdWDF2_slab', 'vdWDF2_mol', 'vdWDF2_mol2',
               'vdWDF2_adsorp'])
beefvdwdata = ('BEEF-vdW Data',
               ['BEEFvdW_ads', 'BEEFvdW_slab', 'BEEFvdW_mol',
                'BEEFvdW_mol2', 'BEEFvdW_adsorp'])
mbeefdata = ('mBEEF Data',
             ['mBEEF_ads', 'mBEEF_slab', 'mBEEF_mol',
              'mBEEF_mol2', 'mBEEF_adsorp'])
mbeefvdwdata = ('mBEEF-vdW Data',
                ['mBEEFvdW_ads', 'mBEEFvdW_slab', 'mBEEFvdW_mol',
                 'mBEEFvdW_mol2', 'mBEEFvdW_adsorp'])

exxdata = ('EXX Data',
           ['EXX_ads', 'EXX_slab', 'EXX_mol', 'EXX_mol2', 'EXX_adsorp'])
rpadata = ('RPA Correlation Energy Extrapolated',
           ['RPA_ads_extrap', 'RPA_slab_extrap',
            'RPA_mol_extrap', 'RPA_mol2_extrap',
            'RPA_adsorp', 'RPA_EXX_adsorp'])

rpaadsdata = ('Ads@slab: RPA Correlation Energy',
              ['RPA_ads_ecut300_k6', 'RPA_ads_ecut400_k6',
               'RPA_ads_ecut500_k6', 'RPA_ads_ecut300_k12', 'RPA_ads_extrap'])
rpaslabdata = ('Slab: RPA Correlation Energy',
               ['RPA_slab_ecut300_k6', 'RPA_slab_ecut400_k6',
                'RPA_slab_ecut500_k6', 'RPA_slab_ecut300_k12',
                'RPA_slab_extrap'])
rpamoldata = ('Molecule 1: RPA Correlation Energy',
              ['RPA_mol_ecut425', 'RPA_mol_ecut450',
               'RPA_mol_ecut475', 'RPA_mol_ecut500',
               'RPA_mol_ecut530', 'RPA_mol_ecut560',
               'RPA_mol_ecut600', 'RPA_mol_extrap'])
rpamol2data = ('Molecule 2: RPA Correlation Energy',
               ['RPA_mol2_ecut425', 'RPA_mol2_ecut450',
                'RPA_mol2_ecut475', 'RPA_mol2_ecut500',
                'RPA_mol2_ecut530', 'RPA_mol2_ecut560',
                'RPA_mol2_ecut600', 'RPA_mol2_extrap'])


layout = convert(
    [('Basic properties',
      [[reac, basic, UNITCELL],
       [ATOMS]]),
     ('DFT Data',
      [[ldadata, pbedata, rpbedata],
       [vdwdf2data, beefvdwdata, mbeefdata, mbeefvdwdata]]),
     ('Exact exchange data',
      [[exxdata]]),
     ('RPA data',
      [[rpaadsdata, rpaslabdata], [rpamoldata, rpamol2data, rpadata]])])
