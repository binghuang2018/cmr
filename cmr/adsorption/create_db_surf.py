# flake8: noqa
# type: ignore
import os
from ase.db import connect
import pickle
import numpy as np
from gpaw import *

db = connect('surf_24_11.db')

direc = '/home/niflheim2/psisc/PhD_adsorp/relax_structs/'

#ecuts = np.array([350, 400, 425, 450, 475, 500, 530, 560, 600])
#adsorbates = ['H', 'O', 'N', 'N2', 'CO', 'NO', 'CH', 'OH']
slabs = ['Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Y', 'Zr', 'Nb', 'Mo', 'Ru', 'Rh', 'Pd', 'Ag', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au']

for j, slab in enumerate(slabs):
    if slab == 'Fe' or slab == 'Co' or slab == 'Ni':
        spin = 'newsetup_spinpol_gpw/'
    else:
        spin = 'newsetup_gpw/'
    atoms, calc = restart(direc+spin+slab+'_slab_vac5_k12_pwcutoff800.gpw',txt=None)

    ###DFT
    #LDA
    LDA_bulk = float(pickle.load(open('data_to_cmr/'+slab+'_bulk_LDA.pckl','rb'),encoding='bytes'))
    LDA_slab = float(pickle.load(open('data_to_cmr/'+slab+'_LDA.pckl','rb'),encoding='bytes'))
    LDA_surf = float(pickle.load(open('data_to_cmr/'+slab+'_surf_LDA.pckl','rb'),encoding='bytes'))

    #PBE
    PBE_bulk = float(pickle.load(open('data_to_cmr/'+slab+'_bulk_PBE.pckl','rb'),encoding='bytes'))
    PBE_slab = float(pickle.load(open('data_to_cmr/'+slab+'_PBE.pckl','rb'),encoding='bytes'))
    PBE_surf = float(pickle.load(open('data_to_cmr/'+slab+'_surf_PBE.pckl','rb'),encoding='bytes'))

    #RPBE
    RPBE_bulk = float(pickle.load(open('data_to_cmr/'+slab+'_bulk_RPBE.pckl','rb'),encoding='bytes'))
    RPBE_slab = float(pickle.load(open('data_to_cmr/'+slab+'_RPBE.pckl','rb'),encoding='bytes'))
    RPBE_surf = float(pickle.load(open('data_to_cmr/'+slab+'_surf_RPBE.pckl','rb'),encoding='bytes'))

    #vdWDF2
    vdWDF2_bulk = float(pickle.load(open('data_to_cmr/'+slab+'_bulk_vdWDF2.pckl','rb'),encoding='bytes'))
    vdWDF2_slab = float(pickle.load(open('data_to_cmr/'+slab+'_vdW-DF2.pckl','rb'),encoding='bytes'))
    vdWDF2_surf = float(pickle.load(open('data_to_cmr/'+slab+'_surf_vdWDF2.pckl','rb'),encoding='bytes'))

    #BEEFvdW
    BEEFvdW_bulk = float(pickle.load(open('data_to_cmr/'+slab+'_bulk_BEEFvdW.pckl','rb'),encoding='bytes'))
    BEEFvdW_slab = float(pickle.load(open('data_to_cmr/'+slab+'_BEEF-vdW.pckl','rb'),encoding='bytes'))
    BEEFvdW_surf = float(pickle.load(open('data_to_cmr/'+slab+'_surf_BEEFvdW.pckl','rb'),encoding='bytes'))

    #mBEEF
    mBEEF_bulk = float(pickle.load(open('data_to_cmr/'+slab+'_bulk_mBEEF.pckl','rb'),encoding='bytes'))
    mBEEF_slab = float(pickle.load(open('data_to_cmr/'+slab+'_mBEEF.pckl','rb'),encoding='bytes'))
    mBEEF_surf = float(pickle.load(open('data_to_cmr/'+slab+'_surf_mBEEF.pckl','rb'),encoding='bytes'))

    #mBEEFvdW
    mBEEFvdW_bulk = float(pickle.load(open('data_to_cmr/'+slab+'_bulk_mBEEFvdW.pckl','rb'),encoding='bytes'))
    mBEEFvdW_slab = float(pickle.load(open('data_to_cmr/'+slab+'_mBEEF-vdW.pckl','rb'),encoding='bytes'))
    mBEEFvdW_surf = float(pickle.load(open('data_to_cmr/'+slab+'_surf_mBEEFvdW.pckl','rb'),encoding='bytes'))

    #EXX
    EXX_bulk = float(pickle.load(open('data_to_cmr/'+slab+'_bulk_EXX.pckl','rb'),encoding='bytes'))
    EXX_slab = float(pickle.load(open('data_to_cmr/'+slab+'_EXX.pckl','rb'),encoding='bytes'))
    EXX_surf = float(pickle.load(open('data_to_cmr/'+slab+'_surf_EXX.pckl','rb'),encoding='bytes'))

    #RPA
    RPA_bulk_ecut300_k12 = float(pickle.load(open('data_to_cmr/'+slab+'_bulk_RPA_k12_300_'+slab+'.pckl','rb'),encoding='bytes'))
    RPA_bulk_ecut400_k12 = float(pickle.load(open('data_to_cmr/'+slab+'_bulk_RPA_k12_400_'+slab+'.pckl','rb'),encoding='bytes'))
    RPA_bulk_ecut500_k12 = float(pickle.load(open('data_to_cmr/'+slab+'_bulk_RPA_k12_500_'+slab+'.pckl','rb'),encoding='bytes'))
    RPA_bulk_extrap = float(pickle.load(open('data_to_cmr/'+slab+'_bulk_RPA_extrap.pckl','rb'),encoding='bytes'))

    RPA_surf = float(pickle.load(open('data_to_cmr/'+slab+'_surf_RPA.pckl','rb'),encoding='bytes'))
    EXXRPA_surf = EXX_surf + RPA_surf

    RPA_slab_ecut300_k6 = float(pickle.load(open('data_to_cmr/'+slab+'_ecut300_k6.pckl','rb'),encoding='bytes'))
    RPA_slab_ecut400_k6 = float(pickle.load(open('data_to_cmr/'+slab+'_ecut400_k6.pckl','rb'),encoding='bytes'))
    RPA_slab_ecut500_k6 = float(pickle.load(open('data_to_cmr/'+slab+'_ecut500_k6.pckl','rb'),encoding='bytes'))
    RPA_slab_ecut300_k12 = float(pickle.load(open('data_to_cmr/'+slab+'_ecut300_k12.pckl','rb'),encoding='bytes'))
    RPA_slab_extrap = float(pickle.load(open('data_to_cmr/'+slab+'_extrap.pckl','rb'),encoding='bytes'))

    db.write(atoms, surf_mat=slab,
             LDA_bulk=LDA_bulk, LDA_slab=LDA_slab, LDA_surf=LDA_surf,
             PBE_bulk=PBE_bulk, PBE_slab=PBE_slab, PBE_surf=PBE_surf,
             RPBE_bulk=RPBE_bulk, RPBE_slab=RPBE_slab, RPBE_surf=RPBE_surf,
             BEEFvdW_bulk=BEEFvdW_bulk, BEEFvdW_slab=BEEFvdW_slab, BEEFvdW_surf=BEEFvdW_surf,
             mBEEFvdW_bulk=mBEEFvdW_bulk, mBEEFvdW_slab=mBEEFvdW_slab, mBEEFvdW_surf=mBEEFvdW_surf,
             mBEEF_bulk=mBEEF_bulk, mBEEF_slab=mBEEF_slab, mBEEF_surf=mBEEF_surf,
             vdWDF2_bulk=vdWDF2_bulk, vdWDF2_slab=vdWDF2_slab, vdWDF2_surf=vdWDF2_surf,
             EXX_bulk=EXX_bulk, EXX_slab=EXX_slab, EXX_surf=EXX_surf,
             RPA_bulk_extrap=RPA_bulk_extrap, RPA_slab_extrap=RPA_slab_extrap,
             RPA_surf=RPA_surf, RPA_EXX_surf=EXXRPA_surf,
             RPA_bulk_ecut300_k12=RPA_bulk_ecut300_k12,RPA_bulk_ecut400_k12=RPA_bulk_ecut400_k12,RPA_bulk_ecut500_k12=RPA_bulk_ecut500_k12,
             RPA_slab_ecut300_k6=RPA_slab_ecut300_k6, RPA_slab_ecut400_k6=RPA_slab_ecut400_k6, RPA_slab_ecut500_k6=RPA_slab_ecut500_k6,
             RPA_slab_ecut300_k12=RPA_slab_ecut300_k12)



