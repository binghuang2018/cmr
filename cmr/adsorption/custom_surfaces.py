from cmr.web import convert_old_layout as convert, ATOMS, UNITCELL

title = 'Surface energy database'

key_descriptions = {
    'surf_mat': ('Material', '', ''),
    'LDA_bulk': ('LDA bulk', 'LDA bulk', 'eV'),
    'LDA_slab': ('LDA slab', 'LDA slab', 'eV'),
    'LDA_surf': ('LDA', 'Surface energy with LDA', 'eV'),
    'PBE_bulk': ('PBE bulk', 'PBE bulk', 'eV'),
    'PBE_slab': ('PBE slab', 'PBE slab', 'eV'),
    'PBE_surf': ('PBE', 'Surface energy with PBE', 'eV'),
    'RPBE_bulk': ('RPBE bulk', 'RPBE bulk', 'eV'),
    'RPBE_slab': ('RPBE slab', 'RPBE slab', 'eV'),
    'RPBE_surf': ('RPBE', 'Surface energy with RPBE', 'eV'),
    'vdWDF2_bulk': ('vdW-DF2 bulk', 'vdW-DF2 bulk', 'eV'),
    'vdWDF2_slab': ('vdW-DF2 slab', 'vdW-DF2 slab', 'eV'),
    'vdWDF2_surf': ('vdW-DF2', 'Surface energy with vdW-DF2', 'eV'),
    'BEEFvdW_bulk': ('BEEF-vdW bulk', 'BEEF-vdW bulk', 'eV'),
    'BEEFvdW_slab': ('BEEF-vdW slab', 'BEEF-vdW slab', 'eV'),
    'BEEFvdW_surf': ('BEEF-vdW', 'Surface energy with BEEF-vdW', 'eV'),
    'mBEEF_bulk': ('mBEEF bulk', 'mBEEF bulk', 'eV'),
    'mBEEF_slab': ('mBEEF slab', 'mBEEF slab', 'eV'),
    'mBEEF_surf': ('mBEEF', 'Surface energy with mBEEF', 'eV'),
    'mBEEFvdW_bulk': ('mBEEF-vdW bulk', 'mBEEF-vdW bulk', 'eV'),
    'mBEEFvdW_slab': ('mBEEF-vdW slab', 'mBEEF-vdW slab', 'eV'),
    'mBEEFvdW_surf': ('mBEEF-vdW', 'Surface energy with mBEEF-vdW', 'eV'),
    'EXX_bulk': ('EXX bulk', 'EXX bulk', 'eV'),
    'EXX_slab': ('EXX slab', 'EXX slab', 'eV'),
    'EXX_surf': ('EXX', 'Surface energy with EXX', 'eV'),
    'RPA_bulk_extrap':
    ('RPA bulk extrapolated', 'RPA bulk extrapolated', 'eV'),
    'RPA_slab_extrap':
    ('RPA slab extrapolated', 'RPA slab extrapolated', 'eV'),
    'RPA_surf':
    ('RPA extrapolated', 'RPA correlation surface energy extrapolated', 'eV'),
    'RPA_EXX_surf': ('EXX+RPA', 'Surface energy with EXX+RPA', 'eV'),
    'RPA_bulk_ecut300_k12':
    ('RPA bulk, Ecut=300, k=12x12x12', 'RPA bulk, Ecut=300, k=12x12x12', 'eV'),
    'RPA_bulk_ecut400_k12': ('RPA bulk, Ecut=400, k=12x12x12',
                             'RPA bulk, Ecut=400, k=12x12x12', 'eV'),
    'RPA_bulk_ecut500_k12': ('RPA bulk, Ecut=500, k=12x12x12',
                             'RPA bulk, Ecut=500, k=12x12x12', 'eV'),
    'RPA_slab_ecut300_k6': ('RPA slab, Ecut=300, k=6x6x1',
                            'RPA slab, Ecut=300, k=6x6x1', 'eV'),
    'RPA_slab_ecut400_k6': ('RPA slab, Ecut=400, k=6x6x1',
                            'RPA slab, Ecut=400, k=6x6x1', 'eV'),
    'RPA_slab_ecut500_k6': ('RPA slab, Ecut=500, k=6x6x1',
                            'RPA slab, Ecut=500, k=6x6x1', 'eV'),
    'RPA_slab_ecut300_k12': ('RPA slab, Ecut=300, k=12x12x1',
                             'RPA slab, Ecut=300, k=12x12x1', 'eV')
}

default_columns = [
    'surf_mat', 'LDA_surf', 'PBE_surf', 'RPBE_surf', 'BEEFvdW_surf',
    'vdWDF2_surf', 'mBEEF_surf', 'mBEEFvdW_surf', 'RPA_EXX_surf'
]

reac = ('Material', ['surf_mat'])
basic = ('Surface energies', [
    'LDA_surf', 'PBE_surf', 'RPBE_surf', 'BEEFvdW_surf', 'vdWDF2_surf',
    'mBEEF_surf', 'mBEEFvdW_surf', 'RPA_EXX_surf'
])
ldadata = ('LDA Data', ['LDA_bulk', 'LDA_slab', 'LDA_surf'])
pbedata = ('PBE Data', ['PBE_bulk', 'PBE_slab', 'PBE_surf'])
rpbedata = ('RPBE Data', ['RPBE_bulk', 'RPBE_slab', 'RPBE_surf'])
vdwdf2data = ('vdW-DF2 Data', ['vdWDF2_bulk', 'vdWDF2_slab', 'vdWDF2_surf'])
beefvdwdata = ('BEEF-vdW Data',
               ['BEEFvdW_bulk', 'BEEFvdW_slab', 'BEEFvdW_surf'])
mbeefdata = ('mBEEF Data', ['mBEEF_bulk', 'mBEEF_slab', 'mBEEF_surf'])
mbeefvdwdata = ('mBEEF-vdW Data',
                ['mBEEFvdW_bulk', 'mBEEFvdW_slab', 'mBEEFvdW_surf'])

exxdata = ('EXX Data', ['EXX_bulk', 'EXX_slab', 'EXX_surf'])
rpadata = ('RPA Correlation Energy Extrapolated',
           ['RPA_bulk_extrap', 'RPA_slab_extrap', 'RPA_surf', 'RPA_EXX_surf'])

rpabulkdata = ('Bulk: RPA Correlation Energy', [
    'RPA_bulk_ecut300_k12', 'RPA_bulk_ecut400_k12', 'RPA_bulk_ecut500_k12',
    'RPA_bulk_extrap'
])
rpaslabdata = ('Slab: RPA Correlation Energy', [
    'RPA_slab_ecut300_k6', 'RPA_slab_ecut400_k6', 'RPA_slab_ecut500_k6',
    'RPA_slab_ecut300_k12', 'RPA_slab_extrap'
])

layout = convert([('Basic properties', [[reac, basic, UNITCELL], [ATOMS]]),
                  ('DFT Data', [[ldadata, pbedata, rpbedata, vdwdf2data],
                                [beefvdwdata, mbeefdata, mbeefvdwdata]]),
                  ('Exact exchange data', [[exxdata]]),
                  ('RPA data', [[rpabulkdata, rpaslabdata], [rpadata]])])
