title = 'Benchmark: compression energies of bulk fcc and rocksalt'

default_columns = [
    'id',
    'age',
    'formula',
    'energy',
    'pbc',
    'volume',
    'charge',
    'mass',
    'magmom',
    'name',
    'relativistic',
]

key_descriptions = {
    key: ('', '', '')
    for key in [
        'basis',
        'calculator_version',
        'kptdensity', 'project', 'relativistic', 'width', 'code', 'iter',
        'structure', 'x', 'time',
        'name', 'category', 'adsorbate', 'site']}
