from cmr.web import ATOMS, create_table as table

title = 'Organic Donor-Acceptor molecules'

key_descriptions = {'rho': ('Packing', 'Packing density', ''),
                    'dip': ('Dipole', 'Dipole moment', 'Debye'),
                    'KS_gap': ('KS_gap', 'HOMO-LUMO gap (B3LYP)', 'eV'),
                    'E_homo': ('KS_HOMO', 'HOMO (B3LYP)', 'eV'),
                    'E_lumo': ('KS_LUMO', 'LUMO (B3LYP)', 'eV'),
                    'E_gap': ('TB_gap_extrapolated',
                              'Tight-binding gap extrapolated', 'eV'),
                    'E_opt': ('E_optical', 'Optical gap (singlet-triplet gap)',
                              'eV'),
                    'Unit': ('Unit', 'Unit', ''),
                    'E_homo_TB': ('TB_HOMO', 'HOMO (Tight-binding)', 'eV'),
                    'E_lumo_TB': ('TB_LUMO', 'LUMO (Tight-binding)', 'eV'),
                    'D_homo': ('D_HOMO',
                               'Dimer-monomer HOMO difference (B3LYP)', 'eV'),
                    'D_lumo': ('D_LUMO',
                               'Dimer-monomer LUMO difference (B3LYP)', 'eV'),
                    'DeltaU': ('DeltaU', 'DeltaU', 'eV'),
                    'Energy': ('GS Energy', 'GS Energy', 'eV'),
                    'V_oc': ('V_oc', 'V_oc for PCBM acceptor', 'V')}

for key in ['CAN_SMILES', 'InChI', 'SMILES', 'Name', 'fold']:
    key_descriptions[key] = ('', '', '')

default_columns = ['id', 'formula', 'Unit', 'KS_gap', 'E_homo', 'E_lumo',
                   'E_gap', 'E_opt', 'rho']


def layout(row, kd, prefix):
    basic = table(row,
                  'Property',
                  ['formula', 'Unit', 'E_homo', 'E_lumo', 'KS_gap', 'E_opt',
                   'E_gap', 'dip', 'rho'],
                  kd)

    return [('Basic properties',
             [[basic], [ATOMS]])]
