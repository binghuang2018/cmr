# type: ignore
import shutil
import subprocess
import traceback
from functools import partial
from multiprocessing import Pool
from pathlib import Path

from ase.db import connect
from ase.db.core import QueryParameters
from ase.db.insert_into import insert_into
from ase.db.postgresql import PostgreSQLDatabase
from ase.db.sqlite import all_tables
from asr.c2db.convex_hull import main as convex_hull
from asr.core import chdir
from asr.database.fromtree import main as fromtree

from cmr import project2folder, skip
from cmr.app import app, initialize_project, projects, tmpdir

C2DB_TREE = Path().home() / 'mount/niflheim2/cmr/C2DB-ASR/tree'
ICSD_COD = Path().home() / 'mount/niflheim2/cmr/C2DB-ASR/ICSD-COD'
DATABASES = Path().home() / 'mount/niflheim/jensj/web-pages/cmr/downloads'
POSTGRES_URL = 'postgresql://ase:ase@localhost:5432/'
OQMD123 = (
    Path().home() /
    'mount/niflheim2/cmr/databases/referencedatabases/oqmd123.db')
REFERENCES = (
    Path().home() /
    'mount/niflheim2/C2DB-ASR/collected-databases/references-c2db.db')
PROJECT_NAMES = [name
                 for name, folder in project2folder.items()
                 if folder not in skip]
PROJECT_NAMES = ['c2db']


def do_everything(max_rows: int = 2,
                  test: bool = True) -> None:
    print(PROJECT_NAMES)
    for name in PROJECT_NAMES:
        print(name)
        if name == 'c2db':
            if test:
                tree = Path('c2db-tree')
                references = create_c2db_references(tree)
                tree.mkdir(parents=True)
                subfolder = 'AB2/MoS2/MoS2-249ca8711314/'
                shutil.copytree(src=C2DB_TREE / subfolder,
                                dst=tree / subfolder,
                                symlinks=True)
                run_convex_hull(tree, references)
            else:
                references = REFERENCES
                tree = C2DB_TREE

            references = create_c2db_references(tree)
            path = create_c2db_database(tree)

        else:
            path = DATABASES / f'{name}.db'

        sql = f'DROP DATABASE {name}; CREATE DATABASE {name};'
        cmd = f'psql -c "{sql}" postgres'
        subprocess.run(cmd, shell=True, check=True)

        pg = connect(POSTGRES_URL + name)
        # delete_all_rows(pg)

        db = connect(path)
        insert_into(source=db,
                    destination=pg,
                    query_parameters=QueryParameters('', 0, -1, ''),
                    show_progress_bar=True)
        print('Remenber metadata ???')
        (tmpdir / name).mkdir()

        with Pool(4) as pool:
            pool.map(partial(create_static_files, name), range(4))


def create_c2db_references(tree: Path) -> Path:
    references = Path('c2db-references.db')
    fromtree(
        folders=[f'{tree}/*/*/*/',
                 f'{tree.parent}/ICSD-COD/*el/*/'],
        recursive=False,
        patterns=('results-asr.magnetic_anisotropy.json,'
                  'results-asr.structureinfo.json,'
                  'results-asr.database.material_fingerprint.json,'
                  'results-asr.gs.json'),
        children_patterns='',
        njobs=4,
        dbname=str(references))
    metadata = {'title': 'Monolayers (from C2DB)',
                'legend': 'Monolayers',
                'name': '{row.formula} ({row.crystal_type})',
                'label': '{row.formula} ({row.crystal_type})',
                'link': '/c2db/row/{row.uid}',
                'method': 'DFT'}
    db = connect(references)
    db.metadata = metadata
    return references


def create_c2db_database(tree: Path) -> Path:
    # uid=Pb2Te6-3995fa1bee6e --delete
    folders = ([str(path) for path in tree.glob('*/*/*/')] +
               [str(path) for path in tree.parent.glob('ICSD-COD/*el/*/')
                if path.is_dir()])
    folders = [f'{tree}/*/*/*/',
               f'{tree.parent}/ICSD-COD/*el/*/']
    dbpath = Path('c2db.db')

    fromtree(
        folders=folders,
        # folders=[str(tree / 'AB2/Mo*/*/')],
        recursive=False,
        children_patterns='',
        dbname=str(dbpath),
        njobs=4)

    return dbpath


def run_convex_hull(tree, references):
    """Rerun convex hull for all materials."""
    referencedatabases = [str(db) for db in [OQMD123, references.absolute()]]
    for path in tree.glob('*/*/*/'):
        with chdir(path):
            convex_hull(databases=referencedatabases)


def create_static_files(name: str, chunk_number: int) -> None:
    print(chunk_number)
    folder = project2folder[name]
    initialize_project(name, folder, POSTGRES_URL + name)

    # Create client:
    app.testing = True
    client = app.test_client()

    project = projects[name]
    db = project['database']
    uid_key = project['uid_key']
    uids = [row.get(uid_key)
            for row in db.select(include_data=False)
            if row.id % 4 == chunk_number]
    print(name, folder, uid_key, len(uids))
    for uid in uids:
        print(uid, end=' ', flush=True)
        try:
            client.get(f'/{name}/row/{uid}').data.decode()
        except Exception as ex:
            traceback.print_exc()
            print('\nError:', ex, uid, flush=True)


def delete_all_rows(db: PostgreSQLDatabase) -> None:
    for table in reversed(all_tables):
        with db.managed_connection() as con:
            cur = con.cursor()
            cur.execute(f'DELETE FROM {table};')


if __name__ == '__main__':
    do_everything(test=False, max_rows=999999)
