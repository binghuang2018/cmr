title = 'Structure optimization with BondMin'

default_columns = [
    'formula', 'energy', 'fmax',
    'n', 'name', 'optimizer', 'dataset']

key_descriptions = dict(
    n=('number of steps',
       'Number of steps (DFT calculations) '
       'needed to optimize the geometry of the system',
       ''),
    name=('name of the system',
          'Unique idetifier of the initial structure to the relaxation, '
          'in format {formula}_{position(optional)}_{copy_number}',
          ''),
    optimizer=('optimizer',
               'Optimizer used for the relaxation',
               ''),
    error=('error message',
           'Error message raised during the run',
           ''),
    dataset=('dataset of the entry',
             'Data set to which the entry belongs '
             '(see paper for more details)',
             ''))
