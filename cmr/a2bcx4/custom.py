from cmr.web import convert_old_layout as convert, ATOMS, UNITCELL

title = 'Database of A2BCX4 materials'

key_descriptions = {
    'E_relative_per_atom': ('Energy per atom',
                            'Energy per atom '
                            '(with respect to the most stable structure)',
                            'meV'),
    'E_uncertainty_per_atom': ('E-unc', 'Uncertainty of the total energy',
                               'meV'),
    'GLLB_dir': ('GLLB-gap-dir', 'Direct band gap (GLLB-SC)', 'eV'),
    'GLLB_ind': ('GLLB-gap-ind', 'Indirect band gap (GLLB-SC)', 'eV'),
    'lattice': ('Crystal system', 'Crystal system', ''),
    'prototype': ('Prototype',
                  'Template crystal structure or prototype present '
                  'in the ICSD database', ''),
    'space_group': ('Space group',
                    'Space group: I-4, I4-2m, P31, Ama2, P1n1, Pmn21', ''),
    'name': ('Short name', 'Short chemical formula', ''),
    'mbeef_en': ('Total energy', 'Total energy  (mBEEF)', 'eV')}


def handle_query(args):
    parts = []
    if args['query']:
        parts.append(args['query'])
    if args['prototype']:
        parts.append('prototype=' + args['prototype'])
    return ','.join(parts)


default_columns = ['name', 'space_group', 'GLLB_ind', 'GLLB_dir',
                   'E_relative_per_atom', 'E_uncertainty_per_atom']

basic = ('Item', ['name', 'prototype', 'space_group', 'GLLB_ind', 'GLLB_dir'])

layout = convert(
    [('Basic properties',
      [[basic, UNITCELL],
       [ATOMS]])])
