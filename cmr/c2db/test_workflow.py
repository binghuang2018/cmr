"""Module containing tests of C2DBs workflow."""
from ase import Atoms
import os
import pytest
from pathlib import Path
import numpy as np
from myqueue.task import task


referencedatabases = [
    '/home/niflheim2/cmr/databases/referencedatabases/oqmd123.db',
    '/home/niflheim2/cmr/C2DB-ASR/collected-databases/references-c2db.db',
]


a = 2.51
BN = Atoms(
    "BN",
    scaled_positions=[[0, 0, 0.5], [1 / 3, 2 / 3, 0.5]],
    cell=[
        [a, 0.0, 0.0],
        [-0.5 * a, np.sqrt(3) / 2 * a, 0],
        [0.0, 0.0, 15.0],
    ],
    pbc=[True, True, False],
)


def task_to_set(list_of_tasks):
    """Convert list_of_tasks to a set."""
    s = set()
    for t in list_of_tasks:
        s.add(str(t))

    return s


@pytest.fixture
def workdir(tmpdir):
    """Fixture creates an empty directory and cd's into that dir."""
    p = os.getcwd()

    os.chdir(tmpdir)

    try:
        yield tmpdir
    finally:
        os.chdir(p)


@pytest.mark.xfail
def test_workflow_basic_workflow(workdir):
    """Test with different combinations of magstates."""
    from .workflow import create_tasks

    test_tasks = []
    BN.write('unrelaxed.json')
    test_tasks.append(task('asr.relax', resources='40:20h'))
    test_tasks.append(task('asr.structureinfo', deps=['asr.relax']))
    test_tasks.append(task("asr.gs", resources="40:5h", deps=["asr.relax"]))
    test_tasks.append(task("asr.magstate", resources="1:10m", deps=["asr.gs"]))
    test_tasks.append(task('asr.convex_hull ' + ' '.join(referencedatabases),
                           name='asr.convex_hull',
                           deps=['asr.gs']))
    test_tasks.append(
        task(
            'asr.bandstructure',
            resources="40:5h",
            deps=["asr.gs"]
        )
    )
    test_tasks.append(
        task(
            'asr.projected_bandstructure',
            resources="1:10m",
            deps=["asr.bandstructure"],
        )
    )
    test_tasks.append(
        task(
            'asr.pdos',
            resources='40:5h',
            deps=['asr.gs']
        )
    )
    test_tasks.append(
        task(
            'asr.bader',
            deps=['asr.gs'],
            tmax='1h',
        )
    )

    tasks = create_tasks()
    assert task_to_set(tasks) == task_to_set(test_tasks)


@pytest.mark.xfail
def test_dynamical_stability_workflow(workdir):
    """Unit test for dynamical_stability_workflow()."""
    from .workflow import dynamical_stability_workflow
    BN.write('structure.json')
    strainfolders = ['strains--1.0%-xx',
                     'strains-+1.0%-xx',
                     'strains--1.0%-yy',
                     'strains-+1.0%-yy',
                     'strains--1.0%-xy',
                     'strains-+1.0%-xy']

    deps = ['strains--1.0%-xx/asr.relax',
            'strains-+1.0%-xx/asr.relax',
            'strains--1.0%-yy/asr.relax',
            'strains-+1.0%-yy/asr.relax',
            'strains--1.0%-xy/asr.relax',
            'strains-+1.0%-xy/asr.relax']

    tasks = dynamical_stability_workflow('.')
    test_tasks = [*[task('asr.relax',
                         folder=strainfolder,
                         resources='40:3h')
                    for strainfolder in strainfolders],
                  task('asr.stiffness',
                       deps=deps),
                  task('asr.phonons', restart=3, resources='40:8h',
                       deps=['asr.gs'])]
    assert task_to_set(tasks) == task_to_set(test_tasks)

    for t in tasks:
        if t.cmd.name == 'asr.stiffness':
            for dep in t.deps:
                assert str(dep).startswith('/')
            break
    else:
        raise AssertionError('No stiffness calc!')


@pytest.mark.xfail
@pytest.mark.parametrize('hform', [-1, 0, 0.21, 1])
def test_is_thermodynamically_stable(workdir, hform):
    """Unit test for is_thermodynamically_stable()."""
    from asr.core import write_json
    from .workflow import is_thermodynamically_stable

    write_json('results-asr.convex_hull.json', {'hform': hform})

    isstable = is_thermodynamically_stable(Path('.'))

    if hform > 0.2:
        assert not isstable
    else:
        assert isstable


basictasks = {'asr.structureinfo', 'asr.gs', 'asr.magstate',
              'asr.convex_hull', 'asr.bandstructure',
              'asr.projected_bandstructure',
              'asr.pdos', 'asr.bader'}
dyn_stab_tasks = {'asr.stiffness', 'asr.phonons'}
strainfolders = ['strains--1.0%-xx',
                 'strains-+1.0%-xx',
                 'strains--1.0%-yy',
                 'strains-+1.0%-yy',
                 'strains--1.0%-xy',
                 'strains-+1.0%-xy']
for strainfolder in strainfolders:
    dyn_stab_tasks.add(strainfolder + '/asr.relax')

property_tasks = {'asr.polarizability'}

gap_tasks = {'asr.emasses', 'asr.borncharges', 'asr.infraredpolarizability',
             'asr.hse', 'asr.gw'}
no_gap_tasks = {'asr.fermisurface', 'asr.plasmafrequency'}


@pytest.mark.xfail
@pytest.mark.parametrize('filename', ['unrelaxed.json', 'structure.json'])
@pytest.mark.parametrize('gap', [0, 1])
@pytest.mark.parametrize('hform', [0, 0.21])
@pytest.mark.parametrize('dynamic_stability_phonons', ['low', 'high'])
@pytest.mark.parametrize('dynamic_stability_stiffness', ['low', 'high'])
@pytest.mark.parametrize('has_inversion', [True, False])
@pytest.mark.parametrize('magstate', ['NM', 'FM', 'AFM'])
def test_full_workflow(workdir, monkeypatch, filename, gap,
                       hform, dynamic_stability_phonons,
                       dynamic_stability_stiffness,
                       has_inversion, magstate):
    """Test full workflow with all eventualities."""
    from .workflow import create_tasks
    from asr.core import write_json

    def get_potential_energy(self, *args, **kwargs):
        return 0

    monkeypatch.setattr(Atoms, 'get_potential_energy', get_potential_energy)

    folder = Path().resolve()
    write_json(folder / 'results-asr.gs.json', {'gap': gap, 'gap_nosoc': gap})
    if not filename == 'unrelaxed.json':
        write_json(folder / 'results-asr.relax.json', {'dummy': 0.0})

    write_json(folder / 'results-asr.convex_hull.json', {'hform': hform})
    write_json(folder / 'results-asr.stiffness.json',
               {'dynamic_stability_stiffness': dynamic_stability_stiffness})
    write_json(folder / 'results-asr.phonons.json',
               {'dynamic_stability_phonons': dynamic_stability_phonons})
    write_json(folder / 'results-asr.magstate.json',
               {'magstate': magstate})
    write_json(folder / 'results-asr.structureinfo.json',
               {'has_inversion_symmetry': has_inversion})
    write_json(folder / 'results-asr.setup.strains.json', {'dummy': 0})

    for basictask in basictasks:
        path = folder / f'results-{basictask}.json'
        if not path.is_file():
            write_json(path, {})

    for strainfolder in strainfolders:
        Path(folder / strainfolder).mkdir()
        write_json(folder / strainfolder / 'results-asr.relax.json',
                   {'dummy': 0})

    BN.write(folder / filename)
    tasks = create_tasks()
    tasknames = {str(t.folder) + '/' + t.cmd.name for t in tasks}

    def prepend_folder(st):
        return {str(folder / name) for name in st}

    if filename == 'unrelaxed.json':
        assert tasknames == prepend_folder(basictasks.union({'asr.relax'}))
        return

    assert str(folder / 'asr.relax') not in tasknames

    if hform > 0.2:
        assert tasknames == prepend_folder(basictasks)
        return

    assert prepend_folder(basictasks).issubset(tasknames)
    assert prepend_folder(dyn_stab_tasks).issubset(tasknames)
    if dynamic_stability_stiffness == 'low' or \
       dynamic_stability_phonons == 'low':
        assert not prepend_folder(no_gap_tasks).intersection(tasknames)
        assert not prepend_folder(gap_tasks).intersection(tasknames)
        return

    assert prepend_folder(property_tasks).issubset(tasknames)

    if gap > 0:
        assert not prepend_folder(no_gap_tasks).intersection(tasknames)
        assert prepend_folder(gap_tasks).issubset(tasknames)
    else:
        assert prepend_folder(no_gap_tasks).issubset(tasknames)
        assert not prepend_folder(gap_tasks).intersection(tasknames)
