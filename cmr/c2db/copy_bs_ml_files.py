from pathlib import Path


def main():
    tree = Path(
        '/home/niflheim/nirkn/electronic-structure-machine-learning/tree')
    c2db = Path('/home/jensj/mount/niflheim2/cmr/C2DB-ASR/tree')
    n = 0
    for path in tree.glob('*/*/*/results-asr.bandstructure_ml.json'):
        new = c2db.joinpath(*path.parts[-4:])
        print(new)
        assert not new.is_file()
        assert new.parent.is_dir()
        # new.symlink_to(path)
        # break
        n += 1
    print(n)


if __name__ == '__main__':
    main()
