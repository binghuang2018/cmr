from collections import Counter

from ase.symbols import string2symbols
from ase.db import connect

tex = """table 1 ..."""

db1 = connect('oqmd_screening2.db')

if 0:
    # extract defect tolerant icsd's from table I:
    icsds = []
    for line in tex.splitlines():
        parts = line.split('&')
        formula = parts[0].split()[0]
        for c in '(){}$_^* ':
            formula = formula.replace(c, '')
        gllb_ind = float(parts[1])
        count = Counter(string2symbols(formula))
        rows = list(db1.select('{},{}<GLLB_ind<{}'
                               .format(formula,
                                       gllb_ind - 0.01,
                                       gllb_ind + 0.01)))
        row = rows[-1]
        if len(rows) == 2:
            assert formula == 'K2Sn2O3'
            assert abs(row.m_e - 0.15) < 0.01
        else:
            assert len(rows) == 1
        icsds.append(int(row.icsd.split('-')[1]))
    print('defect_tolerant =', repr(set(icsds)))


defect_tolerant = {
    41926, 150626, 38322, 152054, 280022, 35342, 58641, 56444, 74904, 23288,
    106350, 52775, 22105, 53243, 300128, 300216, 75195, 62557, 409262, 4071,
    69997, 69995, 246690, 25675, 16550, 401095, 629069, 85138, 26963, 100001,
    28734, 28736, 12157, 30358, 35028, 97997, 33259, 15511, 1053, 44680,
    54244, 68069, 100465, 87414, 56443, 16790, 181102, 2142, 52283, 402227,
    72557, 10457, 63651, 158956, 300217, 40216, 411606, 409438, 64659, 53923,
    52652, 281259, 413385, 152052, 413384, 43028, 23638, 82537, 651447, 652213,
    97998, 26418, 51772, 109291}

print(len(defect_tolerant))

db2 = connect('pv_pec_oqmd.db')

for row in db1.select():
    kvp = row.key_value_pairs
    count = row.count_atoms()

    icsd = int(row.icsd.split('-')[1])
    kvp['icsd'] = icsd
    dt = icsd in defect_tolerant

    if 0.1 <= row.PBE_gap <= 2.5:
        try:
            kvp['magnetic'] = row.selected == 'No'
        except AttributeError:
            print(row)
            kvp['magnetic'] = False
        else:
            if row.selected:
                kvp['defect_tolerant'] = dt

    if dt:
        assert row.selected

    del kvp['selected']
    del kvp['natoms']

    assert kvp['ntypes'] == len(row.count_atoms())
    db2.write(row.toatoms(), **kvp)
