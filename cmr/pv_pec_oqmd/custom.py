from cmr.web import ATOMS, UNITCELL, convert_old_layout as convert

title = 'Screening for PV and PEC materials using the OQMD database'

key_descriptions = {
    'm_e': ('Electron mass', 'Effective electron mass', '`m_e`'),
    'm_h': ('Hole mass', 'Effective hole mass', '`m_e`'),
    'GLLB_dir': ('GLLB-gap-dir', 'Direct band gap (GLLB-SC)', 'eV'),
    'GLLB_ind': ('GLLB-gap-ind', 'Indirect band gap (GLLB-SC)', 'eV'),
    'PBE_gap': ('PBE-gap', 'Band gap (PBE)', 'eV'),
    'lattice': ('Crystal system', 'Crystal system', ''),
    'icsd': ('ICSD number', '', ''),
    'Dxc': ('Derivative discontinuity', 'Derivative discontinuity (GLLB-SC)',
            'eV'),
    'ntypes': ('Species', 'Number of species', ''),
    'spacegroup': ('Space group', '', ''),
    'defect_tolerant': ('Defect tolerant', '', ''),
    'magnetic': ('Magnetic', '', '')}


special_keys = [
    ('SELECT', 'lattice'),
    ('SELECT', 'spacegroup'),
    ('SELECT', 'ntypes'),
    ('SELECT', 'defect_tolerant')]

default_columns = ['formula', 'lattice', 'spacegroup',
                   'GLLB_ind', 'GLLB_dir', 'Dxc', 'PBE_gap',
                   'defect_tolerant', 'magnetic', 'icsd', 'm_e', 'm_h']

layout = convert(
    [('Basic properties',
      [[ATOMS, UNITCELL],
       [('Item', default_columns + ['volume'])]])])
