from ase.db import connect

con1 = connect('c2dm.db', use_lock_file=False)
newname = 'c2dm.new.db'
with connect(newname, use_lock_file=False) as con2:
    for row in con1.select():
        kvp = row.get('key_value_pairs', {})
        data = row.get('data')
        if 0:  # data:
            ef = data['efermi']
            X = data['xbs_K']
            labels = ['${}$'.format(x if x != 'Gamma' else r'\Gamma')
                      for x in data['bslabels_K']]
            plot = {'title': 'Band structure',
                    'data': [{'label': 'LDA',
                              'style': '-k',
                              'x': 'xbs_k',
                              'y': 'ebs_kn'},
                             {'label': '$G_0W_0$',
                              'style': 'o-r',
                              'x': 'xqpbs_k',
                              'y': 'qpbs_kn'},
                             {'label': '$E_F$',
                              'style': '--k',
                              'x': [0, data['xbs_k'][-1]],
                              'y': [ef, ef]}],
                    'xlabel': (X, labels),
                    'ylabel': 'Energy relative to vacuum [eV]',
                    'ylim': (ef - 5, ef + 5)}
            data = {k: data[k]
                    for k in ['xbs_k', 'ebs_kn', 'xqpbs_k', 'qpbs_kn',
                              'qpbands', 'ibzk_kc', 'efermi', 'qp_kn']}
            data['bs'] = plot

        del kvp['project']
        kvp['prototype'] = {'H': 'MoS2', 'T': 'CdI2'}[kvp.pop('phase')]
        con2.write(row, data=data, **kvp)
