from cmr.web import convert_old_layout as convert, ATOMS, UNITCELL

title = 'Computational 2D Materials Database'

default_columns = ['formula', 'phase', 'hform', 'dir_gap',
                   'dir_gap_g0w0']

key_descriptions = {
    'xc': ('XC', 'Exchange-correlation (XC) energy functional', ''),
    'phase': ('Phase', '', ''),
    'hform': ('Heat of formation', 'Heat of formation', 'eV'),
    'hform_fere': (
        'Heat of formation (ref.)',
        'Heat of formation based on fitted elemental phase reference energies',
        'eV'),
    'cbm': ('Con. band', 'Conduction band minimum relative to vacuum', 'eV'),
    'vbm': ('Val. band', 'Valence band maximum relative to vacuum', 'eV'),
    'dir_gap': ('Dir. band gap (DFT)', 'Direct band gap', 'eV'),
    'ind_gap': ('Ind. band gap (DFT)', 'Indirect band gap', 'eV'),
    'dir_gap_g0w0': ('Dir. band gap (G0W0)', 'Direct band gap', 'eV'),
    'ind_gap_g0w0': ('Ind. band gap (G0W0)', 'Indirect band gap', 'eV'),
    'cbm_g0w0': ('Con. band (G0W0)',
                 'Conduction band minimum relative to vacuum', 'eV'),
    'vbm_g0w0': ('Val. band (G0W0)',
                 'Valence band maximum relative to vacuum', 'eV'),
    'emass1_g0w0': ('Electron mass (1)',
                    'Electron mass (direction 1 - smallest)', '`m_e`'),
    'emass2_g0w0': ('Electron mass (2)',
                    'Electron mass (direction 2 - largest)', '`m_e`'),
    'hmass1_g0w0': ('Hole mass (1)',
                    'Hole mass (direction 1 - smallest)', '`m_e`'),
    'hmass2_g0w0': ('Hole mass (2)',
                    'Hole mass (direction 2 - largest)', '`m_e`'),
    'q2d_macro_df_slope':
        ('Dielectic func',
         'Slope of macroscopic 2D static dielectric function at q=0', '?'),
    'a': ('', '', ''),
    'name': ('', '', ''),
    'project': ('', '', '')}

basic = ['hform', 'hform_fere', 'phase',
         'xc',
         'magmom',
         'cbm', 'vbm', 'dir_gap', 'ind_gap',
         'cbm_g0w0', 'vbm_g0w0', 'dir_gap_g0w0', 'ind_gap_g0w0',
         'emass1_g0w0', 'emass2_g0w0', 'hmass1_g0w0', 'hmass2_g0w0',
         'q2d_macro_df_slope']

layout = convert(
    [('Basic Properties',
      [[('Property', basic)],
       [ATOMS, UNITCELL]])])
