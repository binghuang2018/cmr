# flake8: noqa
title = 'New Light Harvesting Materials'

key_descriptions = {
    'gllbsc_dir_gap': ('GLLB Dir. gap', 'Direct bandgap calculated with GLLB-SC.', 'eV'),
    'gllbsc_ind_gap': ('GLLB Ind. gap', 'Indirect bandgap calculated with GLLB-SC.', 'eV'),
    'gllbsc_disc': ('GLLB Der. disc.', 'Derivative discontinuity calculated with GLLB-SC.', 'eV'),
    'mpid': ('MP ID', 'ID of materials in Materials project', ''),
    'icsd_id': ('ICSD ID', 'ID of materials in ICSD', ''),
    'g0w0_gap': ('`G_0W_0` gap', '`G_0W_0` gap at `\\Gamma`', 'eV'),
    'gw0_gap': ('`GW_0` gap', '`GW_0` gap at `\\Gamma`', 'eV'),
    'gw_gap': ('`GW` gap', '`GW` gap at `\\Gamma`', 'eV'),
    'hse06_gap': ('HSE06 gap', 'HSE06 gap at `\\Gamma`', 'eV'),
    'lda_gap': ('LDA gap', 'LDA gap at `\\Gamma`', 'eV'),
    'gllbsc_gap': ('GLLB gap', 'GLLBSC gap at `\\Gamma`', 'eV'),
    'project': ('', '', '')}

default_columns = ['id', 'age', 'formula', 'energy', 'pbc', 'volume', 'charge', 'magmom', 'gllbsc_dir_gap', 'gllbsc_ind_gap', 'mpid']
