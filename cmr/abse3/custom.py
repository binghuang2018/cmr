title = 'Ternary Selenides ABSe3'

default_columns = [
    'prototype', 'name', 'space_group', 'pbe_tot_en', 'pbe_hof']

key_descriptions = dict(
    prototype=('name of the prototype',
               'Prototype indicating the original structure '
               'used in the atomic substitutions',
               ''),
    name=('name of the system',
          'Identifier of the structure: distinguishes the ABSe3 from '
          'the BASe3 structure. The combination of prototype and '
          'name form a unique identifier.',
          ''),
    space_group=('Space group',
                 'Space group as obtained with spglib',
                 ''),
    pbe_tot_en=('PBE total energy',
                '',
                'eV'),
    pbe_hof=('PBE heat of formation',
             'Heat of formation obtained with PBE'
             'OQMD unaries and binaries have been used as a reference',
             'eV/atom'))
