__version__ = '21.5.0'

downloads = [
    ('adsorption', ['adsorption.db', 'surfaces.db']),
    ('absorption_perovskites', ['absorption_perovskites.db',
                                'perfect_abs.txt']),
    ('abse3', ['abse3.db']),
    ('abs3', ['abs3.db']),
    ('abx2', ['abx2.db']),
    ('agau309', ['agau309.db']),
    ('a2bcx4', ['a2bcx4.db']),
    ('beef', ['molecules.db', 'solids.db']),
    ('bondmin', ['bondmin.db']),
    ('catapp', ['catapp.db', 'catappdata.csv']),
    ('compression', ['compression.db']),
    ('cubic_perovskites', ['cubic_perovskites.db']),
    ('c1db', ['c1db.db']),
    ('c2db', ['c2db.db', 'workflow.png', 'c2db-db.png']),
    ('c2dm', ['c2dm.db']),
    ('dcdft', ['dcdft.db', 'dcdft_gpaw_pw_paw09.db']),
    ('dssc', ['dssc.db']),
    ('fcc111', ['fcc111.db']),
    ('funct_perovskites', ['funct_perovskites.db']),
    ('gbrv', ['gbrv.db']),
    ('g2', ['g2.db']),
    ('htgw', ['htgw.db']),
    ('lowdim', ['lowdim.db']),
    ('low_symmetry_perovskites', ['low_symmetry_perovskites.db']),
    ('mp_gllbsc', ['mp_gllbsc.db']),
    ('oqmd12', ['oqmd12.db']),
    ('oqmd123', ['oqmd123.db']),
    ('organometal', ['organometal.db']),
    ('pv_pec_oqmd', ['pv_pec_oqmd.db']),
    ('qpod', ['qpod.db', 'workflow_qpod.png']),
    ('solar', ['solar.db']),
    ('tmfp06d', ['tmfp06d.db']),
    ('imp2d', ['imp2d.db']),
    ('vdwh', ['chi-data.tar.gz', 'graphene-data.tar.gz',
              'chi-data-v2.tar.gz'])]


project2folder = {filename[:-3]: folder
                  for folder, filenames in downloads
                  for filename in filenames if filename.endswith('.db')}


skip = {'dcdft', 'beef', 'vdwh', 'c2dm', 'gbrv', 'htgw', 'bondmin'}

names = [name for name, folder in project2folder.items()
         if folder not in skip]

if 0:
    for name, folder in project2folder.items():
        if folder not in skip:
            # print(f'scp {name}.db cmr@fysik-cmr02:data')
            # print(f'create database {name};')
            # print(f'ase db {name}.db -i $CMR_URL/{name} --progress-bar')
            print(f'ase db {name}.db -m > {name}-meta.json')
