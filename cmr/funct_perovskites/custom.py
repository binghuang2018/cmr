# flake8: noqa
title = 'Functional Perovskites'

key_descriptions = {
    'gllbsc_dir_gap': ('GLLB dir. gap', 'Direct bandgap calculated with GLLB-SC.', 'eV'),
    'gllbsc_ind_gap': ('GLLB ind. gap', 'Indirect bandgap calculated with GLLB-SC.', 'eV'),
    'gllbsc_disc': ('GLLB der. disc.', 'Derivative discontinuity calculated with GLLB-SC.', 'eV'),
    'gllbsc_gamma': ('GLLB BG', 'Bandgap at `\\Gamma` calculated with GLLB-SC.', 'eV'),
    'comb_A': ('A-comb.', 'Cubic perovskite labeled as A-combination', ''),
    'comb_B': ('B-comb.', 'Cubic perovskite labeled as B-combination', ''),
    'sequence': ('Sequence', 'Sequence of A and B layers', ''),
    'project': ('', '', ''),
    'gllbsc_gamma_gap': ('', '', '')}

default_columns = ['id', 'age', 'formula', 'energy', 'pbc', 'volume', 'charge', 'mass', 'comb_A', 'comb_B', 'sequence', 'gllbsc_gamma_gap']
