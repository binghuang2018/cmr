title = 'G2/97'

default_columns = [
    'id',
    'age',
    'formula',
    'energy',
    'fmax',
    'pbc',
    'volume',
    'charge',
    'mass',
    'name',
    'relativistic',
]

key_descriptions = {
    key: ('', '', '')
    for key in [
        'basis', 'basis_threshold', 'calculator_version',
        'kptdensity', 'linspacestr', 'project', 'relativistic',
        'time', 'width', 'x', 'bsse_corrected', 'relaxed',
        'category', 'name']}
