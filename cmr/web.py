from ase import Atoms
from ase.calculators.calculator import kptdensity2monkhorstpack
from ase.db.core import float_to_time_string, now
from ase.geometry import cell_to_cellpar
from ase.formula import Formula


# Predefined blocks:
ATOMS = {'type': 'atoms'}
UNITCELL = {'type': 'cell'}


def create_table(row,  # AtomsRow
                 header,  # List[str]
                 keys,  # List[str]
                 key_descriptions,  # Dict[str, Tuple[str, str, str]]
                 digits=3  # int
                 ):  # -> Dict[str, Any]
    """Create table-dict from row."""
    table = []
    for key in keys:
        if key == 'age':
            age = float_to_time_string(now() - row.ctime, True)
            table.append(('Age', age))
            continue
        value = row.get(key)
        if value is not None:
            if isinstance(value, float):
                value = '{:.{}f}'.format(value, digits)
            elif not isinstance(value, str):
                value = str(value)
            desc, unit = key_descriptions.get(key, ['', key, ''])[1:]
            if unit:
                value += ' ' + unit
            table.append((desc, value))
    return {'type': 'table',
            'header': header,
            'rows': table}


def default_layout(row,  # AtomsRow
                   key_descriptions,  # Dict[str, Tuple[str, str, str]]
                   prefix  # str
                   ):  # -> List[Tuple[str, List[List[Dict[str, Any]]]]]
    """Default page layout.

    "Basic properties" section and the rest in a "miscellaneous" section.
    """
    keys = ['id',
            'energy', 'fmax', 'smax',
            'mass',
            'age']
    table = create_table(row, ['Key', 'Value'], keys, key_descriptions)
    misc = miscellaneous_section(row, key_descriptions, exclude=keys)
    layout = [('Basic properties', [[ATOMS, UNITCELL],
                                    [table]]),
              misc]
    return layout


def miscellaneous_section(row, key_descriptions, exclude):
    """Helper function for adding a "miscellaneous" section.

    Create table with all keys except those in exclude.
    """
    misckeys = (set(key_descriptions) |
                set(row.key_value_pairs)) - set(exclude)
    misc = create_table(row, ['Items', ''], sorted(misckeys), key_descriptions)
    return ('Miscellaneous', [[misc]])


class Summary:
    def __init__(self, row, meta={}, subscript=None, prefix=''):
        self.row = row

        atoms = Atoms(cell=row.cell, pbc=row.pbc)
        self.size = kptdensity2monkhorstpack(atoms,
                                             kptdensity=1.8,
                                             even=False)

        self.cell = [['{:.3f}'.format(a) for a in axis] for axis in row.cell]
        par = ['{:.3f}'.format(x) for x in cell_to_cellpar(row.cell)]
        self.lengths = par[:3]
        self.angles = par[3:]

        self.stress = row.get('stress')
        if self.stress is not None:
            self.stress = ', '.join('{0:.3f}'.format(s) for s in self.stress)

        self.formula = Formula(Formula(row.formula)
                               .format('abc')).format('html')

        kd = meta.get('key_descriptions', {})
        create_layout = meta.get('layout') or default_layout
        self.layout = create_layout(row, kd, prefix)

        self.dipole = row.get('dipole')
        if self.dipole is not None:
            self.dipole = ', '.join('{0:.3f}'.format(d) for d in self.dipole)

        self.data = row.get('data')
        if self.data:
            self.data = ', '.join(self.data.keys())

        self.constraints = row.get('constraints')
        if self.constraints:
            self.constraints = ', '.join(c.__class__.__name__
                                         for c in self.constraints)


def row_to_dict(row, project, layout_function, tmpdir):
    project_name = project['name']
    uid = row.get(project['uid_key'])
    s = Summary(row,
                {'key_descriptions': project['key_descriptions'],
                 'layout': layout_function},
                prefix=str(tmpdir / f'{project_name}/{uid}-'))
    return s


def convert_old_layout(page):
    def layout(row, kd, prefix):
        def fix(block):
            if isinstance(block, tuple):
                title, keys = block
                return create_table(row, [title], keys, kd)
            return block

        return [(title, [[fix(block) for block in column]
                         for column in columns])
                for title, columns in page]
    return layout


def dct2plot(dct, name, filename=None, show=True):
    """Create a plot from a dict.

    Example::

        d = {'a': [0, 1, 2],
             'b': [1.2, 1.1, 1.0],
             'abplot': {'title': 'Example',
                        'data': [{'x': 'a',
                                  'y': 'b',
                                  'label': 'label1',
                                  'style': 'o-g'}],
                        'xlabel': 'blah-blah [eV]'}}
        dct2plot(d, 'plot')

    """
    import matplotlib.pyplot as plt
    fig = plt.figure()
    styles = ['k-', 'r-', 'g-', 'b-']
    plot = dct[name]
    lines = []
    labels = []
    for d in plot['data']:
        x = d['x']
        if isinstance(x, str):
            x = dct[x]
        y = d['y']
        if isinstance(y, str):
            y = dct[y]
        style = d.get('style')
        if not style:
            style = styles.pop()
        lines.append(plt.plot(x, y, style)[0])
        labels.append(d['label'])
    plt.legend(lines, labels)
    if isinstance(plot['xlabel'], str):
        plt.xlabel(plot['xlabel'])
    else:
        x, labels = plot['xlabel']
        plt.xticks(x, labels)
        plt.xlim(x[0], x[-1])
    plt.ylabel(plot['ylabel'])
    if 'ylim' in plot:
        plt.ylim(*plot['ylim'])
    plt.title(plot['title'])
    try:
        plt.tight_layout()
    except AttributeError:
        pass
    if show:
        plt.show()
    if filename:
        plt.savefig(filename)
        plt.close(fig)
