from pathlib import Path
from urllib.request import urlretrieve

import pytest
from ase.db import connect

from cmr import names, project2folder
from cmr.app import app, initialize_project


@pytest.fixture(scope='session')
def client():
    """Create Flask app object."""
    dir = Path(__file__).parent / '../docs'
    path = dir / 'cmr.db'
    if not path.is_file():
        url = 'https://cmr.fysik.dtu.dk'
        urlretrieve(f'{url}/_downloads/cmr.db', path)

    db = connect(path)
    print(db.metadata)
    for row in db.select():
        name = row.project_name
        folder = project2folder[name]
        p = dir / folder / f'{name}.db'
        if not p.is_file():
            db2 = connect(p)
            kvp = row.key_value_pairs
            del kvp['project_name']
            db2.write(row.toatoms(), data=row.data, key_value_pairs=kvp)
            if name in {'c1db', 'c2db', 'qpod'}:
                db2.metadata = db.metadata

    for name in names:
        folder = project2folder[name]
        initialize_project(name,
                           folder,
                           str(dir / folder / f'{name}.db'))

    app.testing = True
    return app.test_client()
