import json
from pathlib import Path

from ase.db import connect

duplicates = """Au2Br2-902a783b0eee
SSn-787570417caf
Nb2Br6-af5044c6839d
Hg3Te3-2338c988a923
Nb2Cl6-62a998190c47
Ag2Br2-0ad99522f6f3
Ag2I2-27cc74b5cd97
Ag2Mn2O8-efb6b2a12665
CuCl2-d657b0c341ff
Zr2I6-3eb514398743
Ta2Se8-8d72a3dff394
Te3-291ade8c0fe4
Hg2O2-32e3a79b70a1
Ta2Br6-3382a3e209cc
Hf2I6-1494dee603c3
Au2Cl2-b4d8bcb49f62
Nb2Se8-4758586cc2d8
""".splitlines()


def add_tags(dbname):
    db = connect(dbname)
    for uid in duplicates:
        print(uid)
        folder = db.get(uid=uid, calculator='dftd3').folder
        print(folder)
        fp = Path(folder) / 'results-asr.database.material_fingerprint.json'
        if fp.is_file():
            fp.unlink()
            print('*')
        s = Path(folder) / 'structure.json'
        text = s.read_text()
        s2 = s.with_name('structure-duplicate.json')
        if s2.is_file():
            print('OK')
            continue
        s2.write_text(text)
        data = json.loads(text)
        data['1']['tags'] = [42] * data['1']['numbers']['__ndarray__'][0][0]
        s.write_text(json.dumps(data))


def fix_dos(glob: str) -> None:
    print(glob)
    for path in Path().glob(glob):
        dos = path / 'results-asr.dos.json'
        if dos.is_file():
            text = dos.read_text()
            dct = json.loads(text)
            try:
                data = dct['kwargs']['data']
            except KeyError:
                continue
            if 'dosspin1_e' not in data:
                print(dos)
                data['dosspin1_e'] = []
                if 1:
                    dos.with_name('old-results-asr.dos.json').write_text(text)
                    dos.write_text(json.dumps(dct))


if __name__ == '__main__':
    import sys
    # add_tags('full_C1DB.db')
    fix_dos(sys.argv[1])
