asr run "database.fromtree --dbname c1db.db --njobs=4 \
--exclude-patterns=results-asr.*_mass.json,results-asr.dos.json \
--children-patterns= \
core/*elements/*/*/*m \
core/*elements/*/*/*new_bulk/nm/  \
core/*elements/*/*/*new_1D/nm/ \
new_structure/*/tree/*/*/*/ \
new_structure/*/tree/*/*/*/*new_1D/nm/ \
new_structure/*/tree/*/*/*/*new_bulk/nm/"
