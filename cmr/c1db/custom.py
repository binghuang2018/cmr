from asr.database.app import create_key_descriptions as ckd, row_to_dict
from asr.database.browser import layout as _layout

__all__ = ['row_to_dict', 'create_key_descriptions', 'layout']

title = 'Computational 1D materials database'

default_columns = ['formula', 'hform', 'gap', 'is_magnetic']

uid_key = 'uid'


def handle_query(args):
    parts = []
    if args['query']:
        parts.append(args['query'])
    if args['dims'] != 'both':
        parts.append('pbc=' + args['dims'])
    if args['xc'] != 'both':
        parts.append('calculator=' + args['xc'])
    if args['source'] != 'any':
        parts.append('Source=' + args['source'])
    if args['dyn_phonons'] != 'all':
        parts.append('dynamic_stability_phonons=' + args['dyn_phonons'])
    if args['from_tdyn'] > '1':
        parts.append('thermodynamic_stability_level>=' + args['from_tdyn'])
    if args['to_tdyn'] < '3':
        parts.append('thermodynamic_stability_level<=' + args['to_tdyn'])
    if args['from_gap']:
        parts.append(args['xc'] + '>=' + args['from_gap'])
    if args['to_gap']:
        parts.append(args['xc'] + '<=' + args['to_gap'])
    if args['stoichiometry']:
        parts.append('stoichiometry=' + args['stoichiometry'])
    if args['is_magnetic']:
        parts.append('is_magnetic=' + args['is_magnetic'])
    return ','.join(parts)


def create_key_descriptions(db):
    key_descriptions = ckd(db)
    key_descriptions['PBE_1D'] = (
        'PBE 1D-uid', 'uid for 1D material calculated using PBE', '')
    key_descriptions['PBED3_1D'] = (
        'PBE-D3 1D-uid', 'uid for 1D material calculated using PBE-D3', '')
    key_descriptions['PBED3_3D'] = (
        'PBE-D3 3D-uid', 'uid for 3D material calculated using PBE-D3', '')
    key_descriptions['Source'] = (
        'Source', 'Source', '')
    key_descriptions['derived_from'] = (
        'derived from', 'derived from', '')
    return key_descriptions


COD = 'https://www.crystallography.net/cod/'
ICSD = 'https://icsd.products.fiz-karlsruhe.de/en/'


def layout(row, kd, prefix):
    panels = _layout(row, kd, prefix)
    for name, panel in panels:
        if name == 'Summary':
            for column in panel:
                for thing in column:
                    if thing['type'] != 'table':
                        continue
                    if thing['header'][0] == 'Structure info':
                        source = row.Source
                        df = row.derived_from
                        if source == 'COD':
                            thing['rows'].append(
                                ['Source',
                                 f'<a href={COD}/{df}.html>COD {df}</a>'])
                        elif source == 'ICSD':
                            thing['rows'].append(
                                ['Source',
                                 f'<a href={ICSD}>ICSD {df}</a>'])
                        else:
                            thing['rows'] += [
                                ['Source', source],
                                ['Derived form',
                                 f'<a href={df}>{df}</a>']]

                        for key, text in [('PBED3_1D', '1D (PBE-D3)'),
                                          ('PBE_1D', '1D (PBE)'),
                                          ('PBED3_3D', '3D (PBE-D3)')]:
                            uid = row.get(key)
                            if uid:
                                thing['rows'].append(
                                    [text, f'<a href={uid}>{uid}</a>'])
                        return panels
    1 / 0
