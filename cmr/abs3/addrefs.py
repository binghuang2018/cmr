from ase.db import connect
from ase.calculators.singlepoint import SinglePointCalculator

db2 = connect('binaries_oqmd_stable.db')
with connect('abs3.db') as db:
    for row in db2.select():
        a = row.toatoms()
        e = row.get('mBEEF_tot_en')
        if e is not None:
            a.calc = SinglePointCalculator(energy=e, atoms=a)
            a.calc.name = 'gpaw'
        db.write(a, icsd=row.icsd, gap_PBE=row.gap_PBE, isreference=True)
