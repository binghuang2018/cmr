from pathlib import Path

import matplotlib.pyplot as plt
from cmr.web import (create_table as table, ATOMS, UNITCELL,
                     miscellaneous_section)

title = 'Database of ABS3 materials'

key_descriptions = {
    'E_hull': ('E-hull',
               'Energy of the convex hull '
               '(with respect to the most stable structure)', 'eV'),
    'm_e': ('Electron mass', 'Effective electron mass', '`m_e`'),
    'm_h': ('Hole mass', 'Effective hole mass', '`m_e`'),
    'E_relative_per_atom': ('Energy per atom',
                            'Energy per atom '
                            '(with respect to the most stable structure)',
                            'eV'),
    'E_uncertainty_hull': ('E-hull-unc',
                           'Uncertainty of the convex hull energy', 'eV'),
    'E_uncertainty_per_atom': ('E-unc', 'Uncertainty of the total energy',
                               'eV'),
    'GLLB_dir': ('GLLB-gap-dir', 'Direct band gap (GLLB-SC)', 'eV'),
    'GLLB_ind': ('GLLB-gap-ind', 'Indirect band gap (GLLB-SC)', 'eV'),
    'PBEsol_gap': ('PBEsol_gap', 'Band gap (PBEsol)', 'eV'),
    'lattice': ('Crystal system', 'Crystal system', ''),
    'prototype': ('Prototype', 'prototype name', ''),
    'ABS3_name': ('Short name', 'Short chemical formula', ''),
    'isreference': ('Is reference', '', '')}


def handle_query(args):
    parts = []
    if args['query']:
        parts.append(args['query'])
    if args['prototype']:
        parts.append('prototype=' + args['prototype'])
    return ','.join(parts)


default_columns = ['formula', 'energy',
                   'ABS3_name', 'prototype', 'GLLB_ind', 'GLLB_dir',
                   'E_relative_per_atom']


def layout(row, kd, prefix):
    keys = ['prototype', 'GLLB_ind', 'GLLB_dir', 'm_e', 'm_h']
    basic = table(row,
                  ['Item', ''],
                  keys,
                  kd)

    page = [('Basic properties',
             [[basic, UNITCELL],
              [ATOMS]])]

    if hasattr(row, 'data'):
        d = row.data
        if 'x' in d:
            path = Path(prefix + 'bs.png')
            if not path.is_file():
                plt.plot(d.x, d.y)
                n1 = len(d.X)
                n2 = len(d.names)
                if n1 != n2:
                    print('bad data:', d.X, d.names, row.id)
                n = min(n1, n2)
                plt.xticks(d.X[:n], d.names[:n])
                plt.xlim(0, d.X[-1])
                plt.ylim(-6, 5)
                plt.ylabel('GLLB-SC energy relative to VBM [eV]')
                plt.savefig(str(path))
                plt.close()
            page.append(('Electronic band structure',
                         [[{'type': 'figure', 'filename': 'bs.png'}]]))

    misc = miscellaneous_section(row, kd, exclude=keys)
    page.append(misc)

    return page
