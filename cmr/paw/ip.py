import json
from collections import defaultdict
from typing import Dict, Tuple, List

from ase.units import Ha
from gpaw import setup_paths
from gpaw.atom.aeatom import AllElectronAtom
from gpaw.atom.atompaw import AtomPAW
from gpaw.setup import create_setup

setup_paths[:] = ['.']


def ip(symbol: str, fd, setup: str = 'paw') -> Tuple[float, float, float]:
    xc = 'LDA'  # AtomPAW can only handle LDA!
    aea = AllElectronAtom(symbol, xc=xc, log=fd)
    aea.initialize()
    aea.run()
    aea.refine()
    aea.scalar_relativistic = True
    aea.refine()
    energy = aea.ekin + aea.eH + aea.eZ + aea.exc
    eigs: Dict[Tuple[int, int], float] = {}
    for l, channel in enumerate(aea.channels):
        n = l + 1
        for e, f in zip(channel.e_n, channel.f_n):
            if f == 0:
                break
            eigs[(n, l)] = -e * Ha
            n += 1
    _, (n0, l0) = min((e, nl) for nl, e in eigs.items())
    aea = AllElectronAtom(symbol, xc=xc, log=fd)
    c = 0.9
    aea.add(n0, l0, -c)
    aea.initialize()
    aea.run()
    aea.refine()
    aea.scalar_relativistic = True
    aea.refine()
    for l, channel in enumerate(aea.channels):
        n = l + 1
        for e, f in zip(channel.e_n, channel.f_n):
            if f == 0:
                break
            eigs[(n, l)] += e * Ha
            n += 1
    IP = aea.ekin + aea.eH + aea.eZ + aea.exc - energy
    IP *= Ha
    print(IP, eigs, file=fd)

    s = create_setup(symbol, type=setup, xc=xc)
    f_ln: Dict[int, List[float]] = defaultdict(list)
    for l, f in zip(s.l_j, s. f_j):
        if f:
            f_ln[l].append(f)

    f_sln = [[f_ln[l] for l in range(1 + max(f_ln))]]
    calc = AtomPAW(symbol, f_sln, xc=xc, txt=fd, setups=setup)
    energy = calc.results['energy']
    eps_i = -calc.wfs.kpt_u[0].eps_n

    f_sln[0][l0][-1] -= c
    calc = AtomPAW(symbol, f_sln, xc=xc, charge=c, txt=fd, setups=setup)
    IP2 = calc.results['energy'] - energy
    eps_i += calc.wfs.kpt_u[0].eps_n
    eps_i *= Ha
    print(IP2, eps_i, file=fd)

    i1 = 0
    for l, f, n in zip(s.l_j, s. f_j, s.n_j):
        if f:
            i2 = i1 + 2 * l + 1
            eps_i[i1:i2] -= eigs[(n, l)]
            i1 = i2

    return IP, IP2, abs(eps_i).max()


if __name__ == '__main__':
    from pathlib import Path
    symbol = Path.cwd().name.split('.')[0]
    with open('ip.out', 'w') as fd:
        IP, IP0, ddeps = ip(symbol, fd)
    Path('ip.json').write_text(json.dumps({'dIP': IP - IP0, 'ddeps': ddeps}))
