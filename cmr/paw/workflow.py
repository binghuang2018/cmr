from pathlib import Path
from myqueue.task import task
from cmr.paw.ren import names as ren_names


symbol2 = {
    'H': 'Li', 'Li': 'F', 'Be': 'S', 'B': 'N', 'C': 'Ti', 'N': 'V',
    'O': 'Cd', 'F': 'Na', 'Na': 'Cl', 'Mg': 'O', 'Al': 'P', 'Si': 'C',
    'P': 'In', 'S': 'Zn', 'Cl': 'Cu', 'K': 'Cl', 'Ca': 'O', 'Sc': 'S',
    'Ti': 'N', 'V': 'C', 'Cr': 'N', 'Mn': 'S', 'Fe': 'O', 'Co': 'O',
    'Ni': 'O', 'Cu': 'Br', 'Zn': 'O', 'Ga': 'P', 'Ge': 'Te', 'As': 'Ga',
    'Se': 'Cd', 'Br': 'K', 'Rb': 'Cl', 'Sr': 'O', 'Y': 'N', 'Zr': 'C',
    'Nb': 'C', 'Mo': 'C', 'Tc': 'O', 'Ru': 'O', 'Rh': 'O', 'Pd': 'O',
    'Ag': 'Cl', 'Cd': 'O', 'In': 'P', 'Sn': 'S', 'Sb': 'In', 'Te': 'Zn',
    'I': 'Li', 'Cs': 'Cl', 'Ba': 'O', 'Lu': 'N', 'Hf': 'C', 'Ta': 'C',
    'W': 'C', 'Re': 'O', 'Os': 'B', 'Ir': 'O', 'Pt': 'O', 'Au': 'S',
    'Hg': 'F', 'Tl': 'Cl', 'Pb': 'S', 'Bi': 'F', 'Po': 'O'}


def create_tasks():
    symbol = Path.cwd().name.split('.')[0]

    t1 = task('cmr.paw.optimize', cores=8, processes=1, tmax='4d')
    t2 = task('cmr.paw.optimize -w', deps=t1)

    tasks = [t1,
             t2,
             task('gpaw.atom.check', tmax='1h', deps=t2),
             task('cmr.paw.nomad 1', cores=8, tmax='1h', deps=t2),
             task('cmr.paw.ip', deps=t2),
             task('cmr.paw.delta', cores=8, tmax='3h', deps=t2)]

    s2 = symbol2.get(symbol)
    if s2:
        tasks.append(task('cmr.paw.nomad 2', cores=8, tmax='1h',
                          deps=[t2, f'../{s2}/cmr.paw.optimize+-w']))

    if symbol in ren_names:
        tasks.append(task('cmr.paw.ren', cores=8, tmax='1h', deps=t2))

    return tasks


if __name__ == '__main__':
    from ase.collections import dcdft
    for s in dcdft.names:
        Path(s).mkdir()
        try:
            (Path(s) / 'start.txt').write_text(
                (Path('..') / 'v2' / s / 'start.txt').read_text())
        except FileNotFoundError:
            print(s)
