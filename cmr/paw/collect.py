import json
import sys
from collections import defaultdict
from pathlib import Path
from math import log10, nan, isnan
from typing import Dict, List, Tuple, Sequence

import numpy as np
from scipy.interpolate import interp1d
from ase.collections import dcdft
from ase.data import atomic_numbers
from ase.db import connect
from ase.eos import EquationOfState as EOS
from ase.units import kJ, Bohr
from ase.utils.deltacodesdft import delta
from gpaw.hgh import sc_setups

import cmr.paw as paw
from cmr.paw.ren import data as ren_data

GPa = 1e-24 * kJ

path = Path(paw.__file__).with_name('aims.json')
aims_data = connect(path)

symbols2: Dict[str, str] = {}


def delta1(path: Path, symbol: str) -> Dict[str, float]:
    if not dcdft.has(symbol):
        return {key: 1.0 if key != 'delta' else 0.0
                for key in ['v', 'B', 'Bp',
                            'v0', 'B0', 'Bp0',
                            'delta']}
    db = connect(path / 'delta.db')
    V = []
    E = []
    for row in db.select(sort='volume'):
        V.append(row.volume / row.natoms)
        E.append(row.energy / row.natoms)
    eos = EOS(V, E, 'birchmurnaghan')
    eos.fit()
    B, Bp, v = eos.eos_parameters[1:]
    dct = dcdft.data[symbol]
    v0 = dct['wien2k_volume']
    B0 = dct['wien2k_B'] * GPa
    Bp0 = dct['wien2k_Bp']
    return {'v': v, 'B': B / GPa, 'Bp': Bp,
            'v0': v0, 'B0': B0 / GPa, 'Bp0': Bp0,
            'delta': delta(v, B, Bp, v0, B0, Bp0)}


def ren(path: Path, symbol: str) -> Dict[str, float]:
    if symbol not in ren_data:
        return {'R' + key: 1.0
                for key in ['v', 'B', 'M', 'v0', 'B0', 'M0']}
    db = connect(path / 'ren.db')
    V = []
    E = []
    M = []
    for row in db.select(sort='volume'):
        V.append(row.volume / row.natoms)
        E.append(row.energy / row.natoms)
        M.append(row.get('magmom', 0))
    eos = EOS(V, E, 'birchmurnaghan')
    eos.fit()
    B, Bp, v = eos.eos_parameters[1:]
    m = float(interp1d(V, M, kind='cubic', fill_value='extrapolate')(v))
    a0, B0, m0 = ren_data[symbol][::2]
    v0 = (a0 * Bohr)**3 / 8
    return {'Rv': v, 'RB': B / GPa, 'RM': m,
            'Rv0': v0, 'RB0': B0, 'RM0': m0}


def ip(path: Path, symbol: str) -> Dict[str, float]:
    results = json.loads((path / 'ip.json').read_text())
    return results


def nomad(path: Path, symbol: str) -> Dict[str, float]:
    if aims_data.count(element=symbol) == 0:
        return {'eh': 0.0, 'eh0': 0.0, 'ex1': 0.0, 'ex2': 0.0}
    if (path / 'nomad.json').is_file():
        results = json.loads((path / 'nomad.json').read_text())
        for ns, res in enumerate(results, 1):
            (path / f'nomad{ns}.json').write_text(json.dumps(res))
        # (path / 'nomad.json').unlink()

    count, (ea, eax), (refa, refax) = json.loads(
        (path / 'nomad1.json').read_text())
    na = count[symbol]
    dea = (eax - ea) / na
    derefa = (refax - refa) / na
    dct = {'ex1': dea, 'ex10': derefa}

    if (path / 'nomad2.json').is_file():
        count, (eb, ebx), (refb, refbx) = json.loads(
            (path / 'nomad2.json').read_text())
        nb = count.pop(symbol)
        symbol2 = list(count)[0]
        symbols2[symbol] = symbol2
        nb2 = count[symbol2]
        deb = (ebx - eb) / (nb + nb2)
        derefb = (refbx - refb) / (nb + nb2)
        if 'hgh' in path.name:
            if symbol2 in sc_setups:
                s2 = '.hgh.sc'
            else:
                s2 = '.hgh'
        elif 'sg15' in path.name:
            s2 = '.sg15'
        elif 'jth' in path.name:
            s2 = '.jth'
        else:
            s2 = ''
        results2 = json.loads((path / f'../{symbol2}{s2}/nomad1.json')
                              .read_text())
        count, (ec, _), (refc, _) = results2
        nc2 = count[symbol2]
        eh = (eb - ea / na * nb - ec / nc2 * nb2) / (nb + nb2)
        ehref = (refb - refa / na * nb - refc / nc2 * nb2) / (nb + nb2)
        dct['eh'] = eh
        dct['eh0'] = ehref
        dct['ex2'] = deb - derefb
    else:
        if symbol in {'He', 'Ne', 'Ar', 'Kr', 'Xe', 'Rn'}:
            dct['eh'] = 0.0
            dct['eh0'] = 0.0
            dct['ex2'] = 0.0
        else:
            dct['eh'] = nan
            dct['eh0'] = nan
            dct['ex2'] = nan

    return dct


def solve(x: Sequence[float], y: np.ndarray, f: float) -> float:
    x2 = x[::-1]
    y2 = abs(y)[::-1]
    for i, e in enumerate(y2):
        if e > f:
            break
    else:
        return x[0]

    ecut1 = x2[i]
    ecut2 = x2[i - 1]
    e1 = y2[i]
    e2 = y2[i - 1]
    return ecut1 + (f - e1) * (ecut2 - ecut1) / (e2 - e1)


def check(path: Path, symbol: str) -> Dict[str, float]:
    db = connect(path / 'check.db')

    egg = nan
    try:
        eegg = [row.eegg for row in db.select(test='eggbox')]
    except (AttributeError, KeyError):
        pass
    else:
        if len(eegg) == 3:
            egg = max(eegg)

    x = [200, 250, 300, 400, 500, 600, 700, 800, 1500]
    try:
        e1 = np.array([db.get(test='pw1', ecut=ecut).energy for ecut in x])
        e2 = np.array([db.get(test='pw2', ecut=ecut).energy for ecut in x])
    except (AttributeError, KeyError):
        return {'egg': egg,
                'e400': nan,
                'ec': nan,
                'dec': nan}

    de = e2 / 2 - e1
    e400 = abs(de[3] - de[-1])
    ec = solve(x, e1 - e1[-1], 0.01)
    dec = solve(x, de - de[-1], 0.01)
    return {'egg': egg,
            'e400': e400,
            'ec': ec,
            'dec': dec}


header = (0.0,
          '--',
          ['tag ',
           'eh ', 'x1 ', 'x2 ',
           'dip ', 'ddeps ',
           'egg ', '400 ', 'ec ', 'dec ',
           'a ', 'B ', 'Bp ', 'D ',
           'Ra ', 'RB ', 'RM '])

errors: List[Tuple[Path, str]] = []
lengths = [len(word) for word in header[2]]
lines: List[Tuple[float, str, List[str]]] = []
paths = [Path(s) for s in sys.argv[1:]]
everything: Dict[str, List[float]] = defaultdict(list)
tags: List[str] = []
for path in paths:
    if not path.is_dir():
        continue
    if path.parent.name == 'venv':
        continue
    symbol, _, tag = path.name.partition('.')
    if not tag:
        tag = path.parent.name
    d: Dict[str, float] = defaultdict(lambda: nan)
    for f in [delta1,
              nomad,
              # ip,
              check,
              ren]:
        try:
            d.update(f(path, symbol))
        except Exception as ex:
            errors.append((path, f'{f.__name__}: {ex}'))

    data = [((d['eh'] - d['eh0']) * 1000, 10),
            ((d['ex1'] - d['ex10']) * 1000, 2),
            (d['ex2'] * 1000, 2),
            (d['dIP'] * 1000, 100),
            (d['ddeps'] * 1000, 100),
            (d['egg'] * 1000, 1),
            (d['e400'] * 1000, 4),
            (d['ec'], 500),
            (d['dec'], 350),
            (((d['v'] / d['v0'])**0.333 - 1) * 100, 0.3),
            (d['B'] - d['B0'], 1),
            (d['Bp'] - d['Bp0'], 0.1),
            (d['delta'] * 1000, 2.5),
            (((d['Rv'] / d['Rv0'])**0.333 - 1) * 100, 0.3),
            (d['RB'] - d['RB0'], 1),
            (d['RM'] - d['RM0'], 0.1)]

    tags.append(tag)
    d['Z'] = atomic_numbers[symbol]
    for key, val in d.items():
        everything[key].append(val)

    badness = 0.0
    for x, dx in data:
        if not isnan(x):
            badness += (x / dx)**2
    words = [tag + ' ']
    for i, (x, dx) in enumerate(data, 1):
        n = max(0, int(2 - log10(dx)))
        if x == 0.0:
            word = ' '
        elif isnan(x):
            word = '***'
        else:
            word = f'{x:.{n}f}'
            if abs(x) > 2 * dx:
                word += '*'
            elif abs(x) > dx:
                word += '?'
            else:
                word += ' '
        words.append(word)
        lengths[i] = max(lengths[i], len(word))
    lines.append((badness, symbol, words))

lines = sorted(lines, key=lambda line: atomic_numbers[line[1]])
for n in range(len(lines) // 15 * 15, -1, -15):
    lines.insert(n, header)

indices = np.argsort(everything['Z'])
dct = {key: [values[i] for i in indices]
       for key, values in everything.items()}
dct['tag'] = [tags[i] for i in indices]
Path('everything.json').write_text(json.dumps(dct))

for bad, symbol, words in lines:
    symbol2 = symbols2.get(symbol, '')
    if bad < 12:
        color = '\033[92m'
    elif bad < 20:
        color = ''
    else:
        color = '\033[91m'
    if bad < 900:
        b = f'{bad:5.1f}'
    else:
        b = '*****'
    print(f'{symbol:2} {color}{b}\033[0m {symbol2:2}', end='')
    for word, n in zip(words, lengths):
        color = word[-1]
        word = word[:-1].rjust(n)
        if color == '?' or bad == 0.0:
            print(word, end='')
        elif color == ' ':
            print('\033[92m' + word + '\033[0m', end='')
        else:
            print('\033[91m' + word + '\033[0m', end='')
    print()

for path, msg in errors:
    print(path, msg)
