import json
import sys
from pathlib import Path
from ase.db import connect
from gpaw import GPAW, PW, Davidson, setup_paths
import cmr.paw as paw
from gpaw.hgh import sc_setups
from typing import Union


def run(row, setups):
    count = row.count_atoms()
    setup_paths[:0] = ['.' if s == symbol else f'../{s}/' for s in count]
    atoms = row.toatoms()
    energies = []
    for expanded in [False, True]:
        tag = row.formula
        if expanded:
            atoms.set_cell(atoms.cell * 1.05**(1 / 3), scale_atoms=True)
            tag += '+'
        atoms.calc = GPAW(xc='PBE',
                          kpts={'density': 4.0},
                          mode=PW(800),
                          setups=setups,
                          eigensolver=Davidson(2),
                          symmetry={'do_not_symmetrize_the_density': True},
                          txt=tag + '.txt')
        e = atoms.get_potential_energy()
        energies.append(e)
    result = (count, energies, [row.energy, row.energy2])
    return result


if __name__ == '__main__':
    path = Path(paw.__file__).with_name('aims.json')
    ref = connect(path)

    # symbol = Path.cwd().name.split('.')[0]
    symbol, _, setup = Path.cwd().name.partition('.')
    setups: Union[dict, str] = setup or {}
    ns = int(sys.argv[1])

    try:
        row = ref.get(element=symbol, ns=ns)
    except KeyError:
        pass
    else:
        if isinstance(setups, str) and setups.startswith('hgh') and ns == 2:
            setups = {symbol: setups}
            s2 = [s for s in row.count_atoms() if s != symbol][0]
            if s2 in sc_setups:
                setups[s2] = 'hgh.sc'
            else:
                setups[s2] = 'hgh'
        result = run(row, setups)
        Path(f'nomad{ns}.json').write_text(json.dumps(result))
