from math import pi
from pathlib import Path
from ase.collections import dcdft
from ase.db import connect
from ase.units import Ha, Bohr
from gpaw import GPAW, PW, Davidson, setup_paths
from ase.calculators.calculator import kptdensity2monkhorstpack
from typing import Union

setup_paths[:0] = ['.']

ecut = 1000
kd = 6.0
symbol, _, setup = Path.cwd().name.partition('.')
setups: Union[dict, str] = setup or {}

db = connect('delta.db')

for s in range(94, 108, 2):
    if not dcdft.has(symbol):
        break
    atoms = dcdft[symbol]
    id = db.reserve(name=symbol, s=s)
    if id is None:
        continue
    kpts = kptdensity2monkhorstpack(atoms, kd)
    h = pi / (4 * ecut / Ha)**0.5 * Bohr
    x = (s / 100)**(1 / 3)
    atoms.set_cell(atoms.cell * x, scale_atoms=True)
    atoms.calc = GPAW(mode=PW(ecut),
                      setups=setups,
                      kpts=kpts,
                      h=h * x,
                      xc='PBE',
                      eigensolver=Davidson(4),
                      occupations={'name': 'fermi-dirac', 'width': 0.02},
                      txt=f'{symbol}-{s}.txt')
    atoms.get_forces()
    atoms.get_stress()
    db.write(atoms=atoms, id=id, name=symbol, s=s)
