# flake8: noqa
title = 'Organometal Halide Perovskites'

key_descriptions = {
    'gllbsc_dir_gap': ('Dir. gap', 'Direct bandgap calculated with GLLB-SC. A correction due to spin-orbit coupling and exciton effects has been applied', 'eV'),
    'gllbsc_ind_gap': ('Ind. gap', 'Indirect bandgap calculated with GLLB-SC. A correction due to spin-orbit coupling and exciton effects has been applied', 'eV'),
    'gllbsc_disc': ('Der. discont.', 'Derivative discontinuity calculated with GLLB-SC', 'eV'),
    'name': ('Crystal name', 'Name given to the crystal structure', ''),
    'symmetry': ('Symmetry', 'Symmetry of the crystal', ''),
    'space_group': ('Space group', 'Space group of the crystal', ''),
    'project': ('', '', '')}

default_columns = ['id', 'age', 'formula', 'energy', 'pbc', 'volume',
                   'charge', 'gllbsc_dir_gap', 'gllbsc_ind_gap']
