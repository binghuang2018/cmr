title = 'Benchmark: adsoption energy of atomic oxygen and carbon on fcc111'

default_columns = [
    'id',
    'age',
    'formula',
    'energy',
    'pbc',
    'volume',
    'charge',
    'mass',
    'name',
    'category',
    'adsorbate',
    'site',
]

key_descriptions = {
    key: ('', '', '')
    for key in [
        'basis',
        'calculator_version',
        'kptdensity', 'project', 'relativistic', 'width',
        'name', 'category', 'adsorbate', 'site']}
