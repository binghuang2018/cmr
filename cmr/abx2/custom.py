# from pathlib import Path

from cmr.web import (create_table as table, ATOMS, UNITCELL,
                     miscellaneous_section)

title = 'Database of ABX2 materials'

key_descriptions = {
    'E_hull': ('E-hull',
               'Energy of the convex hull '
               '(with respect to the most stable structure)', 'eV'),
    'KS_gap': ('KS-gap', 'Kohn Sham band gap', 'eV'),
    'm_e': ('Electron mass', 'Effective electron mass', '`m_e`'),
    'm_h': ('Hole mass', 'Effective hole mass', '`m_e`'),
    'Dxc': ('Derivative discontinuity', 'Derivative discontinuity (GLLB-SC)',
            'eV'),
    'E_relative_perAtom': ('Energy per atom',
                           'Energy per atom '
                           '(with respect to the most stable structure)',
                           'eV'),
    'E_uncertanty_hull': ('E-hull-unc',
                          'Uncertanty of the convex hull energy', 'eV'),
    'E_uncertanty_perAtom': ('E-unc', 'Uncertanty of the total energy', 'eV'),
    'GLLB_dir': ('GLLB-gap-dir', 'Direct band gap (GLLB-SC)', 'eV'),
    'GLLB_ind': ('GLLB-gap-ind', 'Indirect band gap (GLLB-SC)', 'eV'),
    'TB09_dir': ('TB09-gap-dir', 'Direct band gap (TB09)', 'eV'),
    'TB09_ind': ('TB09-gap-ind', 'Indirect band gap (TB09)', 'eV'),
    'lattice': ('Crystal system', 'Crystal system', ''),
    'phase': ('Phase',
              'Phase: ZB=zinckblende, WZ=wurtzite, KT=wz kesterite, '
              'ST=wz stannite, CP=zb chaclopyrite', ''),
    'short_name': ('Sort name', 'Short chemical formula', ''),
    'total_en': ('Total energy', 'Total energy  (mBEEF)', 'eV')}


def handle_query(args):
    parts = []
    if args['query']:
        parts.append(args['query'])
    if args['phase']:
        parts.append('phase=' + args['phase'])
    return ','.join(parts)


default_columns = ['short_name', 'phase', 'GLLB_ind', 'GLLB_dir',
                   'TB09_ind', 'E_relative_perAtom']


def layout(row, kd, prefix):
    keys = ['phase', 'GLLB_ind', 'GLLB_dir', 'TB09_ind', 'm_e', 'm_h']
    basic = table(row,
                  ['Item', ''],
                  keys,
                  kd)
    misc = miscellaneous_section(row, kd, exclude=keys)

    page = [('Basic properties',
             [[basic, UNITCELL],
              [ATOMS]]),
            # ('Electronic band structure',
            #  [[{'type': 'figure', 'filename': 'bs.png'}]]),
            misc]

    # path = Path(prefix + 'bs.png')
    # if not path.is_file():
    #     dct2plot(row.data, name='bs', filename=str(path), show=False)

    return page
