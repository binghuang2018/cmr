title = 'Pseudopotentials for high-throughput DFT calculations'

default_columns = [
    'id',
    'age',
    'formula',
    'energy',
    'pbc',
    'volume',
    'charge',
    'mass',
    'category',
    'name']

key_descriptions = {
    key: ('', '', '')
    for key in [
        'basis', 'basis_threshold', 'calculator_version',
        'kptdensity', 'linspacestr', 'project', 'relativistic',
        'time', 'width', 'x',
        'category', 'name']}
