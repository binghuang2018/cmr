import traceback
import os
from pathlib import Path
from cmr import project2folder
from cmr.app import app, initialize_project, projects

skip = {'dcdft', 'beef', 'vdwh', 'c2dm', 'gbrv'}

url = os.environ.get('CMR_URL', 'postgresql://ase:ase@localhost:5432')
for name, folder in project2folder.items():
    if name != 'c2db':  # folder in skip:
        continue
    initialize_project(name, folder, f'{url}/{name}')

app.testing = True
c = app.test_client()


def run(name, size, rank):
    project = projects[name]
    print(name, end=' ')
    c.get(f'/{name}/').data.decode()
    db = project['database']
    uid_key = project['uid_key']
    n = len(db)
    print(uid_key, n)
    if name == 'lowdim':
        n = 10
    uids = []
    for row in db.select(include_data=False):
        uids.append(row.get(uid_key))
        if len(uids) == n:
            break
    print(len(uids))
    for i, uid in enumerate(uids):
        if i % size != rank:
            continue
        try:
            c.get(f'/{name}/row/{uid}').data.decode()
        except Exception as ex:
            print()
            print(uid, ex)
            Path(f'{uid}.tb').write_text(traceback.format_exc())
        if rank == 0:
            print(f'\rRows: {i + 1}/{len(uids)}', end='', flush=True)
    if rank == 0:
        print()


if __name__ == '__main__':
    import sys
    if len(sys.argv) == 1:
        names = list(projects)
    else:
        names = sys.argv[1:2]
    for name in names:
        run(name, int(sys.argv[2]), int(sys.argv[3]))
