title = 'Porphyrin based dyes'

key_descriptions = {
    'M': ('Metal', 'Metal center', ''),
    'A': ('Anchor', 'Anchor group', ''),
    'R1': ('Side gr. 1', 'First side group', ''),
    'R2': ('Side gr. 2', 'Second side group', ''),
    'R3': ('Side gr. 3', 'Third side group', ''),
    'KS_HOMO': ('HOMO', 'Kohn-Sham HOMO eigenvalue', 'eV'),
    'KS_LUMO': ('LUMO', 'Kohn-Sham LUMO eigenvalue', 'eV'),
    'KS_gap':
        ('KS Gap', 'Kohn-Sham eigenvalue electronic gap (KS_LUMO - KS_HOMO)',
         'eV'),
    'E_HOMO': ('IP', 'HOMO location calculated as ionization potential', 'eV'),
    'E_LUMO': ('EA', 'LUMO location calculated as electron affinity', 'eV'),
    'E_gap': ('EA - IP', 'Electronic gap calculated as E_LUMO - E_HOMO', 'eV'),
    'E_c': ('Cond. band - IP',
            'Energy difference between conduction band and E_HOMO', 'eV'),
    'E_1': ('Triplet gap', 'Triplet optical gap', 'eV'),
    'E_opt_LUMO': ('IP + Triplet',
                   'Optical LUMO location calculated as E_HOMO + E_1', 'eV'),
    'LQual1': (
        'Level align. 1',
        'Level alignment quality calculated with model 1 for the open-circuit '
        'voltage',
        'eV'),
    'LQual2': (
        'Level align. 2',
        'Level alignment quality calculated with model 2 for the open-circuit '
        'voltage', 'eV'),
    'UsedE1': ('', '', ''),
    'UsedEc': ('', '', ''),
    'dssc': ('', '', ''),
    'gpaw_setups': ('', '', ''),
    'gpaw_version': ('', '', ''),
    'project': ('', '', ''),
    'dssc': ('', '', ''),
    'xc': ('', '', ''),
    'DBScreen': ('', '', '')}

default_columns = ['id', 'age', 'formula', 'calculator', 'energy', 'fmax',
                   'charge', 'mass', 'magmom', 'E_gap', 'KS_gap']
