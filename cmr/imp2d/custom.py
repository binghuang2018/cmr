from cmr.web import create_table, ATOMS, UNITCELL, miscellaneous_section

title = 'Impurities in 2D Materials Database'

key_descriptions = {
    'host': ('Host chemical formula', '', ''),
    'dopant': ('Impurity species', '', ''),
    'defecttype': ('Impurity type', '', ''),
    'eform': ('Defect formation energy', 'Formation energy', 'eV'),
    'site': ('Defect setup position', '', ''),
    'depth': ('Depth parameter', '', ''),
    'extension_factor': ('Extension factor', '', ''),
    'en2': ('Total energy, final WF step', 'Total energy', 'eV'),
    'conv2': ('Total energy convergence, final WF step',
              'Total energy convergence', 'eV'),
    'spin': ('Magnetic moment', '', ''),
    'supercell': ('Calculation supercell', '', ''),
    'host_spacegroup': ('Host spacegroup symbol', '', ''),
    'converged': ('Relaxation converged?', '', ''),
    'name': ('System name', '', ''),
    'en1': ('Total energy, first WF step', '', 'eV'),
    'conv1': ('Total energy convergence, first WF step', '', 'eV'),
    'dopant_chemical_potential': ('Dopant chemical potential', '', 'eV'),
    'hostenergy': ('Pristine host total energy', 'Host energy', 'eV')}

uid_key = 'name'

default_columns = ['host', 'dopant', 'defecttype', 'spin']


def handle_query(args):
    parts = []
    if args['query']:
        parts.append(args['query'])
    if args['host']:
        parts.append('host=' + args['host'])
    if args['dopant']:
        parts.append('dopant=' + args['dopant'])
    if args['defecttype']:
        parts.append('defecttype=' + args['defecttype'])
    if args['host_spacegroup']:
        parts.append('host_spacegroup=' + args['host_spacegroup'])
    if args['converged']:
        parts.append('converged=' + args['converged'])
    # if args['from_eform']:
    #     parts.append('eform>=' + args['eform'])
    # if args['to_eform']:
    #     parts.append('eform<=' + args['eform'])
    return ','.join(parts)


def layout(row, key_descriptions, prefix):
    host = create_table(row,
                        ['Host properties', ''],
                        ['host',
                         'supercell',
                         'host_spacegroup',
                         'hostenergy'],
                        key_descriptions)
    structure = create_table(row,
                             ['Structural properties', ''],
                             ['dopant',
                              'defecttype',
                              'depth',
                              'extension_factor',
                              'site'],
                             key_descriptions)
    energetics = create_table(row,
                              ['Electronic properties', ''],
                              ['spin',
                               'en2',
                               'eform',
                               'dopant_chemical_potential',
                               'conv2'],
                              key_descriptions)

    page = [('Basic properties',
             [[ATOMS, UNITCELL],
              [host, structure, energetics]])]

    page.append(miscellaneous_section(row, key_descriptions, default_columns))

    return page
