title = 'CatApp database'

key_descriptions = {
    'a': ('Reactant A', '', ''),
    'b': ('Reactant B', '', ''),
    'ab': ('Product', 'Product AB', ''),
    'surface': ('Surface', 'Description of surface', ''),
    'facet': ('Facet', 'Surface facet', ''),
    'site': ('Ads. site', 'Adsorption site', ''),
    'xc': ('Functional', 'XC-functional', ''),
    'reference': ('Reference', '', ''),
    'url': ('Link', 'Link to reference', ''),
    'dataset': ('Description', 'Description of calculation', ''),
    'project': ('', '', ''),
    'ref': ('', '', ''),
    'ea': ('', '', ''),
    'er': ('', '', '')}

default_columns = ['id', 'formula', 'surface', 'a', 'b', 'ab', 'facet', 'ea',
                   'er', 'xc']
