from cmr.web import create_table, ATOMS, UNITCELL, miscellaneous_section

title = 'One, two and three component references from OQMD'

key_descriptions = {
    'hform': ('Heat of formation', '', 'eV/atom'),
    'nspecies': ('Species', 'Number of species', ''),
    'uid': ('Unique name', '', ''),
    'stoichiometry': ('Stoichiometry', '', '')}

uid_key = 'uid'

default_columns = ['formula', 'energy', 'hform', 'stoichiometry', 'volume',
                   'fmax', 'smax']


def handle_query(args):
    parts = []
    if args['query']:
        parts.append(args['query'])
    if args['nspecies']:
        parts.append('nspecies=' + args['nspecies'])
    if args['stoichiometry']:
        parts.append('stoichiometry=' + args['stoichiometry'])
    return ','.join(parts)


def layout(row, key_descriptions, prefix):
    basic = create_table(row,
                         ['Property', ''],
                         default_columns,
                         key_descriptions)
    if 'oqmd_id' in row:
        id = row.oqmd_id
        basic['rows'].append(
            ('OQMD-link',
             f'<a href="http://oqmd.org/materials/entry/{id}">{id}</a>'))

    page = [('Basic properties',
             [[ATOMS, UNITCELL],
              [basic]])]

    page.append(miscellaneous_section(row, key_descriptions, default_columns))

    return page
