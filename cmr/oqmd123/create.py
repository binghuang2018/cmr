from ase.db import connect
from ase.formula import Formula

db1 = connect('oqmd123-1.db')
energies = {}
for row in db1.select(ns=1):
    energies[row.symbols[0]] = row.energy / row.natoms
print(energies)
with connect('oqmd123.db') as db2:
    for row in db1.select():
        count = row.count_atoms()
        if 'etot' not in row:
            print(row)
            continue
        row.energy = row.etot
        hform = (row.energy -
                 sum(n * energies[symbol]
                     for symbol, n in count.items())) / row.natoms
        f = Formula(row.formula)
        s = str(f.stoichiometry()[0])
        oqmd_id = row.get('oqmd_id', None)
        extra = {} if oqmd_id is None else {'oqmd_id': oqmd_id}
        db2.write(row,
                  hform=hform,
                  nspecies=len(count),
                  stoichiometry=s,
                  uid=f.format('abc'),
                  **extra)
