import importlib
import os
import re
import tempfile
from functools import partial
from pathlib import Path
from typing import Dict, Any, Union

from ase.db import connect
from asr.database.app import app, projects, handle_query
from ase.db.web import create_key_descriptions
from flask import render_template, send_file, send_from_directory

from cmr import project2folder, skip
from cmr.web import row_to_dict

NAME = re.compile('[a-zA-Z0-9-_.]+$')
path = Path(__file__).parent.parent
app.jinja_loader.searchpath.append(str(path))  # noqa


# Folder for caching png-files:
if 'CMR_TMPDIR' in os.environ:
    tmpdir = Path(os.environ['CMR_TMPDIR'])
else:
    tmpdir = Path(tempfile.mkdtemp(prefix='cmr-app-'))


@app.template_filter()
def cmr_sort_key_descriptions(value):
    """Sort column drop down menu."""
    def sort_func(item):
        return item[1][1]

    return sorted(value.items(), key=sort_func)


@app.template_filter()
def cmr_filter_key_descriptions(value):
    """Filter key descriptions."""
    return {key: value for key, value in value.items()
            if not (key.startswith('has_asr') or key.startswith('dim_'))}


@app.route('/')
def index():
    if len(projects) == 0:
        initialize_projects()
    return render_template(
        'cmr/projects.html',
        projects=sorted([(name,
                          proj['title'],
                          proj['database'].count())
                         for name, proj in projects.items()]))


@app.route('/<project>/file/<uid>/<name>')
def file(project: str, uid: str, name: str):
    assert project in projects
    assert NAME.match(uid)
    assert NAME.match(name)
    path = tmpdir / f'{project}/{uid}-{name}'
    return send_file(str(path))


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(path / 'docs/static',
                               'cmr.ico',
                               mimetype='image/vnd.microsoft.icon')


def initialize_projects():
    url = os.environ.get('CMR_URL', 'postgresql://ase:ase@localhost:5432')
    for name, folder in project2folder.items():
        if folder not in skip:
            initialize_project(name, folder, f'{url}/{name}')


def initialize_project(name: str,
                       folder: str,
                       dbname: Union[str, Path]) -> None:
    if folder != name:
        x = f'_{name}'  # more than one database per folder
    else:
        x = ''

    if not (tmpdir / name).is_dir():
        (tmpdir / name).mkdir()

    mod: Any = importlib.import_module(f'cmr.{folder}.custom{x}')

    project = getattr(mod, "project", None)
    if project:
        project.tmpdir = tmpdir
        projects[name] = project
        return

    proj: Dict[str, Any] = {'name': name}

    search = path / f'cmr/{folder}/search{x}.html'
    if search.is_file():
        proj['search_template'] = f'cmr/{folder}/search{x}.html'
    else:
        proj['search_template'] = 'cmr/search.html'

    row = path / f'cmr/{folder}/row{x}.html'
    if row.is_file():
        proj['row_template'] = f'cmr/{folder}/row{x}.html'
    else:
        proj['row_template'] = 'cmr/row.html'

    table_template = f'cmr/{folder}/table{x}.html'
    table_path = path / table_template
    if table_path.is_file():
        proj['table_template'] = table_template
    else:
        proj['table_template'] = 'ase/db/templates/table.html'

    db = connect(dbname)
    proj['database'] = db

    proj['default_columns'] = mod.default_columns
    proj['title'] = mod.title
    proj['uid_key'] = getattr(mod, 'uid_key', 'id')

    # If kd is a callable then it is a function that takes a set of
    # key value pairs to produce some key descriptions
    create_function = getattr(mod, 'create_key_descriptions', None)

    if create_function:
        proj['key_descriptions'] = create_function(db)
    else:
        proj['key_descriptions'] = create_key_descriptions(
            getattr(mod, 'key_descriptions', {}))

    if hasattr(mod, 'row_to_dict'):
        row_to_dict_function = mod.row_to_dict
    else:
        row_to_dict_function = row_to_dict

    proj['row_to_dict_function'] = partial(
        row_to_dict_function,
        layout_function=getattr(mod, 'layout', None),
        tmpdir=tmpdir)

    proj['handle_query_function'] = getattr(mod, 'handle_query',
                                            handle_query)

    if hasattr(mod, 'connect_endpoints'):
        mod.connect_endpoints(app, proj)

    projects[name] = proj


if __name__ == '__main__':
    from sys import argv
    if len(argv) == 1:
        project_names = [name for name, folder in project2folder.items()
                         if folder not in skip]
    else:
        project_names = argv[1:]
    for name in project_names:
        folder = project2folder[name]
        initialize_project(name, folder, path / f'docs/{folder}/{name}.db')
    app.run(host='0.0.0.0', debug=True)
