from pathlib import Path

import asr
from asr.database.app import create_key_descriptions, row_to_dict
from asr.database.browser import layout

from cmr.app import app

__all__ = ['row_to_dict', 'create_key_descriptions', 'layout']

path = Path(asr.__file__).parent.parent
app.jinja_loader.searchpath.append(str(path))  # noqa

title = 'Quantum Point Defects in 2D Materials (QPOD) Database'

default_columns = ['host_name', 'defect_name', 'charge_state',
                   'defect_pointgroup']

uid_key = 'uid'


def handle_query(args):
    parts = []
    if args['query']:
        parts.append(args['query'])
    if args['defect_name']:
        parts.append('defect_name=' + args['defect_name'])
    if args['host_name']:
        parts.append('host_name=' + args['host_name'])
    if args['charge_state']:
        parts.append('charge_state=(charge ' + args['charge_state'] + ')')
    if args['is_magnetic']:
        parts.append('is_magnetic=' + args['is_magnetic'])
    # We only want to show charge 0 systems to the user by default
    # parts.append('charge_state=(charge 0)')

    return ','.join(parts)


def connect_endpoints(app, proj):
    """Set endpoints for downloading data."""
    # Only do this once (and c2db/custom.py does it)
    pass  # setup_data_endpoints()
