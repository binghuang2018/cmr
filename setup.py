import re
from pathlib import Path
from setuptools import setup, find_packages

txt = Path('cmr/__init__.py').read_text()
version = re.search("__version__ = '(.*)'", txt).group(1)

long_description = Path('README.rst').read_text()

setup(name='cmr',
      version=version,
      description='Computational Materials Repository',
      long_description=long_description,
      url='https://cmr.fysik.dtu.dk/',
      packages=find_packages(),
      package_data={'cmr': ['*.html', '*/*.html']},
      install_requires=['myqueue'],
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'License :: OSI Approved :: '
          'GNU General Public License v3 or later (GPLv3+)',
          'Operating System :: Unix',
          'Programming Language :: Python :: 3.7',
          'Programming Language :: Python :: 3.8',
          'Programming Language :: Python :: 3.9',
          'Programming Language :: Python :: 3.10',
          'Topic :: Scientific/Engineering'])
