"""

::

    export ASE_DB_APP_CONFIG=~/cmr/cmrconf.py
    uwsgi uwsgi.ini
"""

import os
import subprocess
from pathlib import Path

names = [
    'dssc',
    'mp_gllbsc',
    'organometal',
    'cubic_perovskites',
    'low_symmetry_perovskites',
    'c2db',
    'tmfp06d',
    'absorption_perovskites',
    'funct_perovskites',
    'fcc111',
    'compression',
    'g2',
    'catapp',
    'imp2d',
    'abx2',
    'solar',
    'agau309',
    'adsorption',
    'surfaces',
    'abs3',
    'oqmd12',
    'pv_pec_oqmd',
    'a2bcx4',
    'lowdim']

pw = os.environ.get('CMR_PASSWORD', 'ase')

home = Path.home()

ASE_DB_TMPDIR = home / 'uwsgi/static'
ASE_DB_TEMPLATES = home / 'uwsgi/templates'
ASE_DB_DOWNLOAD = False

ASE_DB_NAMES = []
for db in names:
    url = f'postgresql://ase:{pw}@localhost:5432/{db}'
    py = home / 'cmr' / db / 'custom.py'
    ASE_DB_NAMES.extend([url, py])

if 0:
    for db in names:
        print(f'create database {db};')

for db in []:  # names:
    if db in []:  # ['lowdim', 'oqmd12', 'c2db', 'abs3']:
        continue
    subprocess.run(
        f'ase db {db}/{db}.db -i postgresql://ase:{pw}@localhost:5432/{db}',
        shell=True)
